USE [master]
GO

/****** Object:  StoredProcedure [dbo].[Usp_BackupDB]    Script Date: 04/30/2014 11:20:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_BackupDB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Usp_BackupDB]
GO

USE [master]
GO

/****** Object:  StoredProcedure [dbo].[Usp_BackupDB]    Script Date: 04/30/2014 11:20:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Usp_BackupDB]  
(
    @pDBName NVARCHAR(200),/* Give Database name here  eg :MFKLocalLog */
	@pBackUpLocation NVARCHAR(400)/*Give the location name here eg : N'C:\ASAIBACKUP\'  */
) 

AS


/*

   Created By      : Altaf Hussain
   Created Date    : 04/21/2014
   Description     : Create a Backup to Restore Images


   EXEC [dbo].[Usp_BackupDB]  @pDBName = 'MFKLocalLog', @pBackUpLocation = N'C:\ASAIBACKUP\'

*/

BEGIN

    DECLARE    @Bc NVARCHAR(400) = @pBackUpLocation + @pDBName +'.bak'

	
	BACKUP DATABASE [MFKLocalLog] TO  DISK =  @BC
	WITH NOFORMAT, NOINIT , CHECKSUM
	 
	DECLARE @BackUpSetId INT
	
	SELECT  @BackUpSetId = Position FROM Msdb..Backupset WHERE Database_Name = @pDBName
	 AND Backup_Set_Id=( SELECT MAX(Backup_Set_ID) FROM Msdb..BackupSet WHERE Database_Name = @pDBName )
	
	IF @backupSetId is null 
	begin 
		raiserror(N'Verify failed. Backup information for database ''MFKLocalLog'' not found.', 16, 1) 
		SELECT (N'Verify failed. Backup information for database ''MFKLocalLog'' not found.') 
	END
	ELSE
	Begin
		SELECT 'Created BackUp '+@BC BackUpLocation -- 
		/* This will print the complete file and file location , just copy this whole path and assign 
		it to @restorepath variable under restore section after re image is done and restoring starts */
	end
	
	RESTORE VERIFYONLY FROM  DISK = @BC WITH  FILE = @backupSetId,  NOUNLOAD,  NOREWIND

	    
END                 
 



GO


