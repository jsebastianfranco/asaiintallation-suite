## This script delete all files (not folders) that are much older than $NumDays.
## Version 2.0 Added Factory files logs
 
$folderLogFiles = "C:\ASAI\LogFiles" # Folder to delete Logs.
$NumDaysLogFiles = 30 # Number of days.
$folderFactoryFiles = "C:\ASAI\TicketRedemption\Logs" # Folder to delete Logs.
$NumDaysFactoryFiles = 7 # Number of days.
$CurDate = get-date # Current date.
$TestDateLogFiles = $Curdate.AddDays(-$NumDaysLogFiles) # set TestDate
$TestDateFactoryFiles = $Curdate.AddDays(-$NumDaysFactoryFiles) # set TestDate
 
## Recursively searches files that are older than ## $NumDays 
 
get-childitem $folderLogFiles -recurse | foreach { ## Get all the files that have $folder
    If ($_.GetType().Name -eq "FileInfo") 
    { 
	
		If ($_.Name -eq "MFK_Asai.log")
		{
		}
		ELSE
		{
			If ($_.CreationTime -lt $TestDateLogFiles) ## Compare the creation date to the $TestDate
			{ 
				$_.Delete() ## Delete file
			} 
		}
    } 
}

get-childitem $folderFactoryFiles -recurse | foreach { ## Get all the files that have $folder
    If ($_.GetType().Name -eq "FileInfo") 
    {
		If ($_.Name -eq "TicketRedemptionSimulator.log" -OR $_.Name -eq "TicketRedemption.log")
		{
		}
		ELSE
		{
			If ($_.CreationTime -lt $TestDateFactoryFiles) ## Compare the creation date to the $TestDate
			{ 
				$_.Delete() ## Delete file
			}
		}		
    } 
}