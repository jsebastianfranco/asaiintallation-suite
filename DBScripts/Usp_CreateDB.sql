
--********************************Create Login if not exist***************************
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'ASAI')
--BEGIN
--DROP LOGIN [ASAI]
SELECT ''
--END
ELSE
BEGIN
 CREATE LOGIN [ASAI] WITH PASSWORD=N'asai1234', DEFAULT_DATABASE=[master],                                DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
EXEC sys.sp_addsrvrolemember @loginame = N'ASAI', @rolename = N'sysadmin'
EXEC sys.sp_configure N'remote access', N'1'
END
GO

--********************************Create Database if not exist***************************

USE [master]
GO

/****** Object:  Database [MFKLocalLog]    Script Date: 04/29/2014 16:21:15 ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'MFKLocalLog')
begin
--DROP DATABASE [MFKLocalLog]
SELECT ''
END
ELSE
BEGIN
/****** Object:  Database [MFKLocalLog]    Script Date: 04/29/2014 16:21:15 ******/
CREATE DATABASE [MFKLocalLog] ON  PRIMARY 
( NAME = N'MFKLocalLog', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\MFKLocalLog.mdf' , SIZE = 148480KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MFKLocalLog_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\MFKLocalLog_1.ldf' , SIZE = 104704KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)

ALTER DATABASE [MFKLocalLog] SET COMPATIBILITY_LEVEL = 100

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MFKLocalLog].[dbo].[sp_fulltext_database] @action = 'enable'
end

ALTER DATABASE [MFKLocalLog] SET ANSI_NULL_DEFAULT OFF 

ALTER DATABASE [MFKLocalLog] SET ANSI_NULLS OFF 

ALTER DATABASE [MFKLocalLog] SET ANSI_PADDING OFF 

ALTER DATABASE [MFKLocalLog] SET ANSI_WARNINGS OFF 

ALTER DATABASE [MFKLocalLog] SET ARITHABORT OFF 

ALTER DATABASE [MFKLocalLog] SET AUTO_CLOSE OFF 

ALTER DATABASE [MFKLocalLog] SET AUTO_CREATE_STATISTICS ON 

ALTER DATABASE [MFKLocalLog] SET AUTO_SHRINK OFF 

ALTER DATABASE [MFKLocalLog] SET AUTO_UPDATE_STATISTICS ON 

ALTER DATABASE [MFKLocalLog] SET CURSOR_CLOSE_ON_COMMIT OFF 

ALTER DATABASE [MFKLocalLog] SET CURSOR_DEFAULT  GLOBAL 

ALTER DATABASE [MFKLocalLog] SET CONCAT_NULL_YIELDS_NULL OFF 

ALTER DATABASE [MFKLocalLog] SET NUMERIC_ROUNDABORT OFF 

ALTER DATABASE [MFKLocalLog] SET QUOTED_IDENTIFIER OFF 

ALTER DATABASE [MFKLocalLog] SET RECURSIVE_TRIGGERS OFF 

ALTER DATABASE [MFKLocalLog] SET  DISABLE_BROKER 

ALTER DATABASE [MFKLocalLog] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 

ALTER DATABASE [MFKLocalLog] SET DATE_CORRELATION_OPTIMIZATION OFF 

ALTER DATABASE [MFKLocalLog] SET TRUSTWORTHY OFF 

ALTER DATABASE [MFKLocalLog] SET ALLOW_SNAPSHOT_ISOLATION OFF 

ALTER DATABASE [MFKLocalLog] SET PARAMETERIZATION SIMPLE 

ALTER DATABASE [MFKLocalLog] SET READ_COMMITTED_SNAPSHOT OFF 

ALTER DATABASE [MFKLocalLog] SET HONOR_BROKER_PRIORITY OFF 

ALTER DATABASE [MFKLocalLog] SET  READ_WRITE 

ALTER DATABASE [MFKLocalLog] SET RECOVERY SIMPLE 

ALTER DATABASE [MFKLocalLog] SET  MULTI_USER 

ALTER DATABASE [MFKLocalLog] SET PAGE_VERIFY CHECKSUM  

ALTER DATABASE [MFKLocalLog] SET DB_CHAINING OFF 

END
GO

USE [MFKLocalLog]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MFKLocalL__Local__0CBAE877]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MFKLocalLog] DROP CONSTRAINT [DF__MFKLocalL__Local__0CBAE877]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MFKLocalL__Attem__0DAF0CB0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MFKLocalLog] DROP CONSTRAINT [DF__MFKLocalL__Attem__0DAF0CB0]
END

GO

--******************Create Table MFKLocalLog if not exist************************

USE [MFKLocalLog]
GO

/****** Object:  Table [dbo].[MFKLocalLog]    Script Date: 04/29/2014 16:22:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MFKLocalLog]') AND type in (N'U'))
DROP TABLE [dbo].[MFKLocalLog]
GO

USE [MFKLocalLog]
GO

/****** Object:  Table [dbo].[MFKLocalLog]    Script Date: 04/29/2014 16:22:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MFKLocalLog](
	[MFKLocalLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[Header] [nvarchar](100) NOT NULL,
	[Content] [nvarchar](4000) NULL,
	[LocalTime] [datetime] NOT NULL,
	[SentToServerTime] [datetime] NULL,
	[Attempts] [int] NULL,
	[Error] [nvarchar](2000) NULL,
 CONSTRAINT [PK_MFKLocalLog] PRIMARY KEY CLUSTERED 
(
	[MFKLocalLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MFKLocalLog] ADD  DEFAULT (getdate()) FOR [LocalTime]
GO

ALTER TABLE [dbo].[MFKLocalLog] ADD  DEFAULT ((0)) FOR [Attempts]
GO

--******************Create Table T_TransactionalLocal for Balancing if not exist************************

USE [MFKLocalLog]
GO

/****** Object:  Table [dbo].[T_TransactionLocal]    Script Date: 04/29/2014 16:22:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T_TransactionLocal]') AND type in (N'U'))
DROP TABLE [dbo].[T_TransactionLocal]
GO

USE [MFKLocalLog]
GO

/****** Object:  Table [dbo].[T_TransactionLocal]    Script Date: 04/29/2014 16:22:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T_TransactionLocal](
	[TransactionLocalID] [bigint] IDENTITY(1,1) NOT NULL,
	[KioskId] [int] NOT NULL,
	[KioskIP] [nvarchar](24) NULL,
	[SequenceNumber] [nvarchar](20) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[BillDespensed] [decimal](18, 2) NULL,
	[CardNumber] [nvarchar](24) NULL,
	[TicketData] [nvarchar](4000) NULL,
	[TranStatusCode] [nvarchar](32) NOT NULL,
	[OpeCode] [nvarchar](12) NULL,
	[OpeSubject] [nvarchar](16) NULL,
	[TranCode] [nvarchar](12) NULL,
	[TranSubject] [nvarchar](16) NULL,
	[AccCode] [nvarchar](12) NULL,
	[AccSubject] [nvarchar](16) NULL,
	[DateTrx] [datetime] NULL,
	[BillTotal_01] [int] NULL,
	[BillTotal_02] [int] NULL,
	[BillTotal_03] [int] NULL,
	[BillTotal_04] [int] NULL,
	[BillTotal_05] [int] NULL,
	[BillTotal_06] [int] NULL,
	[BillDenom_01] [smallint] NULL,
	[BillDenom_02] [smallint] NULL,
	[BillDenom_03] [smallint] NULL,
	[BillDenom_04] [smallint] NULL,
	[BillDenom_05] [smallint] NULL,
	[BillDenom_06] [smallint] NULL,
	[CoinTotal_01] [int] NULL,
	[CoinTotal_02] [int] NULL,
	[CoinTotal_03] [int] NULL,
	[CoinTotal_04] [int] NULL,
	[CoinTotal_05] [int] NULL,
	[CoinTotal_06] [int] NULL,
	[CoinDenom_01] [smallint] NULL,
	[CoinDenom_02] [smallint] NULL,
	[CoinDenom_03] [smallint] NULL,
	[CoinDenom_04] [smallint] NULL,
	[CoinDenom_05] [smallint] NULL,
	[CoinDenom_06] [smallint] NULL,
	[ManualPay] [decimal](18, 2) NULL,
	[Fee] [decimal](18, 2) NULL,
	[AuthorizationNumber] [nvarchar](32) NULL,
	[AuthorizationRegisterTime] [datetime] NULL,
 CONSTRAINT [PK_TransactionLocal] PRIMARY KEY CLUSTERED 
(
	[TransactionLocalID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--******************Create SP InsertUpdate_TransactionLocal_usp if not exist************************

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[InsertUpdate_TransactionLocal_usp]    Script Date: 03/05/2014 18:17:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertUpdate_TransactionLocal_usp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertUpdate_TransactionLocal_usp]
GO

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[InsertUpdate_TransactionLocal_usp]    Script Date: 06/16/2014 13:54:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsertUpdate_TransactionLocal_usp] (
	@KioskId [int] = 0,
	@KioskIP [nvarchar](24)= NULL,
	@SequenceNumber [nvarchar](20) = '',
	@Amount [decimal](18, 2) = 0,
	@BillDespensed [decimal](18, 2)= NULL,
	@CardNumber [nvarchar](24)= NULL,
	@TicketData [nvarchar](4000)= NULL,
	@TranStatusCode [nvarchar](32) = '',
	@OpeCode [nvarchar](12)= NULL,
	@OpeSubject [nvarchar](16)= NULL,
	@TranCode [nvarchar](12)= NULL,
	@TranSubject [nvarchar](16)= NULL,
	@AccCode [nvarchar](12)= NULL,
	@AccSubject [nvarchar](16)= NULL,
	@DateTrx [datetime]= NULL,
	@BillTotal_01 [int]= NULL,
	@BillTotal_02 [int]= NULL,
	@BillTotal_03 [int]= NULL,
	@BillTotal_04 [int]= NULL,
	@BillTotal_05 [int]= NULL,
	@BillTotal_06 [int]= NULL,
	@BillDenom_01 [smallint]= NULL,
	@BillDenom_02 [smallint]= NULL,
	@BillDenom_03 [smallint]= NULL,
	@BillDenom_04 [smallint]= NULL,
	@BillDenom_05 [smallint]= NULL,
	@BillDenom_06 [smallint]= NULL,
	@CoinTotal_01 [int]= NULL,
	@CoinTotal_02 [int]= NULL,
	@CoinTotal_03 [int]= NULL,
	@CoinTotal_04 [int]= NULL,
	@CoinTotal_05 [int]= NULL,
	@CoinTotal_06 [int]= NULL,
	@CoinDenom_01 [smallint]= NULL,
	@CoinDenom_02 [smallint]= NULL,
	@CoinDenom_03 [smallint]= NULL,
	@CoinDenom_04 [smallint]= NULL,
	@CoinDenom_05 [smallint]= NULL,
	@CoinDenom_06 [smallint]= NULL,
	@ManualPay [decimal](18, 2)= NULL,
	@Fee [decimal](18, 2)= NULL,
	@AuthorizationNumber [nvarchar](32)= NULL,
	@AuthorizationRegisterTime [datetime]= NULL
)
AS
BEGIN
	DECLARE @TransactionLocalID [bigint] 

	SELECT @TransactionLocalID = [TransactionLocalID]
	  FROM [dbo].[T_TransactionLocal]
	 WHERE [KioskId] = @KioskId
	   AND [SequenceNumber] = @SequenceNumber

	SET @TransactionLocalID = ISNULL(@TransactionLocalID, 0)

	IF (@TransactionLocalID <= 0)
	BEGIN
		INSERT INTO [dbo].[T_TransactionLocal]
			   ([KioskId]
			   ,[KioskIP]
			   ,[SequenceNumber]
			   ,[Amount]
			   ,[BillDespensed]
			   ,[CardNumber]
			   ,[TicketData]
			   ,[TranStatusCode]
			   ,[OpeCode]
			   ,[OpeSubject]
			   ,[TranCode]
			   ,[TranSubject]
			   ,[AccCode]
			   ,[AccSubject]
			   ,[DateTrx]
			   ,[BillTotal_01]
			   ,[BillTotal_02]
			   ,[BillTotal_03]
			   ,[BillTotal_04]
			   ,[BillTotal_05]
			   ,[BillTotal_06]
			   ,[BillDenom_01]
			   ,[BillDenom_02]
			   ,[BillDenom_03]
			   ,[BillDenom_04]
			   ,[BillDenom_05]
			   ,[BillDenom_06]
			   ,[CoinTotal_01]
			   ,[CoinTotal_02]
			   ,[CoinTotal_03]
			   ,[CoinTotal_04]
			   ,[CoinTotal_05]
			   ,[CoinTotal_06]
			   ,[CoinDenom_01]
			   ,[CoinDenom_02]
			   ,[CoinDenom_03]
			   ,[CoinDenom_04]
			   ,[CoinDenom_05]
			   ,[CoinDenom_06]
			   ,[ManualPay]
			   ,[Fee]
			   ,[AuthorizationNumber]
			   ,[AuthorizationRegisterTime])
		 VALUES
			   (@KioskId
			   ,@KioskIP
			   ,@SequenceNumber
			   ,@Amount
			   ,@BillDespensed
			   ,@CardNumber
			   ,@TicketData
			   ,@TranStatusCode
			   ,@OpeCode
			   ,@OpeSubject
			   ,@TranCode
			   ,@TranSubject
			   ,@AccCode
			   ,@AccSubject
			   ,@DateTrx
			   ,@BillTotal_01
			   ,@BillTotal_02
			   ,@BillTotal_03
			   ,@BillTotal_04
			   ,@BillTotal_05
			   ,@BillTotal_06
			   ,@BillDenom_01
			   ,@BillDenom_02
			   ,@BillDenom_03
			   ,@BillDenom_04
			   ,@BillDenom_05
			   ,@BillDenom_06
			   ,@CoinTotal_01
			   ,@CoinTotal_02
			   ,@CoinTotal_03
			   ,@CoinTotal_04
			   ,@CoinTotal_05
			   ,@CoinTotal_06
			   ,@CoinDenom_01
			   ,@CoinDenom_02
			   ,@CoinDenom_03
			   ,@CoinDenom_04
			   ,@CoinDenom_05
			   ,@CoinDenom_06
			   ,@ManualPay
			   ,@Fee
			   ,@AuthorizationNumber
			   ,@AuthorizationRegisterTime)
		SELECT @TransactionLocalID = IDENT_CURRENT('T_TransactionLocal')
	END
	ELSE 
	BEGIN 
		UPDATE [dbo].[T_TransactionLocal]
		   SET [KioskId] = @KioskId
			  --,[KioskIP] = @KioskIP
			  --,[SequenceNumber] = @SequenceNumber
			  --,[Amount] = @Amount
			  ,[BillDespensed] = @BillDespensed
			  --,[CardNumber] = @CardNumber
			  --,[TicketData] = @TicketData
			  ,[TranStatusCode] = @TranStatusCode
			  --,[OpeCode] = @OpeCode
			  --,[OpeSubject] = @OpeSubject
			  --,[TranCode] = @TranCode
			  --,[TranSubject] = @TranSubject
			  --,[AccCode] = @AccCode
			  --,[AccSubject] = @AccSubject
			  --,[DateTrx] = @DateTrx
			  --,[BillTotal_01] = @BillTotal_01
			  --,[BillTotal_02] = @BillTotal_02
			  --,[BillTotal_03] = @BillTotal_03
			  --,[BillTotal_04] = @BillTotal_04
			  --,[BillTotal_05] = @BillTotal_05
			  --,[BillTotal_06] = @BillTotal_06
			  --,[BillDenom_01] = @BillDenom_01
			  --,[BillDenom_02] = @BillDenom_02
			  --,[BillDenom_03] = @BillDenom_03
			  --,[BillDenom_04] = @BillDenom_04
			  --,[BillDenom_05] = @BillDenom_05
			  --,[BillDenom_06] = @BillDenom_06
			  --,[CoinTotal_01] = @CoinTotal_01
			  --,[CoinTotal_02] = @CoinTotal_02
			  --,[CoinTotal_03] = @CoinTotal_03
			  --,[CoinTotal_04] = @CoinTotal_04
			  --,[CoinTotal_05] = @CoinTotal_05
			  --,[CoinTotal_06] = @CoinTotal_06
			  --,[CoinDenom_01] = @CoinDenom_01
			  --,[CoinDenom_02] = @CoinDenom_02
			  --,[CoinDenom_03] = @CoinDenom_03
			  --,[CoinDenom_04] = @CoinDenom_04
			  --,[CoinDenom_05] = @CoinDenom_05
			  --,[CoinDenom_06] = @CoinDenom_06
			  --,[ManualPay] = @ManualPay
			  --,[Fee] = @Fee
			  --,[AuthorizationNumber] = @AuthorizationNumber
			  --,[AuthorizationRegisterTime] = @AuthorizationRegisterTime
		 WHERE [TransactionLocalID] = @TransactionLocalID
	END 

	SELECT @TransactionLocalID As [TransactionLocalID];
END

GO

--******************Create SP MfkLocalLog_DeleteStatus if not exist************************

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[MfkLocalLog_DeleteStatus]    Script Date: 04/29/2014 16:23:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MfkLocalLog_DeleteStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MfkLocalLog_DeleteStatus]
GO

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[MfkLocalLog_DeleteStatus]    Script Date: 04/29/2014 16:23:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[MfkLocalLog_DeleteStatus]
(
	@MinutesToHold INT = 5
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE  @Itr  Bit 
		, @DtRpt DateTime 
		, @Rcnt INT
		, @PrevCnt INT
		, @LastCnt INT
		, @Filter varchar(256)
             
		,   @ErrLine    VARCHAR(10)
		,   @ErrMsg     VARCHAR(3000) 

	SELECT @Filter = '%DeviceStatusInfo%'

	SELECT @PrevCnt = COUNT(1)
	  FROM [dbo].[MFKLocalLog]
			 WHERE ([Header] Like @Filter) 

	SELECT @DtRpt =  DATEADD(N, -@MinutesToHold, MAX([LocalTime]))
	  FROM [dbo].[MFKLocalLog]

	BEGIN TRY 
     
		SET @Itr = 1
		WHILE (@Itr = 1 )
		BEGIN

				 SET ROWCOUNT 4999

				 DELETE
				 FROM [dbo].[MFKLocalLog]
				 WHERE ([Header] Like @Filter) AND ([LocalTime] <= @DtRpt)

				 SET @Rcnt = @@ROWCOUNT

				 IF @Rcnt = 0
				 BEGIN
						   SET @Itr = 0
				 END
		END
		SET ROWCOUNT 0

		SELECT @LastCnt = COUNT(1) 
		  FROM [dbo].[MFKLocalLog]
				 WHERE ([Header] Like @Filter) 
    
		SELECT @PrevCnt As 'Previous Count', @LastCnt As 'Final Count', (@PrevCnt - @LastCnt) As 'Deleted'
             
    END TRY 
    
    BEGIN CATCH   
            
		SELECT     @ErrLine =  ERROR_LINE()
			 ,  @ErrMsg  =  ERROR_MESSAGE()
		 
		SET @ErrMsg = 'Error at line no ' + @ErrLine + ' ' + @ErrMsg

		RAISERROR(@ErrMsg , 16,1 )       
                 
    END CATCH 
	SET ROWCOUNT 0
END                 

GO

--******************Create SP Usp_DeleteData if not exist************************

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[Usp_DeleteData]    Script Date: 04/29/2014 16:23:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_DeleteData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Usp_DeleteData]
GO

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[Usp_DeleteData]    Script Date: 04/29/2014 16:23:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_DeleteData]  
(
   @DaysToDel   INT = 2
) 

AS


/*

   Created By      : Altaf Hussain 
   Created Date    : 02/24/2014
   Description     : Removes records from table for given days


   EXEC [dbo].[Usp_DeleteData]  @DaysToDel   = 2 

*/

BEGIN

     DECLARE     @LogId      INT 
             ,   @LtDate     DATETIME 
             ,   @Rcnt       INT 
             
             ,   @ErrLine    VARCHAR(10)
             ,   @ErrMsg     VARCHAR(3000) 

     
     BEGIN TRY 
     
              SELECT @LtDate = MAX(LocalTime) FROM [MFKLocalLog].[dbo].[MFKLocalLog](NOLOCK)           
    
             ---------------------------------------------
             -- Fetch latest id of 2 days old 
             ---------------------------------------------
            
             SELECT    @LogId = MAX(MFKLocalLogID)
                    ,  @Rcnt  = COUNT(1) 
             FROM [MFKLocalLog].[dbo].[MFKLocalLog] (NOLOCK) 
             WHERE CONVERT (VARCHAR(12),LocalTime,102)<  CONVERT(VARCHAR(12),DATEADD(DAY,-@DaysToDel,@LtDate) ,102) 
                   
             
             -----------------------------
             -- Remove given days of old data
             -----------------------------
             
             IF @Rcnt <> 0
             
             BEGIN 
             
                 DELETE FROM [MFKLocalLog].[dbo].[MFKLocalLog] WHERE  MFKLocalLogID < = @LogId  AND [SentToServerTime] IS NOT NULL
                 
             END  
             
    END TRY 
    
    BEGIN CATCH   
            
             SELECT     @ErrLine =  ERROR_LINE()
                     ,  @ErrMsg  =  ERROR_MESSAGE()
                 
             SET @ErrMsg = 'Error at line no ' + @ErrLine + ' ' + @ErrMsg
         
             RAISERROR(@ErrMsg , 16,1 )       
                 
    END CATCH 
    
END                 
 

GO


--*****************************Create Table T_DeviceStatusLocal if not exist*********************************************


IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_T_DeviceStatusLocal_iStatusCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[T_DeviceStatusLocal] DROP CONSTRAINT [DF_T_DeviceStatusLocal_iStatusCount]
END

GO

USE [MFKLocalLog]
GO

/****** Object:  Table [dbo].[T_DeviceStatusLocal]    Script Date: 10/30/2014 10:51:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T_DeviceStatusLocal]') AND type in (N'U'))
DROP TABLE [dbo].[T_DeviceStatusLocal]
GO

USE [MFKLocalLog]
GO

/****** Object:  Table [dbo].[T_DeviceStatusLocal]    Script Date: 10/30/2014 10:51:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[T_DeviceStatusLocal](
	[iDeviceStatusID] [int] IDENTITY(1,1) NOT NULL,
	[iKioskId] [int] NOT NULL,
	[vDevice] [varchar](32) NOT NULL,
	[vItemType] [varchar](32) NULL,
	[iItemDenomination] [smallint] NULL,
	[iItemCount] [smallint] NULL,
	[iContainerNumber] [smallint] NULL,
	[vStatus] [varchar](32) NOT NULL,
	[DateReported] [datetime] NOT NULL,
	[DateLogged] [datetime] NOT NULL,
	[iStatusCount] [int] NOT NULL,
 CONSTRAINT [PK_DeviceStatus] PRIMARY KEY CLUSTERED 
(
	[iDeviceStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[T_DeviceStatusLocal] ADD  CONSTRAINT [DF_T_DeviceStatusLocal_iStatusCount]  DEFAULT ((0)) FOR [iStatusCount]
GO

--*****************Create SP GetAcceptorUserReplyCounter if not exist******************************

/****** Object:  StoredProcedure [dbo].[GetAcceptorUserReplyCounter]    Script Date: 10/30/2014 10:53:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAcceptorUserReplyCounter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAcceptorUserReplyCounter]
GO

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[GetAcceptorUserReplyCounter]    Script Date: 10/30/2014 10:53:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Wilson Guasca R. -ASAI Bogota Group
-- Create date: 2014-0-23
-- Description:	Use instead of direct insert a record on the
--   table [T_DeviceStatusLocal]
--   Will create a new record if necessary, or update the existent.
--   Original purpose is to keep track of UserReply 93/99 event for 
--   BillAcceptors.
--   EXEC GetAcceptorUserReplyCounter @iKioskId = '50', @vDevice = 'BillAcceptor2', @vItemType = 'UR093'
-- =============================================

CREATE PROCEDURE [dbo].[GetAcceptorUserReplyCounter]
	@iKioskId int
	,@vDevice varchar(32)
	,@vItemType varchar(32)
	,@iContainerNumber int
AS
BEGIN
	SET NOCOUNT ON;
	IF ((ISNULL(@iKioskId, 0) = '')
		OR (RTRIM(ISNULL(@vDevice, '')) = ''))
		RETURN;

	DECLARE @iStatusCount int 
			,@ItemTypeNotNull varchar(32)
			,@newDeviceStatusID bigint 

	SELECT @ItemTypeNotNull = ISNULL(@vItemType, '')

	--Obtain previous count of status for the device
	--Obtain previous status for the device
	SELECT @newDeviceStatusID = [iDeviceStatusID]
			,@iStatusCount = [iItemCount]
	  FROM [dbo].[T_DeviceStatusLocal]
	 WHERE [iKioskId] = @iKioskId
	   AND [vDevice] = @vDevice
	   AND ISNULL([vItemType], '') = @ItemTypeNotNull
	   AND [iContainerNumber] = @iContainerNumber

	SELECT ISNULL(@iStatusCount, 0) As 'Counter'

	-- ======================== -- */

END -- PROCEDURE 



GO

--*****************Create SP GetAcceptorUserReplyCounter if not exist******************************

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[InsertLocalDeviceStatus]    Script Date: 10/30/2014 11:04:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertLocalDeviceStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertLocalDeviceStatus]
GO

USE [MFKLocalLog]
GO

/****** Object:  StoredProcedure [dbo].[InsertLocalDeviceStatus]    Script Date: 10/30/2014 11:04:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Wilson Guasca R. -ASAI Bogota Group
-- Create date: 2014-0-23
-- Description:	Use instead of direct insert a record on the
--   table [T_DeviceStatusLocal]
--   Will create a new record if necessary, or update the existent.
--   Original purpose is to keep track of UserReply 93/99 event for 
--   BillAcceptors.
--   EXEC InsertLocalDeviceStatus @iKioskId = '50', @vDevice = 'Heartbeat', @vItemType = 'KNOCK OUT', @DateReported = '2014-03-19 20:21', @iItemDenomination = 0, @iItemCount = 0,@iContainerNumber = 0, @vStatus = 'IMHERE'
-- =============================================
CREATE PROCEDURE [dbo].[InsertLocalDeviceStatus]
	@iKioskId int
	,@vDevice varchar(32)
	,@vItemType varchar(32)
	,@iItemDenomination smallint
	,@iItemCount smallint
	,@iContainerNumber smallint
	,@vStatus varchar(32)
	,@DateReported datetime
	,@DateLogged datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF ((ISNULL(@iKioskId, 0) = '')
		OR (RTRIM(ISNULL(@vDevice, '')) = ''))
		RETURN;

	/* --
	-- DECLARATION OF VARIABLES
	DECLARE -- INPUT VARIABLES
	@iKioskId int
	,@vDevice varchar(32)
	,@vItemType varchar(32)
	,@iItemDenomination smallint
	,@iItemCount smallint
	,@iContainerNumber smallint
	,@vStatus varchar(32)
	,@DateReported datetime
	,@DateLogged datetime = GetDate()
	-- --*/

	DECLARE @iStatusCount int 
			,@newDeviceStatusID bigint 
			,@prevStatus varchar(32)
			,@prevDateReported datetime
			,@ItemTypeNotNull varchar(32);

	SELECT @ItemTypeNotNull = ISNULL(@vItemType, '')

	--Obtain previous count of status for the device
	--Obtain previous status for the device
	SELECT @newDeviceStatusID = [iDeviceStatusID]
			,@prevStatus = [vStatus]
			,@iStatusCount = [iStatusCount]
			,@prevDateReported = [DateReported]
	  FROM [dbo].[T_DeviceStatusLocal]
	 WHERE [iKioskId] = @iKioskId
	   AND [vDevice] = @vDevice
	   AND ISNULL([vItemType], '') = @ItemTypeNotNull
	   AND [iContainerNumber] = @iContainerNumber

	SELECT @iStatusCount = ISNULL(@iStatusCount, 0) 

	SELECT @prevStatus = ISNULL(@prevStatus, '')

	--Check and correct previous status for special cases
	IF (@prevStatus = @vStatus AND (@ItemTypeNotNull IN ('BCR', 'USD', 'RETRACT')
		 OR (@ItemTypeNotNull LIKE 'OPER.%')))
	BEGIN 
		SELECT @prevStatus = 'NOT-' + ISNULL(@vStatus, '')
	END

	-- DEBUG 01
	--SELECT @iStatusCount aS 'StatusCount', @prevStatus As 'prevStatus', @vStatus As 'vStatus'

	SELECT @DateLogged = ISNULL(@DateLogged, GetDate())

	/* -- ======================== --
	IF (NOT @prevStatus = @vStatus)
	BEGIN 
	
		-- DEBUG 02
		--SELECT (@iStatusCount + 1) aS 'StatusCount' ,@vDevice As 'vDevice', @vStatus As 'vStatus'
		
		-- INSERT THE 'HISTORICAL' RECORD ONLY IF IT DOESN'T EXIST
		IF NOT 
		EXISTS(SELECT [iDeviceStatusID] 
			  FROM [dbo].[T_DeviceStatusLocal]
			 WHERE [iKioskId] = @iKioskId
			   AND [vDevice] = @vDevice
			   AND ISNULL([vItemType], '') = @ItemTypeNotNull
			   AND [iContainerNumber] = @iContainerNumber
			   AND [DateReported] = @DateReported)
		BEGIN 
		
			INSERT INTO [dbo].[T_DeviceStatusLocal]
					   ([iKioskId]
					   ,[vDevice]
					   ,[vItemType]
					   ,[iItemDenomination]
					   ,[iItemCount]
					   ,[iContainerNumber]
					   ,[vStatus]
					   ,[DateReported]
					   ,[DateLogged]
					   ,[iStatusCount])
				 VALUES
					   (@iKioskId
					   ,@vDevice
					   ,@vItemType
					   ,@iItemDenomination
					   ,@iItemCount
					   ,@iContainerNumber
					   ,@vStatus
					   ,@DateReported
					   ,@DateLogged
					   ,@iStatusCount + 1)
		
			SELECT @newDeviceStatusID = IDENT_CURRENT('T_DeviceStatusLocal')
		END
	END
	-- DEBUG 03
	--SELECT @newDeviceStatusID aS 'newDeviceStatusID' ,@vDevice As 'vDevice', @vItemType As 'vItemType'
	-- ======================== -- */

	-- UPDATE/create UNIQUE RECORD FOR STATUS QUERIES

	IF (@prevDateReported IS NULL OR 0 = @newDeviceStatusID)
		BEGIN 
		INSERT INTO [dbo].[T_DeviceStatusLocal]
				   ([iKioskId]
				   ,[vDevice]
				   ,[vItemType]
				   ,[iItemDenomination]
				   ,[iItemCount]
				   ,[iContainerNumber]
				   ,[vStatus]
				   ,[DateReported]
				   ,[DateLogged]
				   ,[iStatusCount])
			 VALUES
				   (@iKioskId
				   ,@vDevice
				   ,@vItemType
				   ,@iItemDenomination
				   ,@iItemCount
				   ,@iContainerNumber
				   ,@vStatus
				   ,@DateReported
				   ,@DateLogged
				   ,@iStatusCount + 1)

		SELECT @newDeviceStatusID = IDENT_CURRENT('T_DeviceStatusLocal')
		END
	ELSE
		BEGIN 
			IF (@prevDateReported < @DateReported)
			UPDATE [dbo].[T_DeviceStatusLocal] SET
					   [iItemDenomination] = @iItemDenomination
					   ,[iItemCount] = @iItemCount
					   ,[vStatus] = @vStatus
					   ,[DateReported] = @DateReported
					   ,[DateLogged] = @DateLogged
					   ,[iStatusCount] = @iStatusCount --+ 1
					   ,[iContainerNumber] = @iContainerNumber
			 WHERE (@newDeviceStatusID = [iDeviceStatusID])
			 --WHERE [iKioskId] = @iKioskId
			 --  AND [vDevice] = @vDevice
			 --  AND ISNULL([vItemType], '') = @ItemTypeNotNull
			 --  AND [iContainerNumber] = @iContainerNumber
		END
	SELECT @newDeviceStatusID As 'RecordID';
END -- PROCEDURE 

GO


--*************************** END **************************************