﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace InstallationWizard

{
    public class CashAdvanceService
    {
        //Validate Certificate
        private bool ValidateServerCertficate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate cert,
            System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        private bool _securityMode;
        private Uri _url;
        private string _macAddress;
        public Uri URL
        {
            get { return _url; }
            set { _url = value; }
        }

        public bool SecurityMode
        {
            get { return _securityMode; }
            set { _securityMode = value; }
        }

        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        private BasicHttpBinding CreateBinding()
        {
            BasicHttpBinding httpBinding = new BasicHttpBinding();
            httpBinding.Name = "IDSCashAdvanceEndpoint";
            TimeSpan UsedTimeConfig = new TimeSpan(0, 1, 0);
            httpBinding.CloseTimeout = UsedTimeConfig;
            httpBinding.OpenTimeout = UsedTimeConfig;
            httpBinding.SendTimeout = UsedTimeConfig;
            UsedTimeConfig = new TimeSpan(0, 10, 0);
            httpBinding.ReceiveTimeout = UsedTimeConfig;
            httpBinding.AllowCookies = false;
            httpBinding.BypassProxyOnLocal = false;
            httpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            httpBinding.MaxBufferSize = 65536;
            httpBinding.MaxBufferPoolSize = 524288;
            httpBinding.MaxReceivedMessageSize = 65536;
            httpBinding.MessageEncoding = WSMessageEncoding.Text;
            httpBinding.TextEncoding = Encoding.UTF8;
            httpBinding.TransferMode = TransferMode.Buffered;
            httpBinding.UseDefaultWebProxy = true;
            System.Xml.XmlDictionaryReaderQuotas ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas();
            ReaderQuotas.MaxDepth = 32;
            ReaderQuotas.MaxStringContentLength = 8192;
            ReaderQuotas.MaxArrayLength = 16384;
            ReaderQuotas.MaxBytesPerRead = 4096;
            ReaderQuotas.MaxNameTableCharCount = 16384;
            httpBinding.ReaderQuotas = ReaderQuotas;
            BasicHttpSecurity httpSecurity = httpBinding.Security;

            if (SecurityMode)
                httpSecurity.Mode = BasicHttpSecurityMode.Transport;
            else
                httpSecurity.Mode = BasicHttpSecurityMode.None;

            HttpTransportSecurity TransportSecurity = httpSecurity.Transport;
            TransportSecurity.ClientCredentialType = HttpClientCredentialType.None;
            TransportSecurity.ProxyCredentialType = HttpProxyCredentialType.None;
            TransportSecurity.Realm = "";
            BasicHttpMessageSecurity MessageSecurity = httpSecurity.Message;
            MessageSecurity.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            MessageSecurity.AlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Default;
            return httpBinding;
        }

        public IDSCashAdvanceDataContract.TerminalConfiguration TerminalConfigRequest()
        {
            BasicHttpBinding httpBinding = CreateBinding();
            EndpointAddress endPoint = new EndpointAddress(URL);
            DSCashAdvanceServiceContractClient ids = new DSCashAdvanceServiceContractClient(httpBinding, endPoint);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertficate;
            ids.Open();
            IDSCashAdvanceDataContract.TerminalConfiguration terminalConfig = ids.GetTerminalConfiguration(MacAddress);
            ids.Close();
            return terminalConfig;
        }

        public IDSCashAdvanceDataContract.FeesStructure FeeRequest()
        {
            BasicHttpBinding httpBinding = CreateBinding();
            EndpointAddress endPoint = new EndpointAddress(URL);
            DSCashAdvanceServiceContractClient ids = new DSCashAdvanceServiceContractClient(httpBinding, endPoint);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertficate;
            ids.Open();
            IDSCashAdvanceDataContract.TerminalConfiguration terminalConfig = ids.GetTerminalConfiguration(MacAddress);
            IDSCashAdvanceDataContract.FeesStructure feeConfig = ids.GetFees(terminalConfig.TerminalID);
            ids.Close();
            return feeConfig;
        }
        
        public IDSCashAdvanceDataContract.FeesStructure FeeRequest(string terminalId)
        {
            BasicHttpBinding httpBinding = CreateBinding();
            EndpointAddress endPoint = new EndpointAddress(URL);
            DSCashAdvanceServiceContractClient ids = new DSCashAdvanceServiceContractClient(httpBinding, endPoint);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertficate;
            ids.Open();
            IDSCashAdvanceDataContract.FeesStructure feeConfig = ids.GetFees(terminalId);
            ids.Close();
            return feeConfig;
        }

        public IDSCashAdvanceDataContract.TransactionResponse ApprovalRequest(ref IDSCashAdvanceDataContract.Transaction trn)
        {
            BasicHttpBinding httpBinding = CreateBinding();
            EndpointAddress endPoint = new EndpointAddress(URL);
            DSCashAdvanceServiceContractClient ids = new DSCashAdvanceServiceContractClient(httpBinding, endPoint);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertficate;
            ids.Open();
            IDSCashAdvanceDataContract.TerminalConfiguration terminalConfig = ids.GetTerminalConfiguration(MacAddress);
            trn.TerminalID = terminalConfig.TerminalID;
            IDSCashAdvanceDataContract.TransactionResponse ret = ids.Execute(trn);
            ids.Close();
            return ret;
        }

        public IDSCashAdvanceDataContract.TransactionResponse ApprovalRequest(string terminalId,IDSCashAdvanceDataContract.Transaction trn)
        {
            BasicHttpBinding httpBinding = CreateBinding();
            EndpointAddress endPoint = new EndpointAddress(URL);
            DSCashAdvanceServiceContractClient ids = new DSCashAdvanceServiceContractClient(httpBinding, endPoint);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertficate;
            ids.Open();
            trn.TerminalID = terminalId;
            IDSCashAdvanceDataContract.TransactionResponse ret = ids.Execute(trn);
            ids.Close();
            return ret;
        }
    }
}