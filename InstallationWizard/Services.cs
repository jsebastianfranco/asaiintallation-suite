﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceProcess;

namespace InstallationWizard
{
    public partial class Services : UserControl
    {
        private static Services _instance;
        public static Services Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Services();
                return _instance;
            }
        }

        public Services()
        {
            InitializeComponent();
            ServiceController[] services = ServiceController.GetServices();

            // try to find service name
            foreach (ServiceController servicioWindows in services)
            {
                if (servicioWindows.ServiceName.ToUpper().StartsWith("MFK"))
                {
                    if (servicioWindows != null)
                    {
                        try
                        {
                            lblServices.Text += servicioWindows.ServiceName + " " + servicioWindows.Status.ToString() + "\n";
                        }
                        catch (Exception ex)
                        {
                            lblServices.Text = ex.Message + "\n";
                        }
                    }
                }
            }
        }

        private void Services_Load(object sender, EventArgs e)
        {

        }

        private void btnStartServices_Click(object sender, EventArgs e)
        {
            string statres = "";
            // MFKServices

            lblServices.Text = "";

            ServiceController[] services = ServiceController.GetServices();

            // try to find service name
            foreach (ServiceController servicioWindows in services)
            {
                if (servicioWindows.ServiceName.ToUpper().StartsWith("MFK"))
                {
                    lblServices.Text += "Starting " + servicioWindows.ServiceName + "...";
                    try
                    {
                        if (servicioWindows != null &&
                     servicioWindows.Status == ServiceControllerStatus.Stopped)
                        {
                            servicioWindows.Start();
                        }
                        servicioWindows.WaitForStatus(ServiceControllerStatus.Running);
                        servicioWindows.Close();
                        statres = servicioWindows.ServiceName + " Started.\n";
                    }
                    catch (Exception ex)
                    {
                        statres = ("Error starting service " + servicioWindows.ServiceName + ": " + ex.Message + ".\n");
                    }
                    lblServices.Text += statres;
                }
            }
        }

        private void btnStopServices_Click(object sender, EventArgs e)
        {
            string statres = "";
            // MFKServices

            lblServices.Text = "";

            ServiceController[] services = ServiceController.GetServices();

            // try to find service name
            foreach (ServiceController servicioWindows in services)
            {
                if (servicioWindows.ServiceName.ToUpper().StartsWith("MFK"))
                {
                    lblServices.Text += "Stoping " + servicioWindows.ServiceName + "...";
                    try
                    {
                        if (servicioWindows != null &&
                             servicioWindows.Status == ServiceControllerStatus.Running)
                        {
                            servicioWindows.Stop();
                        }
                        servicioWindows.WaitForStatus(ServiceControllerStatus.Stopped);
                        servicioWindows.Close();
                        statres = servicioWindows.ServiceName + " Stopped.\n";
                    }
                    catch (Exception ex)
                    {
                        statres = ("Error stoping service " + servicioWindows.ServiceName + ": " + ex.Message + ".\n");
                    }
                    lblServices.Text += statres;
                }
            }
        }

        private void btnRestartServices_Click(object sender, EventArgs e)
        {
            string statres = "";
            // MFKServices

            lblServices.Text = "";

            ServiceController[] services = ServiceController.GetServices();

            // try to find service name
            foreach (ServiceController servicioWindows in services)
            {
                if (servicioWindows.ServiceName.ToUpper().StartsWith("MFK"))
                {
                    lblServices.Text += "Stoping " + servicioWindows.ServiceName + "...";
                    try
                    {
                        if (servicioWindows != null &&
                             servicioWindows.Status == ServiceControllerStatus.Running)
                        {
                            servicioWindows.Stop();
                        }
                        servicioWindows.WaitForStatus(ServiceControllerStatus.Stopped);
                        servicioWindows.Close();
                    }
                    catch (Exception ex)
                    {
                        statres = ("Error stoping service " + servicioWindows.ServiceName + ": " + ex.Message + ".\n");
                    }
                    lblServices.Text += "Starting " + servicioWindows.ServiceName + "...";
                    try
                    {
                        if (servicioWindows != null &&
                     servicioWindows.Status == ServiceControllerStatus.Stopped)
                        {
                            servicioWindows.Start();
                        }
                        servicioWindows.WaitForStatus(ServiceControllerStatus.Running);
                        servicioWindows.Close();
                        statres = servicioWindows.ServiceName + " Restarted.\n";
                    }
                    catch (Exception ex)
                    {
                        statres = ("Error Restarting service " + servicioWindows.ServiceName + ": " + ex.Message + ".\n");
                    }
                    lblServices.Text += statres;
                }
            }
        }
    }
}
