﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using InstallationWizardClass;
using Ionic.Zip;
using System.Threading;
using System.Diagnostics;

namespace InstallationWizard
{
    public partial class Pf4OneTouch : UserControl
    {
        private static Pf4OneTouch _instance;
        public int timer;
        public Pf4OneTouch()
        {
            InitializeComponent();
        }

        public static Pf4OneTouch Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Pf4OneTouch();
                return _instance;
            }
        }


        private void Pf4OneTouch_Load(object sender, EventArgs e)
        {
            circularProgressBar1.Value = 0;
            circularProgressBar1.Minimum = 0;
            circularProgressBar1.Maximum = 100;
        }
        private void ReadFilePaths()
        {
            InitializeComponent();
            string files = string.Empty;
            string paths = string.Empty;
            timer = 1;
            //Show the files and folders to process
            files = files + "\n" + "Copied Touch Files: " + "\n" + "\n";
            int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["TouchCount"]);


            for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            {
                string app = "Touch" + rowCounter;
                files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app]) + "\n";
                paths = (System.IO.Path.Combine(ConfigurationManager.AppSettings[app])).Trim();
                timer += 1;
                //MessageBox.Show(paths);
                //call copy function and send the paths


                if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch\"+ rowCounter))
                {
                    Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch\" + rowCounter);
                }


                CopyFiles(paths, @"C:\ASAI\OneTouchPF4\OneTouch\"+rowCounter);
            }

            circularProgressBar1.Visible = true;

            for (int i = 1; i <= 24; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
                ResultPath.Visible = false;
            }
            Thread.Sleep(1000);

            files = files + "\n" + timer;
            timer += 1;
            lblOneTFiles.Text = files;


        }

        public static void CopyFiles(string sourceDirectory, string destinationPath)
        {
            DirectoryInfo dirSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo dirDestination = new DirectoryInfo(destinationPath);

            CopyAllFiles(dirSource, dirDestination);
        }


        public static void CopyAllFiles(DirectoryInfo source, DirectoryInfo destination)
        {

            try
            {
                foreach (FileInfo fi in source.GetFiles())
                {
                    wLogging.log(@"Copying the file {0}\{1}" + destination.FullName + fi.Name);
                    fi.CopyTo(Path.Combine(destination.FullName, fi.Name), true);
                }

                // Copy each subdirectory using recursion.
                foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
                {
                    DirectoryInfo nextTargetSubDir = destination.CreateSubdirectory(diSourceSubDir.Name);
                    CopyAllFiles(diSourceSubDir, nextTargetSubDir);
                }
            }
            catch (Exception err)
            {
                 wLogging.log("Error Trying copy the source files into destination folder : " + err);
            }


        }

        public void CompressFolder()
        {
            try
            {
                string startPath = @"C:\ASAI\OneTouchPF4\OneTouch";
                string zipPath = @"C:\ASAI\OneTouchPF4\OneTouch" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".zip";

                System.IO.Compression.ZipFile.CreateFromDirectory(startPath, zipPath);
            }
            catch (Exception err)
            {
                MessageBox.Show("Error Trying compress the source files into destination folder", "Touch File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wLogging.log("Error Trying compress the source files into destination folder : " + err);
            }

        }

        public void deleteTouchFolder()
        {
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(@"C:\ASAI\OneTouchPF4\OneTouch");

                if (Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch"))
                {
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Error Trying delete the source  folder", "Touch File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wLogging.log("Error Trying delete the source folder : " + err);
            }

        }

        private void CreateOneT()
        {
            MessageBox.Show("The Coping process has started, please press OK and wait.", "Touch File", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!Directory.Exists(@"C:\ASAI\OneTouchPF4\OneTouch"))
            {
                Directory.CreateDirectory(@"C:\ASAI\OneTouchPF4\OneTouch");
            }



            ReadFilePaths();

            for (int i = 24; i <= 68; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
            }

            CompressFolder();


            for (int i = 68; i <= 100; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
            }


            Thread.Sleep(1000);
            //MessageBox.Show("The Coping process has ended successfully.", "Touch File", MessageBoxButtons.OK, MessageBoxIcon.Information);
            deleteTouchFolder();

            if (circularProgressBar1.Value == 100)
            {
                lblProgressBar.Text = "Completed";
            }

            ResultPath.Visible = true;
            ResultPath.Text = @"C:\ASAI\OneTouchPF4\OneTouch";

        }


        private void btnClean_Click(object sender, EventArgs e)
        {
            CreateOneT();

        }

        private void ResultPath_Click(object sender, EventArgs e)
        {

        }
    }


}


