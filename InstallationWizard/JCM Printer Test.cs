﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstallationWizard
{
    public partial class JCM_Printer_Test : UserControl
    {
        private static JCM_Printer_Test _instance;
        public static JCM_Printer_Test Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new JCM_Printer_Test();
                return _instance;
            }
        }

        public JCM_Printer_Test()
        {
            InitializeComponent();
        }
        private void JCM_Printer_Test_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\ASAI\jcmPrinter\WfTclPrinter.exe");
        }
    }
}
