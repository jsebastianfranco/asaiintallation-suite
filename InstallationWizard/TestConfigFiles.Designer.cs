﻿namespace InstallationWizard
{
    partial class TestConfigFiles
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestConfigFiles));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHealth = new System.Windows.Forms.Label();
            this.bwBackgroundWorkerKioskInfo = new System.ComponentModel.BackgroundWorker();
            this.bwBackgroundWorkerNew = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.lblKioskId = new System.Windows.Forms.Label();
            this.lblKioskIp = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTicketServerIp = new System.Windows.Forms.Label();
            this.lblTicketServerName = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblLoIp = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(434, 386);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(144, 40);
            this.simpleButton1.TabIndex = 85;
            this.simpleButton1.Text = "Test";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(14, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(416, 39);
            this.label2.TabIndex = 88;
            this.label2.Text = "Mode: Config Files Test.";
            // 
            // lblHealth
            // 
            this.lblHealth.AutoSize = true;
            this.lblHealth.BackColor = System.Drawing.Color.Transparent;
            this.lblHealth.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblHealth.Location = new System.Drawing.Point(38, 402);
            this.lblHealth.Name = "lblHealth";
            this.lblHealth.Size = new System.Drawing.Size(0, 13);
            this.lblHealth.TabIndex = 84;
            // 
            // bwBackgroundWorkerKioskInfo
            // 
            this.bwBackgroundWorkerKioskInfo.WorkerReportsProgress = true;
            this.bwBackgroundWorkerKioskInfo.WorkerSupportsCancellation = true;
            this.bwBackgroundWorkerKioskInfo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwBackgroundWorkerKioskInfo_DoWork);
            // 
            // bwBackgroundWorkerNew
            // 
            this.bwBackgroundWorkerNew.WorkerReportsProgress = true;
            this.bwBackgroundWorkerNew.WorkerSupportsCancellation = true;
            this.bwBackgroundWorkerNew.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwBackgroundWorkerKioskInfo_DoWork);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(41, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 89;
            this.label1.Text = "Kiosk Id:";
            // 
            // lblKioskId
            // 
            this.lblKioskId.AutoSize = true;
            this.lblKioskId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKioskId.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblKioskId.Location = new System.Drawing.Point(117, 167);
            this.lblKioskId.Name = "lblKioskId";
            this.lblKioskId.Size = new System.Drawing.Size(0, 20);
            this.lblKioskId.TabIndex = 90;
            // 
            // lblKioskIp
            // 
            this.lblKioskIp.AutoSize = true;
            this.lblKioskIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKioskIp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblKioskIp.Location = new System.Drawing.Point(116, 218);
            this.lblKioskIp.Name = "lblKioskIp";
            this.lblKioskIp.Size = new System.Drawing.Size(0, 20);
            this.lblKioskIp.TabIndex = 92;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(40, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 91;
            this.label5.Text = "Kiosk Ip:";
            // 
            // lblTicketServerIp
            // 
            this.lblTicketServerIp.AutoSize = true;
            this.lblTicketServerIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTicketServerIp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTicketServerIp.Location = new System.Drawing.Point(480, 218);
            this.lblTicketServerIp.Name = "lblTicketServerIp";
            this.lblTicketServerIp.Size = new System.Drawing.Size(0, 20);
            this.lblTicketServerIp.TabIndex = 96;
            // 
            // lblTicketServerName
            // 
            this.lblTicketServerName.AutoSize = true;
            this.lblTicketServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTicketServerName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTicketServerName.Location = new System.Drawing.Point(480, 167);
            this.lblTicketServerName.Name = "lblTicketServerName";
            this.lblTicketServerName.Size = new System.Drawing.Size(0, 20);
            this.lblTicketServerName.TabIndex = 94;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(312, 218);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 20);
            this.label9.TabIndex = 93;
            this.label9.Text = "Ticket Server Ip:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(39, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 25);
            this.label10.TabIndex = 97;
            this.label10.Text = "Kiosk.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(311, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(208, 25);
            this.label11.TabIndex = 98;
            this.label11.Text = "Ticket Redemption";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(40, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 25);
            this.label12.TabIndex = 99;
            this.label12.Text = "Live Office";
            // 
            // lblLoIp
            // 
            this.lblLoIp.AutoSize = true;
            this.lblLoIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoIp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblLoIp.Location = new System.Drawing.Point(158, 325);
            this.lblLoIp.Name = "lblLoIp";
            this.lblLoIp.Size = new System.Drawing.Size(0, 20);
            this.lblLoIp.TabIndex = 101;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(41, 325);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 20);
            this.label14.TabIndex = 100;
            this.label14.Text = "Live office Ip:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(312, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 20);
            this.label7.TabIndex = 102;
            this.label7.Text = "Ticket Server Name:";
            // 
            // TestConfigFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblLoIp);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblTicketServerIp);
            this.Controls.Add(this.lblTicketServerName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblKioskIp);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblKioskId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblHealth);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TestConfigFiles";
            this.Size = new System.Drawing.Size(694, 457);
            this.Load += new System.EventHandler(this.TestConfigFiles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblHealth;
        private System.ComponentModel.BackgroundWorker bwBackgroundWorkerKioskInfo;
        private System.ComponentModel.BackgroundWorker bwBackgroundWorkerNew;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblKioskId;
        private System.Windows.Forms.Label lblKioskIp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTicketServerIp;
        private System.Windows.Forms.Label lblTicketServerName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblLoIp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label7;
    }
}
