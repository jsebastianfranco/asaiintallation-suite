﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InstallationWizard
{
    public partial class InstallationWizardF : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        string  HelpPath = System.IO.Path.Combine(Application.StartupPath, "WizardManual.chm");
        public InstallationWizardF()
        {
            InitializeComponent();
            
        }

        private void InstallationWizardF_Load(object sender, EventArgs e)
        {
            
            System.Threading.Thread.Sleep(1000);
            Screen[] screens = Screen.AllScreens;
            this.Location = screens[1].WorkingArea.Location;//Screen.AllScreens[1].WorkingArea.Location;
            
            this.KeyPreview = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(myKeyPress);

        }

        private void accordionControlElement1_Click(object sender, EventArgs e)
        {

        }

        private void accordionControlElement13_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void accordionControlElement5_Click(object sender, EventArgs e)
        {

        }

        private void accordionControlElement8_Click(object sender, EventArgs e)
        {

        }

        //not called
        private void accordionControlElement20_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(TestFullConfigFiles.Instance))
            {
                panel.Controls.Add(TestFullConfigFiles.Instance);
                TestFullConfigFiles.Instance.Dock = DockStyle.Fill;
                TestFullConfigFiles.Instance.BringToFront();
            }
            else
                TestFullConfigFiles.Instance.BringToFront();
        }

        private void accordionControlElement14_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(BackUp.Instance))
            {
                panel.Controls.Add(BackUp.Instance);
                BackUp.Instance.Dock = DockStyle.Fill;
                BackUp.Instance.BringToFront();
            }
            else
                BackUp.Instance.BringToFront();
        }

        private void accordionControlElement18_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(Services.Instance))
            {
                panel.Controls.Add(Services.Instance);
                Services.Instance.Dock = DockStyle.Fill;
                Services.Instance.BringToFront();
            }
            else
                Services.Instance.BringToFront();
        }

        private void accordionControlElement15_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(Restorev2.Instance))
            {
                panel.Controls.Add(Restorev2.Instance);
                Restorev2.Instance.Dock = DockStyle.Fill;
                Restorev2.Instance.BringToFront();
            }
            else
                Restorev2.Instance.BringToFront();
        }

        private void accordionControlElement16_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(Clean.Instance))
            {
                panel.Controls.Add(Clean.Instance);
                Clean.Instance.Dock = DockStyle.Fill;
                Clean.Instance.BringToFront();
            }
            else
                Clean.Instance.BringToFront();
        }

        private void accordionControlElement17_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(OnlyConfig.Instance))
            {
                panel.Controls.Add(OnlyConfig.Instance);
                OnlyConfig.Instance.Dock = DockStyle.Fill;
                OnlyConfig.Instance.BringToFront();
            }
            else
                OnlyConfig.Instance.BringToFront();
        }

        private void accordionControlElement19_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(TestConfigFiles.Instance))
            {
                panel.Controls.Add(TestConfigFiles.Instance);
                TestConfigFiles.Instance.Dock = DockStyle.Fill;
                TestConfigFiles.Instance.BringToFront();
            }
            else
                TestConfigFiles.Instance.BringToFront();
        }

        private void accordionControlElement21_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(JCM_Printer_Test.Instance))
            {
                panel.Controls.Add(JCM_Printer_Test.Instance);
                JCM_Printer_Test.Instance.Dock = DockStyle.Fill;
                JCM_Printer_Test.Instance.BringToFront();
            }
            else
                JCM_Printer_Test.Instance.BringToFront();

            System.Diagnostics.Process.Start(@"C:\ASAI\jcmPrinter\WfTclPrinter.exe");
        }

        private void accordionControlElement22_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(BvsTester.Instance))
            {
                panel.Controls.Add(BvsTester.Instance);
                BvsTester.Instance.Dock = DockStyle.Fill;
                BvsTester.Instance.BringToFront();
            }
            else
                BvsTester.Instance.BringToFront();

            System.Diagnostics.Process.Start(@"C:\ASAI\BillValidator\TestMei\AsaiMeiBvTester.exe");
        }

        private void accordionControlElement23_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(TracesService.Instance))
            {
                panel.Controls.Add(TracesService.Instance);
                TracesService.Instance.Dock = DockStyle.Fill;
                TracesService.Instance.BringToFront();
            }
            else
                TracesService.Instance.BringToFront();
        }

        private void accordionControlElement24_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(TestDBConn.Instance))
            {
                panel.Controls.Add(TestDBConn.Instance);
                TestDBConn.Instance.Dock = DockStyle.Fill;
                TestDBConn.Instance.BringToFront();
            }
            else
                TestDBConn.Instance.BringToFront();
        }

        private void accordionControlElement25_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(DbRecovery.Instance))
            {
                panel.Controls.Add(DbRecovery.Instance);
                DbRecovery.Instance.Dock = DockStyle.Fill;
                DbRecovery.Instance.BringToFront();
            }
            else
                DbRecovery.Instance.BringToFront();
        }

        private void accordionControlElement26_Click(object sender, EventArgs e)
        {
            
            if (!panel.Controls.Contains(DbTransactionsRecovery.Instance))
            {
                panel.Controls.Add(DbTransactionsRecovery.Instance);
                DbTransactionsRecovery.Instance.Dock = DockStyle.Fill;
                DbTransactionsRecovery.Instance.BringToFront();
            }
            else
                DbTransactionsRecovery.Instance.BringToFront();

        }

        private void accordionControlElement27_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(CheckConnections.Instance))
            {
                panel.Controls.Add(CheckConnections.Instance);
                CheckConnections.Instance.Dock = DockStyle.Fill;
                CheckConnections.Instance.BringToFront();
            }
            else
                CheckConnections.Instance.BringToFront();

        }

        private void help_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"C:\ASAI\InstallationWizard\Help\WizardManual.chm");
        }

        protected void myKeyPress(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Help.ShowHelp(this, @"C:\ASAI\InstallationWizard\Help\WizardManual.chm");
            }
        }

        private void InstallationWizardF_Load_1(object sender, EventArgs e)
        {
            this.Location = Screen.AllScreens[0].WorkingArea.Location;
        }

        private void accordionControlElement28_Click(object sender, EventArgs e)
        {
            if (!panel.Controls.Contains(Pf4OneTouch.Instance))
            {
                panel.Controls.Add(Pf4OneTouch.Instance);
                Pf4OneTouch.Instance.Dock = DockStyle.Fill;
                Pf4OneTouch.Instance.BringToFront();
            }
            else
                Pf4OneTouch.Instance.BringToFront();
        }
    }
}
