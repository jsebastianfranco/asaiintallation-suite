﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace InstallationWizard
{
    public partial class SplashScreen1 : SplashScreen
    {
        int i = 0;
        public SplashScreen1()
        {
            InitializeComponent();
            this.labelCopyright.Text = "Copyright ©" + DateTime.Now.Year.ToString();
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum SplashScreenCommand
        {
        }

        private void labelCopyright_Click(object sender, EventArgs e)
        {

        }

        private void SplashScreen1_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = true;
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // MessageBox.Show(timer1.Interval.ToString());
            i += 1;
            if(i==4)
            {
                InstallationWizardF cls = new InstallationWizardF();
                cls.Show();
                this.Hide();
                timer1.Enabled = false;
            }
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }
    }
}