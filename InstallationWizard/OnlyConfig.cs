﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Text.RegularExpressions;
using InstallationWizardClass;
using System.Threading;
using System.Security.AccessControl;
using System.IO;

namespace InstallationWizard
{
    public partial class OnlyConfig : UserControl
    {
        private static OnlyConfig _instance;

        public int timer;
        public string showStatus, resultStatus, bp, kioskstatus, starting;
        public static OnlyConfig Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new OnlyConfig();
                return _instance;
            }
        }

        public OnlyConfig()
        {
            InitializeComponent();
            txtConfigKioskId.Focus();
            //var machineName = Environment.MachineName;
            //txtConfigLOPort.Text = machineName;
            //lblFiles.Text = "Create New: \n";

            timer = 0;
            int ct = 0;
            //Folder permisions
            timer += 1;
            ct += 1;
            //lblFiles.Text += ct + ".Create File: " + System.IO.Path.GetFileName(ConfigurationManager.AppSettings["BOSWebSvcSettings"]) + "\n";
            timer += 1;
            ct += 1;
            //lblFiles.Text += ct + ".Create File: " + System.IO.Path.GetFileName(ConfigurationManager.AppSettings["KioskWebSvcSettings"]) + "\n";
            timer += 1;
            ct += 1;
            //lblFiles.Text += ct + ".Create File: " + System.IO.Path.GetFileName(ConfigurationManager.AppSettings["KioskWinService.Properties"]) + "\n";
            timer += 1;
            ct += 1;
            //lblFiles.Text += ct + ".Create File: " + System.IO.Path.GetFileName(ConfigurationManager.AppSettings["MFKSyncroSvcSettings"]) + "\n";
            timer += 1;
            ct += 1;
            //lblFiles.Text += ct + ".Create File: " + System.IO.Path.GetFileName(ConfigurationManager.AppSettings["MFKSynchroModuleSvc"]) + "\n";


         //   lblFiles.Visible = false;
        }


        private void OnlyConfig_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            circularProgressBar1.Visible = false;
            //Validate liveofficeurl 
            var foundMatch = Regex.IsMatch(txtConfigLiveOfficeUrl.Text, @"[0-9.]+(:(6553[0-5]|655[0-2][0-9]|65[0-4][0-9][0-9]|6[0-4][0-9][0-9][0-9]|\d{2,4}|[1-9]))?");
            if (foundMatch)
            {
                if ((txtConfigLOPort.Text == "") || (txtConfigServicePort.Text == ""))
                {
                    if (txtConfigLOPort.Text == "")
                    {
                        MessageBox.Show("Please put LO port");
                        txtConfigLOPort.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Please put Service port");
                        txtConfigServicePort.Focus();
                    }
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure?", "CONFIRMATION", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string folder = ConfigurationManager.AppSettings["ASAIFOLDER"];
                        //  lblProgressBar.Text = "Config Kiosk Started\n\n0. Start giving permision to " + folder;
                        //btnCreate.Enabled = false;
                        //Thread.Sleep(1000);
                        //bwBackgroundWorkerConfigs.RunWorkerAsync();

                        circularProgressBar1.Visible = true;

                        for (int i = 1; i <= 70; i++)
                        {
                            Thread.Sleep(20);
                            circularProgressBar1.Value = i;
                            circularProgressBar1.Update();
                            circularProgressBar1.Text = i.ToString() + "%";
                        }


                        Configs();

                        for (int i = 70; i <= 80; i++)
                        {
                            Thread.Sleep(20);
                            circularProgressBar1.Value = i;
                            circularProgressBar1.Update();
                            circularProgressBar1.Text = i.ToString() + "%";
                        }

                        Thread.Sleep(1000);

                        for (int i = 80; i <= 100; i++)
                        {
                            Thread.Sleep(20);
                            circularProgressBar1.Value = i;
                            circularProgressBar1.Update();
                            circularProgressBar1.Text = i.ToString() + "%";
                        }


                        if (circularProgressBar1.Value == 100)
                        {
                            lblProgressBar.Text = "Completed";
                        }


                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        lblConfigKioskId.Focus();
                    }
                }

            }
            else
            {
                MessageBox.Show("The url address you have entered is incorrect!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        protected void Configs()
        {
            circularProgressBar1.Visible = true;
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            string liveUrl = txtConfigLiveOfficeUrl.Text.Trim() + ":" + txtConfigLOPort.Text.Trim(), kioskID = txtConfigKioskId.Text, moduleUrl = txtConfigLiveOfficeUrl.Text.Trim() + ":" + txtConfigServicePort.Text.Trim();
            //decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;
            string sWebLOProtocol = "";

            if (chbConfigHttpLO.Checked)
                sWebLOProtocol = "https";
            else
                sWebLOProtocol = "http";

            string sWebServiceProtocol = "";

            if (chbConfigHttpService.Checked)
                sWebServiceProtocol = "https";
            else
                sWebServiceProtocol = "http";
            //Check in database to make sure this scripts exists.
            //showStatus += sq.ScriptsDataBase();
            //countTimer += 1;

            //Time when start the work
            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            //Information for new files
            string serviceIntervalMinutes = "5";
            //string sourcePath = "";
            string backupPath = System.IO.Path.Combine(ConfigurationManager.AppSettings["ASAIFOLDER"], "BackUpConfigFiles" + (DateTime.Now.ToString("yyyyMMddHHmmss")));
            status = "Config Installation Started\n\nKioskID = " + kioskID + "\nLiveOfficeURL = " + sWebLOProtocol + "://" + liveUrl + "\n\n";
            string path = "";


            if (bContinue)
            {
                showStatus = "0.Start Giving permision to " + ConfigurationManager.AppSettings["ASAIFOLDER"];
                //Show Progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                Thread.Sleep(1000);
                //Try to give permision to Backup Folder
                try
                {
                    //If backup folder don't exist create it
                    if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["ASAIFOLDER"]))
                    {
                        System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    }
                    //Give per|mision to Asai Folder Backup
                    path = (ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    DirectorySecurity ds = Directory.GetAccessControl(path);
                    FileSystemAccessRule fs = new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow);
                    ds.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
                    Directory.SetAccessControl(path, ds);
                    showStatus = "0. Give permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + " Successful\n";
                }
                catch (Exception ex)
                {
                    showStatus = "0.Error giving permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". Error: " + ex.Message + "\n";
                    bContinue = false;
                }
                //Give 1 second for apply the permisions
                Thread.Sleep(1000);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;

                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));

            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["BOSWebSvcSettings"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_BOSWebSvcSettings(liveUrl, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["KioskWebSvcSettings"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_KioskWebSvcSettings(liveUrl, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["KioskWinService.Properties"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_KioskWinServiceProperties(liveUrl, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["MFKSyncroSvcSettings"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_MFKSyncroSvcSettings(kioskID, serviceIntervalMinutes, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["MFKSynchroModuleSvc"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.OnlyConfig_Module(liveUrl, moduleUrl, path, backupPath, count, sWebServiceProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }

            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"Config\");
            //resultStatus = status;
            //Report last progress
            //bwBackgroundWorkerConfigs.ReportProgress(101);

            //if (bwBackgroundWorkerConfigs.CancellationPending)
            //{
            //    e.Cancel = true;
            //    return;
            //}

        }


        private void bwBackgroundWorkerConfigs_DoWork(object sender, DoWorkEventArgs e)
        {
            #region bwBackgroundWorkerConfigs Start
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            string liveUrl = txtConfigLiveOfficeUrl.Text.Trim() + ":" + txtConfigLOPort.Text.Trim(), kioskID = txtConfigKioskId.Text, moduleUrl = txtConfigLiveOfficeUrl.Text.Trim() + ":" + txtConfigServicePort.Text.Trim();
            decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;
            string sWebLOProtocol = "";

            if (chbConfigHttpLO.Checked)
                sWebLOProtocol = "https";
            else
                sWebLOProtocol = "http";

            string sWebServiceProtocol = "";

            if (chbConfigHttpService.Checked)
                sWebServiceProtocol = "https";
            else
                sWebServiceProtocol = "http";
            //Check in database to make sure this scripts exists.
            //showStatus += sq.ScriptsDataBase();
            //countTimer += 1;

            //Time when start the work
            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            //Information for new files
            string serviceIntervalMinutes = "5";
            //string sourcePath = "";
            string backupPath = System.IO.Path.Combine(ConfigurationManager.AppSettings["ASAIFOLDER"], "BackUpConfigFiles" + (DateTime.Now.ToString("yyyyMMddHHmmss")));
            status = "Config Installation Started\n\nKioskID = " + kioskID + "\nLiveOfficeURL = " + sWebLOProtocol + "://" + liveUrl + "\n\n";
            string path = "";
            #endregion

            #region bwBackgroundWorkerConfigs PermisionAsaiFolderBackup
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                showStatus = "0.Start Giving permision to " + ConfigurationManager.AppSettings["ASAIFOLDER"];
                //Show Progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                Thread.Sleep(1000);
                //Try to give permision to Backup Folder
                try
                {
                    //If backup folder don't exist create it
                    if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["ASAIFOLDER"]))
                    {
                        System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    }
                    //Give per|mision to Asai Folder Backup
                    path = (ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    DirectorySecurity ds = Directory.GetAccessControl(path);
                    FileSystemAccessRule fs = new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow);
                    ds.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
                    Directory.SetAccessControl(path, ds);
                    showStatus = "0. Give permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + " Successful\n";
                }
                catch (Exception ex)
                {
                    showStatus = "0.Error giving permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". Error: " + ex.Message + "\n";
                    bContinue = false;
                }
                //Give 1 second for apply the permisions
                Thread.Sleep(1000);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;

                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));

            }
            #endregion

            #region bwBackgroundWorkerConfigs BosWebSvcSettings
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["BOSWebSvcSettings"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_BOSWebSvcSettings(liveUrl, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerConfigs KioskWebSvcSettings
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["KioskWebSvcSettings"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_KioskWebSvcSettings(liveUrl, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerConfigs KioskWinService.Properties
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["KioskWinService.Properties"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_KioskWinServiceProperties(liveUrl, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerConfigs MFKSyncroSvcSettings
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["MFKSyncroSvcSettings"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_MFKSyncroSvcSettings(kioskID, serviceIntervalMinutes, path, backupPath, count, sWebLOProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerConfigs MFKSynchroModuleSvc
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                path = ConfigurationManager.AppSettings["MFKSynchroModuleSvc"];
                showStatus += count + ".Start Creating " + System.IO.Path.GetDirectoryName(path) + ".\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.OnlyConfig_Module(liveUrl, moduleUrl, path, backupPath, count, sWebServiceProtocol);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nConfig Installation Failed. See Below Errors.\n";
                    showStatus += "\nConfig Installation Failed. See Below Errors.\n";
                    bContinue = false;
                    countTimer -= 1;
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerConfigs EndProcess
            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerConfigs.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"Config\");
            //resultStatus = status;
            //Report last progress
            bwBackgroundWorkerConfigs.ReportProgress(101);

            if (bwBackgroundWorkerConfigs.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            #endregion
        }

        private void bwBackgroundWorkerConfigs_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //When progresspercentage is 101 it means the work finished
            if (e.ProgressPercentage == 101)
            {
                MessageBox.Show(resultStatus, "New Installation Result");
                //Close the application
                //Application.Exit();
            }
            else
            {
                //pbProgressBar.Value = e.ProgressPercentage; //Update progress bar
                //if (pbProgressBar.Value == 100)
                //{
                //    //Backup process finished. Wait for 101 to show the report to the user
                //    lblProgressBar.Text = "Completed " + e.ProgressPercentage + "%\n" + showStatus;
                //}
                //else
                //{
                //    //Report the progress.
                //    lblProgressBar.Text = "Creating........ " + e.ProgressPercentage + "%\n" + showStatus;
                //}
            }
        }

        private void bwBackgroundWorkerConfigs_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("The task has been cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error. Details: " + (e.Error as Exception).ToString());
            }
            else
            {
                MessageBox.Show(showStatus, "New Installation Result");
                Application.Exit();
            }
        }

    }
}
