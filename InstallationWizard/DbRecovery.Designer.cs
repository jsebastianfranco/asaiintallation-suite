﻿namespace InstallationWizard
{
    partial class DbRecovery
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DbRecovery));
            this.BtnRsDb = new DevExpress.XtraEditors.SimpleButton();
            this.button1 = new DevExpress.XtraEditors.SimpleButton();
            this.label24 = new System.Windows.Forms.Label();
            this.txtRsDb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // BtnRsDb
            // 
            this.BtnRsDb.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.BtnRsDb.Appearance.Font = new System.Drawing.Font("Titillium Web", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRsDb.Appearance.Options.UseBackColor = true;
            this.BtnRsDb.Appearance.Options.UseFont = true;
            this.BtnRsDb.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("BtnRsDb.ImageOptions.SvgImage")));
            this.BtnRsDb.Location = new System.Drawing.Point(38, 354);
            this.BtnRsDb.Name = "BtnRsDb";
            this.BtnRsDb.Size = new System.Drawing.Size(179, 49);
            this.BtnRsDb.TabIndex = 53;
            this.BtnRsDb.Text = "Results";
            this.BtnRsDb.Click += new System.EventHandler(this.BtnRsDb_Click);
            // 
            // button1
            // 
            this.button1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.button1.Appearance.Font = new System.Drawing.Font("Titillium Web", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Appearance.Options.UseBackColor = true;
            this.button1.Appearance.Options.UseFont = true;
            this.button1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("button1.ImageOptions.SvgImage")));
            this.button1.Location = new System.Drawing.Point(38, 258);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 49);
            this.button1.TabIndex = 52;
            this.button1.Text = "Execute";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Titillium Web", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label24.Location = new System.Drawing.Point(50, 30);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(384, 66);
            this.label24.TabIndex = 51;
            this.label24.Text = "Mode: Db Recovery";
            // 
            // txtRsDb
            // 
            this.txtRsDb.Location = new System.Drawing.Point(260, 122);
            this.txtRsDb.Margin = new System.Windows.Forms.Padding(4);
            this.txtRsDb.Multiline = true;
            this.txtRsDb.Name = "txtRsDb";
            this.txtRsDb.ReadOnly = true;
            this.txtRsDb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRsDb.Size = new System.Drawing.Size(570, 410);
            this.txtRsDb.TabIndex = 50;
            // 
            // DbRecovery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.BtnRsDb);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtRsDb);
            this.Name = "DbRecovery";
            this.Size = new System.Drawing.Size(868, 563);
            this.Load += new System.EventHandler(this.DbRecovery_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton BtnRsDb;
        private DevExpress.XtraEditors.SimpleButton button1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtRsDb;
    }
}
