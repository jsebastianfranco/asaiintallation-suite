﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InstallationWizardClass;
using System.IO;

namespace InstallationWizard
{
    public partial class DbRecovery : UserControl
    {
        private static DbRecovery _instance;
        public static DbRecovery Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DbRecovery();
                return _instance;
            }
        }

        public DbRecovery()
        {
            InitializeComponent();
        }


        private void DbRecovery_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            RecoveryDb rDb = new RecoveryDb();
            wLogging.log("-----");

            try
            {
                wLogging.log("Calling the RestoreDb class");
                rDb.cmd();
            }
            catch (Exception err1)
            {
                wLogging.log("Error calling the RestoreDb class" + err1.Message);
            }


            MessageBox.Show("Process ended", "Check DataBase", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void BtnRsDb_Click(object sender, EventArgs e)
        {
            try
            {
                string ResultsPath = @"C:\ASAI\InstallationWizard\RecoveryDbResults\Results.txt";
                string readResult = File.ReadAllText(ResultsPath);
                txtRsDb.Text = readResult;
            }
            catch (Exception errReading)
            {
                wLogging.log("File couldnt be read" + errReading.Message);
            }

        }
    }
}
