﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InstallationWizardClass;
using System.Configuration;
using System.Reflection;
using System.Xml.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Xml;
using System.Threading;

namespace InstallationWizard
{
    public partial class TestConfigFiles : UserControl
    {
        public int timer;
        public string showStatus, resultStatus, bp, kioskstatus, starting;
        private static TestConfigFiles _instance;
        public static TestConfigFiles Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new TestConfigFiles();
                return _instance;
            }
        }

        public TestConfigFiles()
        {
            InitializeComponent();
            
            //bwBackgroundWorkerKioskInfo.RunWorkerAsync();
        }
        private void TestConfigFiles_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            readKioskXml();
            readTicketServer();
            readLOXml();
            
        }



        private void readTicketServer()
        {
            //reading Kiosk Settings file xml by elements
            wLogging.log("Reading ConfigfileKioskInfo element on xml ");
            try
            {
                //Todo Decrypt

                if (File.Exists(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml"))
                {
                    File.Delete(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml");
                }

                string j = KioskConfiguration.Generic.MfkCrypto.Decrypt(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfo.xml");

                //Searching KioskId into XML

                XmlDocument xmlDc2 = new XmlDocument();
                xmlDc2.LoadXml(j);
                xmlDc2.Save(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml");
                Thread.Sleep(1000);

                XmlNodeList elemList = xmlDc2.GetElementsByTagName("TicketServerName");

                for (int i = 0; i < elemList.Count; i++)
                {

                    lblTicketServerName.Text = elemList[i].InnerXml;
                }

                Thread.Sleep(1000);


                //Searching KioskIp into XML

                xmlDc2.LoadXml(j);
                xmlDc2.Save(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml");
                Thread.Sleep(1000);

                XmlNodeList elemList2 = xmlDc2.GetElementsByTagName("TicketServerIP");

                for (int i = 0; i < elemList2.Count; i++)
                {

                    lblTicketServerIp.Text = elemList2[i].InnerXml;
                }


            }
            catch (Exception ex)
            {
                wLogging.log("Error Trying to reading Kiosk Settings " + ex);
                MessageBox.Show("Error Trying to reading Kiosk Settings - Ticketing server" + ex, "Testing Ticketing Configs");
            }
        }


        private void readLOXml()
        {
            //read xml node by searching its attribute Kiosk Service URl
            wLogging.log("Reading kiosk atribute on xml file ");
            try
            {
                XmlDocument xmlDc = new XmlDocument();
                xmlDc.Load(@"C:\ASAI\Services\MFKServices\BOSWebSvcSettings.config");

                XmlNodeList xnList = xmlDc.SelectNodes("/BOSWebSvcAPI.Properties.BOSWebSvcSettings/setting[@name='KioskServiceURL']");

                foreach (XmlNode xn in xnList)
                {

                    string conf1 = xn["value"].InnerText;
                    lblLoIp.Text = conf1;
                }
            }
            catch (Exception err)
            {
                wLogging.log("Error reading kiosk atribute on xml file " + err);
                MessageBox.Show("Error reading Live office configs " + err, "Testing Live office Configs");
            }
        }

        private void readKioskXml()
        {
            //reading Kiosk Settings file xml by elements
            wLogging.log("Reading ConfigfileKioskInfo element on xml ");
            try
            {
                //Todo Decrypt

                if (File.Exists(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml"))
                {
                    File.Delete(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml");
                }

                string j = KioskConfiguration.Generic.MfkCrypto.Decrypt(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfo.xml");

                //Searching KioskId into XML

                XmlDocument xmlDc2 = new XmlDocument();
                xmlDc2.LoadXml(j);
                xmlDc2.Save(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml");

                Thread.Sleep(1000);



                XmlNodeList elemList = xmlDc2.GetElementsByTagName("KioskId");

                for (int i = 0; i < elemList.Count; i++)
                {

                    lblKioskId.Text = elemList[i].InnerXml;
                }

                Thread.Sleep(1000);


                //Searching KioskIp into XML

                xmlDc2.LoadXml(j);
                xmlDc2.Save(@"C:\ASAI\CacheConfigFile\ConfigFileKioskInfoTemp.xml");

                Thread.Sleep(1000);



                XmlNodeList elemList2 = xmlDc2.GetElementsByTagName("KioskIP");

                for (int i = 0; i < elemList2.Count; i++)
                {

                    lblKioskIp.Text = elemList2[i].InnerXml;
                }


            }
            catch (Exception ex)
            {
                wLogging.log("Error Trying to reading Kiosk Settings " + ex);
                MessageBox.Show("Error Trying to reading Kiosk Settings" + ex, "Testing Kiosk Configs");
            }
        }


        #region bwBackgroundWorkerKioskInfo
        private void bwBackgroundWorkerKioskInfo_DoWork(object sender, DoWorkEventArgs e)
        {

            #region bwBackgroundWorkerKioskInfo Start
            kioskstatus = "Downloaded Config Files                                 BIN Config Files\n\n";
            var sq = new Create();
            starting = "";
            string elementname = null;
            string result = null;
            //XmlDataDocument xmldoc = new XmlDataDocument();
            int countTimer = 0, count = 0;
            decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;
            //FileStream fs = new FileStream(@"c:\ASAI\cacheconfigfile\ConfigFileKioskInfo.xml", FileMode.Open, FileAccess.Read);

            //Check in database to make sure this scripts exists.
            //showStatus += sq.ScriptsDataBase();
            //countTimer += 1;

            //Time when start the work
            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            //Information for new files
            string backupPath = System.IO.Path.Combine(ConfigurationManager.AppSettings["ASAIFOLDER"], "BackUpConfigFiles" + (DateTime.Now.ToString("yyyyMMddHHmmss")));
            //kioskstatus = "Kiosk Info Started\n\n";
            //string path = "123";

            string libPath = (ConfigurationManager.AppSettings["LibPath"]);
            showStatus = "Starting Reading Decrypted";
            i = 1;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            Assembly assembly = Assembly.LoadFile(libPath);
            Type mkfCfgClass = assembly.GetType("KioskConfiguration.MFKConfig");



            Type mfkUtilClass = assembly.GetType("KioskConfiguration.Generic.Utilities");
            var enums = assembly.GetType("KioskConfiguration.ConfigSection").GetEnumNames();
            // This method ensures every config file gets loaded to cache manager.
            MethodInfo cfgMethod = mkfCfgClass.GetMethod("CheckIsLoaded", new[] { typeof(string) });
            MethodInfo utilMethod = mfkUtilClass.GetMethod("SerializeToString");
            cfgMethod.Invoke(null, new[] { "None" });
            // This method is used to retrieve single objects from the cache. 
            cfgMethod = mkfCfgClass.GetMethod("Get", new[] { typeof(string) });
            showStatus = "Read Complete";
            i = 1;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            showStatus = "";
            #endregion

            #region bwBackgroundWorkerKioskInfo Kiosk Data

            // Kiosk 

            count += 1;
            //fs = new FileStream(@"c:\ASAI\cacheconfigfile\ConfigFileKioskInfo.xml", FileMode.Open, FileAccess.Read);
            //xmldoc.Load(fs);

            dtStart = DateTime.Now;
            elementname = "KioskName";
            //mFKCrypto en kiokconfiguration

            Object data = cfgMethod.Invoke(null, new[] { "KioskInfo" });
            Object data2 = cfgMethod.Invoke(null, new[] { "CashAdvanceFees" });

            result = data.GetType().GetProperty(elementname).GetValue(data, null).ToString();
            //result = sq.KioskInfo_FileInfo(xmldoc, elementname);
            showStatus = "LiveOfficeName " + result + "                                   KioskName " + Environment.MachineName + "\n";
            //Calculate time taked doing this progress
            dtFinish = DateTime.Now;
            tsDuration = dtFinish - dtStart;
            dSecondTotal = tsDuration.TotalSeconds;
            kioskstatus += showStatus;
            showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //Show progress
            countTimer += 1;
            count += 1;
            starting = count + ". Starting Reading Kiosk ID";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            //ConfigurationManager.AppSettings["MFKSyncroSvcSettings"]


            //XElement root = XElement.Load(@"C:\program files\Diebold\Agilis Empower\bin\MFKSyncroSvcSettings.config");

            XElement root = XElement.Load(ConfigurationManager.AppSettings["MFKSyncroSvcSettings"]);


            dtStart = DateTime.Now;
            elementname = "KioskId";
            result = data.GetType().GetProperty(elementname).GetValue(data, null).ToString();
            //result = sq.KioskInfo_FileInfo(xmldoc, elementname);
            showStatus = "LiveOfficeID " + result + ".";
            elementname = "KioskIdBOS";
            string result2 = sq.KioskInfo_FileInfoConfig(root, elementname);
            showStatus += "                                               Kiosk ID" + result2 + "\n";
            //Calculate time taked doing this progress
            dtFinish = DateTime.Now;
            tsDuration = dtFinish - dtStart;
            dSecondTotal = tsDuration.TotalSeconds;
            kioskstatus += showStatus;
            showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //Show progress
            countTimer += 1;
            count += 1;
            starting = count + ". Starting Reading Kiosk IP";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            //fs2 = new FileStream(@"C:\program files\Diebold\Agilis Empower\bin\BOSWebSvcSettings.config", FileMode.Open, FileAccess.Read);
            //xmldoc2 = new XmlDataDocument();
            //xmldoc2.Load(fs2);

            //XElement root = XElement.Load(@"C:\program files\Diebold\Agilis Empower\bin\BOSWebSvcSettings.config");
            //result = sq.KioskInfo_FileInfoConfig(root, elementname);

            dtStart = DateTime.Now;
            elementname = "KioskIP";
            result = data.GetType().GetProperty(elementname).GetValue(data, null).ToString();
            //result = sq.KioskInfo_FileInfo(xmldoc, elementname);
            showStatus = "KioskIPInLO " + result + "                             ";
            IPHostEntry host2;
            string localIP = "";
            host2 = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host2.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    //break;
                }
            }
            showStatus += "Local IP " + localIP + "\n\n";
            //Calculate time taked doing this progress
            dtFinish = DateTime.Now;
            tsDuration = dtFinish - dtStart;
            dSecondTotal = tsDuration.TotalSeconds;
            kioskstatus += showStatus;
            showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //Show progress
            countTimer += 1;
            count += 1;
            starting = count + ". Starting Reading TicketServerName";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            #endregion

            #region bwBackgroundWorkerKioskInfo Ticket Server
            //Ticket server

            dtStart = DateTime.Now;
            elementname = "TicketServerName";
            result = data.GetType().GetProperty(elementname).GetValue(data, null).ToString();
            //result = sq.KioskInfo_FileInfo(xmldoc, elementname);
            showStatus = elementname + " " + result + "\n";
            //Calculate time taked doing this progress
            dtFinish = DateTime.Now;
            tsDuration = dtFinish - dtStart;
            dSecondTotal = tsDuration.TotalSeconds;
            kioskstatus += showStatus;
            showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //Show progress
            countTimer += 1;
            count += 1;
            starting = count + ". Starting Reading TicketServerIP";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            dtStart = DateTime.Now;
            elementname = "TicketServerIP";
            result = data.GetType().GetProperty(elementname).GetValue(data, null).ToString();
            //result = sq.KioskInfo_FileInfo(xmldoc, elementname);
            showStatus = elementname + " " + result + "\n";
            if (result == "Not Found.")
                showStatus += "\n";
            else
                showStatus += elementname + sq.KioskInfo_ping(result) + "\n\n";
            //Calculate time taked doing this progress
            dtFinish = DateTime.Now;
            tsDuration = dtFinish - dtStart;
            dSecondTotal = tsDuration.TotalSeconds;
            kioskstatus += showStatus;
            showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //Show progress
            countTimer += 1;
            count += 1;
            starting = count + ". Starting Reading LiveOffice IP and Test";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            #endregion

            #region bwBackgroundWorkerKioskInfo Liveoffice
            //Liveoffice

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.LoadXml(ds.GetXml());
            //xml1.XPathNavigator = xmlDoc.CreateNavigator();
            //xml1.TransformSource = @"~/XSLT/LogEntryTransform.xslt";


            //FileStream fs = new FileStream(ConfigurationManager.AppSettings["MFKSyncroSvcSettings"], FileMode.Open, FileAccess.Read);
            FileStream fs = new FileStream(ConfigurationManager.AppSettings["BOSWebSvcSettings"], FileMode.Open, FileAccess.Read);
            //XmlDataDocument xmldoc = new XmlDataDocument();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(fs);

            dtStart = DateTime.Now;
            elementname = "BOSWebSvcAPI.Properties.BOSWebSvcSettings";
            result = sq.KioskInfo_FileInfo(xmldoc, elementname).Trim().Replace("/BOSWebServices.LogService.svc", "");
            if (result == "Not Found.")
            {
                showStatus = "LiveOffice IP " + result + "\n";
                showStatus += "";
            }
            else
            {
                if (result.ToUpper().StartsWith("HTTP:"))
                {
                    showStatus = "LiveOffice IP " + result.Substring(7, result.Length - 12) + "\n";
                    showStatus += "LiveOffice IP" + sq.KioskInfo_ping(result.Substring(7, result.Length - 12)) + "\n\n";
                }
                else
                {
                    showStatus = "LiveOffice IP " + result.Substring(8, result.Length - 13) + "\n";
                    showStatus += "LiveOffice IP" + sq.KioskInfo_ping(result.Substring(8, result.Length - 13)) + "\n\n";
                }
            }
            //Calculate time taked doing this progress
            dtFinish = DateTime.Now;
            tsDuration = dtFinish - dtStart;
            dSecondTotal = tsDuration.TotalSeconds;
            kioskstatus += showStatus;
            showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //Show progress
            countTimer += 1;
            count += 1;
            starting = count + ". Starting Reading EcheckUrl and  ";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            #endregion

            //if (timer > 6)
            //{
            //    #region bwBackgroundWorkerKioskInfo ECheck

            //    //Console.WriteLine("Main thread: starting a timer");
            //    //System.Timers.Timer t = new System.Timers.Timer(ComputeBoundOp, 5, 0, 2000);
            //    //Console.WriteLine("Main thread: Doing other work here...");
            //    //Thread.Sleep(10000); // Simulating other work (10 seconds)
            //    //t.Dispose(); // Cancel the timer now

            //    fs = new FileStream(@"c:\ASAI\cacheconfigfile\ConfigFileBOServerParameters.xml", FileMode.Open, FileAccess.Read);
            ////xmldoc = new XmlDataDocument();
            //xmldoc = new XmlDocument();
            //xmldoc.Load(fs);

            //    //Echeck

            //    dtStart = DateTime.Now;
            //    elementname = "BOServerParametersInfo";
            //    result = sq.KioskInfo_BOServerParametersInfo(xmldoc, elementname, count);
            //    showStatus = result;
            //    //Calculate time taked doing this progress
            //    dtFinish = DateTime.Now;
            //    tsDuration = dtFinish - dtStart;
            //    dSecondTotal = tsDuration.TotalSeconds;
            //    kioskstatus += showStatus;
            //    showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //    //Show progress
            //    countTimer += 1;
            //    count += 1;
            //    starting = count + ". Starting Reading ATM IP and Test";
            //    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //    bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            //    #endregion

            //    #region bwBackgroundWorkerKioskInfo ATM

            //    dtStart = DateTime.Now;
            //    Socket socket = null;
            //    string host = null;
            //    int port = 0;
            //    string str = null;
            //    try
            //    {
            //        string targetPath = @"C:\Program Files\Diebold\ABC\devices.ini";

            //        ReadIniFiles iniFile = new ReadIniFiles(targetPath, ";");
            //        host = iniFile.GetValue("[App=API, Name=Dev000]", "IPHost", "");
            //        int.TryParse(iniFile.GetValue("[App=API, Name=Dev000]", "RemotePort", ""), out port);

            //        socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            //        System.Net.IPAddress remoteIPAddress = System.Net.IPAddress.Parse(host);
            //        str = remoteIPAddress.ToString();
            //        System.Net.IPEndPoint remoteEndPoint = new System.Net.IPEndPoint(remoteIPAddress, port);
            //        socket.Connect(remoteEndPoint);
            //        System.Threading.Thread.Sleep(3000);
            //        socket.Close();

            //        showStatus = "ATM IP " + str + "\nATM IP " + " Connect Success" + "\n\n";
            //    }
            //    catch (Exception ex)
            //    {
            //        string exe = ex.Message;
            //        showStatus = "ATM IP " + str + "\nATM IP Connect Failed" + "\n\n";
            //    }
            //    //Calculate time taked doing this progress
            //    dtFinish = DateTime.Now;
            //    tsDuration = dtFinish - dtStart;
            //    dSecondTotal = tsDuration.TotalSeconds;
            //    kioskstatus += showStatus;
            //    showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //    //Show progress
            //    countTimer += 1;
            //    count += 1;
            //    starting = count + ". Starting Reading CashAdvance IP and Test";
            //    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //    bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            //    #endregion

            //    #region bwBackgroundWorkerKioskInfo CashAdvance
            //    //cashAdvance IP

            //    dtStart = DateTime.Now;
            //    MFKConfig.CheckIsLoaded(ConfigSection.None);
            //    BOSWebSvcContract.Kiosk.CashAdvanceServerInfo casi = (CashAdvanceServerInfo)MFKConfig.Get(ConfigSection.CashAdvanceServerInfo);

            //    if (null != casi && casi.CashAdvanceServersId > 0)
            //    {
            //        //IDSCashAdvanceDataContract.FeesStructure feesConfig = (FeesStructure)GetFeesProcessor(casi);
            //        IDSCashAdvanceDataContract.FeesStructure feesConfig = (FeesStructure)GetFeesClient(casi);
            //        str = casi.ServerURL.ToString();
            //        //string oSerialized = Utilities.SerializeToString(feesConfig);
            //        if (feesConfig != null)
            //        {
            //            showStatus = "CashAdvance IP " + str + "\nCashAdvance IP Connect Success" + "\n\n";
            //        }
            //        else
            //        {
            //            showStatus = "CashAdvance IP " + str + "\nCashAdvance IP Connect Failed" + "\n\n";
            //        }
            //    }
            //    else
            //    {
            //        showStatus = "CashAdvance IP Failed to read \nCashAdvance IP Connect Failed" + "\n\n";
            //    }
            //    //Calculate time taked doing this progress
            //    dtFinish = DateTime.Now;
            //    tsDuration = dtFinish - dtStart;
            //    dSecondTotal = tsDuration.TotalSeconds;
            //    kioskstatus += showStatus;
            //    showStatus = count + "." + showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
            //    //Show progress
            //    countTimer += 1;
            //    count += 1;
            //    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //    bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));

            //    #endregion
            //}
            #region bwBackgroundWorkerKioskInfo EndProcess
            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            //kioskstatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            starting = "";
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerKioskInfo.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(kioskstatus, @"KioskInfo\");
            //resultStatus = status;
            //Report last progress
            bwBackgroundWorkerKioskInfo.ReportProgress(101);

            if (bwBackgroundWorkerNew.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            #endregion

        }

        private void bwBackgroundWorkerKioskInfo_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //When progresspercentage is 101 it means the work finished
            if (e.ProgressPercentage == 101)
            {
                //MessageBox.Show(resultStatus, "New Installation Result");
                //Close the application
                //Application.Exit();
            }
            else
            {
                //pbProgressBar.Value = e.ProgressPercentage; //Update progress bar
                //if (e.ProgressPercentage == 100)
                //{
                //    //Backup process finished. Wait for 101 to show the report to the user
                //    lblProgressBar.Text = "Completed " + e.ProgressPercentage + "%\n" + showStatus;
                //    lblHealth.Text = kioskstatus;
                //}
                //else
                //{
                //    //Report the progress.
                //    lblProgressBar.Text = "loading........ " + e.ProgressPercentage + "%        " + starting + "\nComplete: " + showStatus;
                //    lblHealth.Text = kioskstatus;
                //}
            }
        }

        private void bwBackgroundWorkerKioskInfo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("The task has been cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error. Details: " + (e.Error as Exception).ToString());
            }
            else
            {
                MessageBox.Show(showStatus, "New Installation Result");
                Application.Exit();
            }
        }

        private static void ComputeBoundOp(Object state)
        {
            // This method is executed by a thread pool thread 
            Console.WriteLine("In ComputeBoundOp: state={0}", state);
            Thread.Sleep(1000); // Simulates other work (1 second)
            // When this method returns, the thread goes back 
            // to the pool and waits for another task 
        }

        #endregion
    }
}
