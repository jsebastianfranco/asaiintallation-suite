﻿namespace InstallationWizard
{
    partial class OnlyConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnlyConfig));
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.chbConfigHttpService = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtConfigServicePort = new System.Windows.Forms.TextBox();
            this.lblConfigServicePort = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chbConfigHttpLO = new System.Windows.Forms.CheckBox();
            this.lblConfigHttp = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblConfigLiveOfficeUrlExample = new System.Windows.Forms.Label();
            this.txtConfigLOPort = new System.Windows.Forms.TextBox();
            this.txtConfigLiveOfficeUrl = new System.Windows.Forms.TextBox();
            this.txtConfigKioskId = new System.Windows.Forms.TextBox();
            this.lblConfigLOPort = new System.Windows.Forms.Label();
            this.lblConfigLiveOfficeUrl = new System.Windows.Forms.Label();
            this.lblConfigKioskId = new System.Windows.Forms.Label();
            this.bwBackgroundWorkerConfigs = new System.ComponentModel.BackgroundWorker();
            this.lblProgressBar = new System.Windows.Forms.Label();
            this.circularProgressBar1 = new CircularProgressBar.CircularProgressBar();
            this.SuspendLayout();
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseBackColor = true;
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton7.ImageOptions.SvgImage")));
            this.simpleButton7.Location = new System.Drawing.Point(527, 377);
            this.simpleButton7.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(129, 40);
            this.simpleButton7.TabIndex = 75;
            this.simpleButton7.Text = "Cancel";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseBackColor = true;
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton8.ImageOptions.SvgImage")));
            this.simpleButton8.Location = new System.Drawing.Point(349, 377);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(141, 40);
            this.simpleButton8.TabIndex = 74;
            this.simpleButton8.Text = "Create";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(44, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(366, 39);
            this.label1.TabIndex = 73;
            this.label1.Text = "Mode: Configurations";
            // 
            // chbConfigHttpService
            // 
            this.chbConfigHttpService.AutoSize = true;
            this.chbConfigHttpService.BackColor = System.Drawing.Color.Transparent;
            this.chbConfigHttpService.Location = new System.Drawing.Point(322, 256);
            this.chbConfigHttpService.Name = "chbConfigHttpService";
            this.chbConfigHttpService.Size = new System.Drawing.Size(15, 14);
            this.chbConfigHttpService.TabIndex = 62;
            this.chbConfigHttpService.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(251, 256);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 72;
            this.label11.Text = "Use HTTPS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(166, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 71;
            this.label8.Text = "Example: 9443";
            // 
            // txtConfigServicePort
            // 
            this.txtConfigServicePort.Location = new System.Drawing.Point(167, 254);
            this.txtConfigServicePort.MaxLength = 15;
            this.txtConfigServicePort.Name = "txtConfigServicePort";
            this.txtConfigServicePort.Size = new System.Drawing.Size(74, 20);
            this.txtConfigServicePort.TabIndex = 61;
            // 
            // lblConfigServicePort
            // 
            this.lblConfigServicePort.AutoSize = true;
            this.lblConfigServicePort.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigServicePort.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfigServicePort.Location = new System.Drawing.Point(70, 257);
            this.lblConfigServicePort.Name = "lblConfigServicePort";
            this.lblConfigServicePort.Size = new System.Drawing.Size(65, 13);
            this.lblConfigServicePort.TabIndex = 70;
            this.lblConfigServicePort.Text = "Service Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(167, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 69;
            this.label3.Text = "Example: 8443";
            // 
            // chbConfigHttpLO
            // 
            this.chbConfigHttpLO.AutoSize = true;
            this.chbConfigHttpLO.BackColor = System.Drawing.Color.Transparent;
            this.chbConfigHttpLO.Location = new System.Drawing.Point(322, 209);
            this.chbConfigHttpLO.Name = "chbConfigHttpLO";
            this.chbConfigHttpLO.Size = new System.Drawing.Size(15, 14);
            this.chbConfigHttpLO.TabIndex = 60;
            this.chbConfigHttpLO.UseVisualStyleBackColor = false;
            // 
            // lblConfigHttp
            // 
            this.lblConfigHttp.AutoSize = true;
            this.lblConfigHttp.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigHttp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfigHttp.Location = new System.Drawing.Point(251, 209);
            this.lblConfigHttp.Name = "lblConfigHttp";
            this.lblConfigHttp.Size = new System.Drawing.Size(65, 13);
            this.lblConfigHttp.TabIndex = 68;
            this.lblConfigHttp.Text = "Use HTTPS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(80, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 67;
            // 
            // lblConfigLiveOfficeUrlExample
            // 
            this.lblConfigLiveOfficeUrlExample.AutoSize = true;
            this.lblConfigLiveOfficeUrlExample.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigLiveOfficeUrlExample.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfigLiveOfficeUrlExample.Location = new System.Drawing.Point(167, 178);
            this.lblConfigLiveOfficeUrlExample.Name = "lblConfigLiveOfficeUrlExample";
            this.lblConfigLiveOfficeUrlExample.Size = new System.Drawing.Size(122, 13);
            this.lblConfigLiveOfficeUrlExample.TabIndex = 66;
            this.lblConfigLiveOfficeUrlExample.Text = "Example: 192.168.21.26";
            // 
            // txtConfigLOPort
            // 
            this.txtConfigLOPort.Location = new System.Drawing.Point(169, 206);
            this.txtConfigLOPort.MaxLength = 15;
            this.txtConfigLOPort.Name = "txtConfigLOPort";
            this.txtConfigLOPort.Size = new System.Drawing.Size(74, 20);
            this.txtConfigLOPort.TabIndex = 59;
            // 
            // txtConfigLiveOfficeUrl
            // 
            this.txtConfigLiveOfficeUrl.Location = new System.Drawing.Point(170, 155);
            this.txtConfigLiveOfficeUrl.MaxLength = 20;
            this.txtConfigLiveOfficeUrl.Name = "txtConfigLiveOfficeUrl";
            this.txtConfigLiveOfficeUrl.Size = new System.Drawing.Size(138, 20);
            this.txtConfigLiveOfficeUrl.TabIndex = 58;
            // 
            // txtConfigKioskId
            // 
            this.txtConfigKioskId.Location = new System.Drawing.Point(170, 119);
            this.txtConfigKioskId.MaxLength = 4;
            this.txtConfigKioskId.Name = "txtConfigKioskId";
            this.txtConfigKioskId.Size = new System.Drawing.Size(38, 20);
            this.txtConfigKioskId.TabIndex = 57;
            // 
            // lblConfigLOPort
            // 
            this.lblConfigLOPort.AutoSize = true;
            this.lblConfigLOPort.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigLOPort.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfigLOPort.Location = new System.Drawing.Point(76, 209);
            this.lblConfigLOPort.Name = "lblConfigLOPort";
            this.lblConfigLOPort.Size = new System.Drawing.Size(43, 13);
            this.lblConfigLOPort.TabIndex = 65;
            this.lblConfigLOPort.Text = "LO Port";
            // 
            // lblConfigLiveOfficeUrl
            // 
            this.lblConfigLiveOfficeUrl.AutoSize = true;
            this.lblConfigLiveOfficeUrl.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigLiveOfficeUrl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfigLiveOfficeUrl.Location = new System.Drawing.Point(76, 158);
            this.lblConfigLiveOfficeUrl.Name = "lblConfigLiveOfficeUrl";
            this.lblConfigLiveOfficeUrl.Size = new System.Drawing.Size(68, 13);
            this.lblConfigLiveOfficeUrl.TabIndex = 64;
            this.lblConfigLiveOfficeUrl.Text = "LiveOffice IP";
            // 
            // lblConfigKioskId
            // 
            this.lblConfigKioskId.AutoSize = true;
            this.lblConfigKioskId.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigKioskId.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfigKioskId.Location = new System.Drawing.Point(76, 128);
            this.lblConfigKioskId.Name = "lblConfigKioskId";
            this.lblConfigKioskId.Size = new System.Drawing.Size(53, 13);
            this.lblConfigKioskId.TabIndex = 63;
            this.lblConfigKioskId.Text = "KIOSK ID";
            // 
            // bwBackgroundWorkerConfigs
            // 
            this.bwBackgroundWorkerConfigs.WorkerReportsProgress = true;
            this.bwBackgroundWorkerConfigs.WorkerSupportsCancellation = true;
            this.bwBackgroundWorkerConfigs.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwBackgroundWorkerConfigs_DoWork);
            // 
            // lblProgressBar
            // 
            this.lblProgressBar.AutoSize = true;
            this.lblProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.lblProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressBar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblProgressBar.Location = new System.Drawing.Point(442, 308);
            this.lblProgressBar.Name = "lblProgressBar";
            this.lblProgressBar.Size = new System.Drawing.Size(0, 29);
            this.lblProgressBar.TabIndex = 99;
            // 
            // circularProgressBar1
            // 
            this.circularProgressBar1.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularProgressBar1.AnimationSpeed = 500;
            this.circularProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.circularProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.circularProgressBar1.ForeColor = System.Drawing.Color.White;
            this.circularProgressBar1.InnerColor = System.Drawing.Color.Transparent;
            this.circularProgressBar1.InnerMargin = 2;
            this.circularProgressBar1.InnerWidth = -1;
            this.circularProgressBar1.Location = new System.Drawing.Point(430, 123);
            this.circularProgressBar1.MarqueeAnimationSpeed = 2000;
            this.circularProgressBar1.Name = "circularProgressBar1";
            this.circularProgressBar1.OuterColor = System.Drawing.Color.White;
            this.circularProgressBar1.OuterMargin = -40;
            this.circularProgressBar1.OuterWidth = 26;
            this.circularProgressBar1.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(36)))), ((int)(((byte)(71)))));
            this.circularProgressBar1.ProgressWidth = 10;
            this.circularProgressBar1.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circularProgressBar1.Size = new System.Drawing.Size(170, 169);
            this.circularProgressBar1.StartAngle = 270;
            this.circularProgressBar1.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularProgressBar1.SubscriptText = "";
            this.circularProgressBar1.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularProgressBar1.SuperscriptText = "";
            this.circularProgressBar1.TabIndex = 98;
            this.circularProgressBar1.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularProgressBar1.Visible = false;
            // 
            // OnlyConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lblProgressBar);
            this.Controls.Add(this.circularProgressBar1);
            this.Controls.Add(this.simpleButton7);
            this.Controls.Add(this.simpleButton8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chbConfigHttpService);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtConfigServicePort);
            this.Controls.Add(this.lblConfigServicePort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chbConfigHttpLO);
            this.Controls.Add(this.lblConfigHttp);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblConfigLiveOfficeUrlExample);
            this.Controls.Add(this.txtConfigLOPort);
            this.Controls.Add(this.txtConfigLiveOfficeUrl);
            this.Controls.Add(this.txtConfigKioskId);
            this.Controls.Add(this.lblConfigLOPort);
            this.Controls.Add(this.lblConfigLiveOfficeUrl);
            this.Controls.Add(this.lblConfigKioskId);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "OnlyConfig";
            this.Size = new System.Drawing.Size(677, 457);
            this.Load += new System.EventHandler(this.OnlyConfig_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbConfigHttpService;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtConfigServicePort;
        private System.Windows.Forms.Label lblConfigServicePort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chbConfigHttpLO;
        private System.Windows.Forms.Label lblConfigHttp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblConfigLiveOfficeUrlExample;
        private System.Windows.Forms.TextBox txtConfigLOPort;
        private System.Windows.Forms.TextBox txtConfigLiveOfficeUrl;
        private System.Windows.Forms.TextBox txtConfigKioskId;
        private System.Windows.Forms.Label lblConfigLOPort;
        private System.Windows.Forms.Label lblConfigLiveOfficeUrl;
        private System.Windows.Forms.Label lblConfigKioskId;
        private System.ComponentModel.BackgroundWorker bwBackgroundWorkerConfigs;
        private System.Windows.Forms.Label lblProgressBar;
        private CircularProgressBar.CircularProgressBar circularProgressBar1;
    }
}
