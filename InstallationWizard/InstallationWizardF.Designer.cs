﻿namespace InstallationWizard
{
    partial class InstallationWizardF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstallationWizardF));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            this.accordionControl1 = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement14 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement15 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement16 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement17 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement3 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement18 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement4 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement19 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement5 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement21 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement22 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement6 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement23 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement7 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement24 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement25 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement26 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement8 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement27 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement20 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement28 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement13 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.fluentDesignFormControl1 = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl();
            this.fluentFormDefaultManager1 = new DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager(this.components);
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement9 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement10 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement11 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement12 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel = new System.Windows.Forms.Panel();
            this.help = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentFormDefaultManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // accordionControl1
            // 
            this.accordionControl1.Appearance.AccordionControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(40)))), ((int)(((byte)(80)))));
            this.accordionControl1.Appearance.AccordionControl.Options.UseBackColor = true;
            this.accordionControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionControl1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement1,
            this.accordionControlElement3,
            this.accordionControlElement4,
            this.accordionControlElement5,
            this.accordionControlElement6,
            this.accordionControlElement7,
            this.accordionControlElement8,
            this.accordionControlElement20,
            this.accordionControlElement13});
            this.accordionControl1.Location = new System.Drawing.Point(0, 0);
            this.accordionControl1.Name = "accordionControl1";
            this.accordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch;
            this.accordionControl1.Size = new System.Drawing.Size(286, 707);
            this.accordionControl1.TabIndex = 1;
            this.accordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu;
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement14,
            this.accordionControlElement15,
            this.accordionControlElement16,
            this.accordionControlElement17});
            this.accordionControlElement1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement1.ImageOptions.SvgImage")));
            this.accordionControlElement1.Name = "accordionControlElement1";
            toolTipItem1.Text = "In this module you are allow to perform some \r\nactivities related to Asai softwar" +
    "e files";
            superToolTip1.Items.Add(toolTipItem1);
            this.accordionControlElement1.SuperTip = superToolTip1;
            this.accordionControlElement1.Text = "Image";
            this.accordionControlElement1.Click += new System.EventHandler(this.accordionControlElement1_Click);
            // 
            // accordionControlElement14
            // 
            this.accordionControlElement14.Name = "accordionControlElement14";
            this.accordionControlElement14.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement14.Text = "Back Up";
            this.accordionControlElement14.Click += new System.EventHandler(this.accordionControlElement14_Click);
            // 
            // accordionControlElement15
            // 
            this.accordionControlElement15.Name = "accordionControlElement15";
            this.accordionControlElement15.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement15.Text = "Restore";
            this.accordionControlElement15.Click += new System.EventHandler(this.accordionControlElement15_Click);
            // 
            // accordionControlElement16
            // 
            this.accordionControlElement16.Name = "accordionControlElement16";
            this.accordionControlElement16.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement16.Text = "Clean";
            this.accordionControlElement16.Click += new System.EventHandler(this.accordionControlElement16_Click);
            // 
            // accordionControlElement17
            // 
            this.accordionControlElement17.Name = "accordionControlElement17";
            this.accordionControlElement17.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement17.Text = "Only Config";
            this.accordionControlElement17.Click += new System.EventHandler(this.accordionControlElement17_Click);
            // 
            // accordionControlElement3
            // 
            this.accordionControlElement3.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement18});
            this.accordionControlElement3.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement3.ImageOptions.SvgImage")));
            this.accordionControlElement3.Name = "accordionControlElement3";
            toolTipItem2.Text = "In this module you are allow to manage \r\nASAI Services Installed on the kiosk";
            superToolTip2.Items.Add(toolTipItem2);
            this.accordionControlElement3.SuperTip = superToolTip2;
            this.accordionControlElement3.Text = "Services";
            // 
            // accordionControlElement18
            // 
            this.accordionControlElement18.Name = "accordionControlElement18";
            this.accordionControlElement18.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement18.Text = "Manage Service";
            this.accordionControlElement18.Click += new System.EventHandler(this.accordionControlElement18_Click);
            // 
            // accordionControlElement4
            // 
            this.accordionControlElement4.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement19});
            this.accordionControlElement4.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement4.ImageOptions.SvgImage")));
            this.accordionControlElement4.Name = "accordionControlElement4";
            toolTipItem3.Text = "In this module you can test the \r\nconfigurations Kiosk files";
            superToolTip3.Items.Add(toolTipItem3);
            this.accordionControlElement4.SuperTip = superToolTip3;
            this.accordionControlElement4.Text = "Test";
            // 
            // accordionControlElement19
            // 
            this.accordionControlElement19.Name = "accordionControlElement19";
            this.accordionControlElement19.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement19.Text = "Config Files";
            this.accordionControlElement19.Click += new System.EventHandler(this.accordionControlElement19_Click);
            // 
            // accordionControlElement5
            // 
            this.accordionControlElement5.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement21,
            this.accordionControlElement22});
            this.accordionControlElement5.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement5.ImageOptions.SvgImage")));
            this.accordionControlElement5.Name = "accordionControlElement5";
            toolTipItem4.Text = "In this module you will find \r\nAsai device testers in order to \r\ncheck devices he" +
    "alth";
            superToolTip4.Items.Add(toolTipItem4);
            this.accordionControlElement5.SuperTip = superToolTip4;
            this.accordionControlElement5.Text = "ASAI Testers";
            this.accordionControlElement5.Click += new System.EventHandler(this.accordionControlElement5_Click);
            // 
            // accordionControlElement21
            // 
            this.accordionControlElement21.Name = "accordionControlElement21";
            this.accordionControlElement21.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement21.Text = "JCM Printer";
            this.accordionControlElement21.Click += new System.EventHandler(this.accordionControlElement21_Click);
            // 
            // accordionControlElement22
            // 
            this.accordionControlElement22.Name = "accordionControlElement22";
            this.accordionControlElement22.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement22.Text = "Bv\'s Tester";
            this.accordionControlElement22.Click += new System.EventHandler(this.accordionControlElement22_Click);
            // 
            // accordionControlElement6
            // 
            this.accordionControlElement6.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement23});
            this.accordionControlElement6.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement6.ImageOptions.SvgImage")));
            this.accordionControlElement6.Name = "accordionControlElement6";
            toolTipItem5.Text = "In this module are able to install and manage\r\nthe traces service";
            superToolTip5.Items.Add(toolTipItem5);
            this.accordionControlElement6.SuperTip = superToolTip5;
            this.accordionControlElement6.Text = "Traces Service";
            // 
            // accordionControlElement23
            // 
            this.accordionControlElement23.Name = "accordionControlElement23";
            this.accordionControlElement23.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement23.Text = "Open Traces Manager";
            this.accordionControlElement23.Click += new System.EventHandler(this.accordionControlElement23_Click);
            // 
            // accordionControlElement7
            // 
            this.accordionControlElement7.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement24,
            this.accordionControlElement25,
            this.accordionControlElement26});
            this.accordionControlElement7.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement7.ImageOptions.SvgImage")));
            this.accordionControlElement7.Name = "accordionControlElement7";
            toolTipItem6.Text = "In this module you are allow to \r\nperform some Data Base activities \r\nrelated to " +
    "recovery.";
            superToolTip6.Items.Add(toolTipItem6);
            this.accordionControlElement7.SuperTip = superToolTip6;
            this.accordionControlElement7.Text = "Data Base";
            // 
            // accordionControlElement24
            // 
            this.accordionControlElement24.Name = "accordionControlElement24";
            this.accordionControlElement24.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement24.Text = "Test Db Connection";
            this.accordionControlElement24.Click += new System.EventHandler(this.accordionControlElement24_Click);
            // 
            // accordionControlElement25
            // 
            this.accordionControlElement25.Name = "accordionControlElement25";
            this.accordionControlElement25.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement25.Text = "DataBase Recovery";
            this.accordionControlElement25.Click += new System.EventHandler(this.accordionControlElement25_Click);
            // 
            // accordionControlElement26
            // 
            this.accordionControlElement26.Name = "accordionControlElement26";
            this.accordionControlElement26.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement26.Text = "Transactions Recovery";
            this.accordionControlElement26.Click += new System.EventHandler(this.accordionControlElement26_Click);
            // 
            // accordionControlElement8
            // 
            this.accordionControlElement8.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement27});
            this.accordionControlElement8.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement8.ImageOptions.SvgImage")));
            this.accordionControlElement8.Name = "accordionControlElement8";
            toolTipItem7.Text = "In this module you can test some \r\nserver connections";
            superToolTip7.Items.Add(toolTipItem7);
            this.accordionControlElement8.SuperTip = superToolTip7;
            this.accordionControlElement8.Text = "Connections";
            this.accordionControlElement8.Click += new System.EventHandler(this.accordionControlElement8_Click);
            // 
            // accordionControlElement27
            // 
            this.accordionControlElement27.Name = "accordionControlElement27";
            this.accordionControlElement27.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement27.Text = "Check Connections";
            this.accordionControlElement27.Click += new System.EventHandler(this.accordionControlElement27_Click);
            // 
            // accordionControlElement20
            // 
            this.accordionControlElement20.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement28});
            this.accordionControlElement20.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement20.ImageOptions.SvgImage")));
            this.accordionControlElement20.Name = "accordionControlElement20";
            this.accordionControlElement20.Text = "PF4 Files";
            // 
            // accordionControlElement28
            // 
            this.accordionControlElement28.Name = "accordionControlElement28";
            this.accordionControlElement28.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement28.Text = "Asai OneTouch";
            this.accordionControlElement28.Click += new System.EventHandler(this.accordionControlElement28_Click);
            // 
            // accordionControlElement13
            // 
            this.accordionControlElement13.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement13.ImageOptions.SvgImage")));
            this.accordionControlElement13.Name = "accordionControlElement13";
            this.accordionControlElement13.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement13.Text = "EXIT";
            this.accordionControlElement13.Click += new System.EventHandler(this.accordionControlElement13_Click);
            // 
            // fluentDesignFormControl1
            // 
            this.fluentDesignFormControl1.FluentDesignForm = this;
            this.fluentDesignFormControl1.Location = new System.Drawing.Point(0, 0);
            this.fluentDesignFormControl1.Manager = this.fluentFormDefaultManager1;
            this.fluentDesignFormControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fluentDesignFormControl1.Name = "fluentDesignFormControl1";
            this.fluentDesignFormControl1.Size = new System.Drawing.Size(1089, 0);
            this.fluentDesignFormControl1.TabIndex = 2;
            this.fluentDesignFormControl1.TabStop = false;
            // 
            // fluentFormDefaultManager1
            // 
            this.fluentFormDefaultManager1.DockingEnabled = false;
            this.fluentFormDefaultManager1.Form = this;
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement9,
            this.accordionControlElement10,
            this.accordionControlElement11,
            this.accordionControlElement12});
            this.accordionControlElement2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("accordionControlElement2.ImageOptions.SvgImage")));
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Text = "Image";
            // 
            // accordionControlElement9
            // 
            this.accordionControlElement9.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.accordionControlElement9.Name = "accordionControlElement9";
            this.accordionControlElement9.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement9.Text = "Back Up";
            // 
            // accordionControlElement10
            // 
            this.accordionControlElement10.Name = "accordionControlElement10";
            this.accordionControlElement10.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement10.Text = "Restore";
            // 
            // accordionControlElement11
            // 
            this.accordionControlElement11.Name = "accordionControlElement11";
            this.accordionControlElement11.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement11.Text = "Clean";
            // 
            // accordionControlElement12
            // 
            this.accordionControlElement12.Name = "accordionControlElement12";
            this.accordionControlElement12.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement12.Text = "Only Config";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(449, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(505, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "PF4 Suite Tools CashStream Installation Suite";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Location = new System.Drawing.Point(960, 24);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(47, 49);
            this.panel1.TabIndex = 4;
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.Transparent;
            this.panel.Location = new System.Drawing.Point(292, 91);
            this.panel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(795, 521);
            this.panel.TabIndex = 5;
            // 
            // help
            // 
            this.help.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.help.Appearance.ForeColor = System.Drawing.Color.White;
            this.help.Appearance.Options.UseFont = true;
            this.help.Appearance.Options.UseForeColor = true;
            this.help.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("help.ImageOptions.SvgImage")));
            this.help.Location = new System.Drawing.Point(309, 12);
            this.help.Name = "help";
            this.help.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.help.Size = new System.Drawing.Size(114, 49);
            this.help.TabIndex = 6;
            this.help.Text = "Help";
            this.help.Click += new System.EventHandler(this.help_Click);
            // 
            // InstallationWizardF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(1089, 707);
            this.ControlBox = false;
            this.Controls.Add(this.help);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.accordionControl1);
            this.Controls.Add(this.fluentDesignFormControl1);
            this.FluentDesignFormControl = this.fluentDesignFormControl1;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.IconOptions.Image = ((System.Drawing.Image)(resources.GetObject("InstallationWizardF.IconOptions.Image")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "InstallationWizardF";
            this.NavigationControl = this.accordionControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.InstallationWizardF_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentFormDefaultManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl1;
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl fluentDesignFormControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager fluentFormDefaultManager1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement9;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement10;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement11;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement12;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement5;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement6;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement7;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement8;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement13;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement14;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement15;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement16;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement17;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement18;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement19;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement21;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement22;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement23;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement24;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement25;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement26;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement27;
        private DevExpress.XtraEditors.SimpleButton help;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement20;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement28;
    }
}