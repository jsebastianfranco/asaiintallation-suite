﻿namespace InstallationWizard
{
    partial class Services
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Services));
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnRestartServices = new DevExpress.XtraEditors.SimpleButton();
            this.btnStopServices = new DevExpress.XtraEditors.SimpleButton();
            this.btnStartServices = new DevExpress.XtraEditors.SimpleButton();
            this.lblServices = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Titillium Web", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label13.Location = new System.Drawing.Point(447, 143);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(199, 43);
            this.label13.TabIndex = 63;
            this.label13.Text = "Found Services";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Titillium Web", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(112, 153);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 43);
            this.label12.TabIndex = 62;
            this.label12.Text = "Actions";
            // 
            // btnRestartServices
            // 
            this.btnRestartServices.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.btnRestartServices.Appearance.Font = new System.Drawing.Font("Titillium Web", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestartServices.Appearance.Options.UseBackColor = true;
            this.btnRestartServices.Appearance.Options.UseFont = true;
            this.btnRestartServices.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton9.ImageOptions.SvgImage")));
            this.btnRestartServices.Location = new System.Drawing.Point(77, 339);
            this.btnRestartServices.Name = "btnRestartServices";
            this.btnRestartServices.Size = new System.Drawing.Size(179, 49);
            this.btnRestartServices.TabIndex = 61;
            this.btnRestartServices.Text = "Reset";
            this.btnRestartServices.Click += new System.EventHandler(this.btnRestartServices_Click);
            // 
            // btnStopServices
            // 
            this.btnStopServices.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.btnStopServices.Appearance.Font = new System.Drawing.Font("Titillium Web", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopServices.Appearance.Options.UseBackColor = true;
            this.btnStopServices.Appearance.Options.UseFont = true;
            this.btnStopServices.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton5.ImageOptions.SvgImage")));
            this.btnStopServices.Location = new System.Drawing.Point(77, 277);
            this.btnStopServices.Name = "btnStopServices";
            this.btnStopServices.Size = new System.Drawing.Size(179, 49);
            this.btnStopServices.TabIndex = 55;
            this.btnStopServices.Text = "Stop";
            this.btnStopServices.Click += new System.EventHandler(this.btnStopServices_Click);
            // 
            // btnStartServices
            // 
            this.btnStartServices.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.btnStartServices.Appearance.Font = new System.Drawing.Font("Titillium Web", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartServices.Appearance.Options.UseBackColor = true;
            this.btnStartServices.Appearance.Options.UseFont = true;
            this.btnStartServices.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton6.ImageOptions.SvgImage")));
            this.btnStartServices.Location = new System.Drawing.Point(77, 211);
            this.btnStartServices.Name = "btnStartServices";
            this.btnStartServices.Size = new System.Drawing.Size(179, 49);
            this.btnStartServices.TabIndex = 54;
            this.btnStartServices.Text = "Start";
            this.btnStartServices.Click += new System.EventHandler(this.btnStartServices_Click);
            // 
            // lblServices
            // 
            this.lblServices.AutoSize = true;
            this.lblServices.BackColor = System.Drawing.Color.Transparent;
            this.lblServices.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServices.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblServices.Location = new System.Drawing.Point(405, 211);
            this.lblServices.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServices.Name = "lblServices";
            this.lblServices.Size = new System.Drawing.Size(34, 29);
            this.lblServices.TabIndex = 57;
            this.lblServices.Text = "...";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Titillium Web", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label24.Location = new System.Drawing.Point(40, 36);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(308, 66);
            this.label24.TabIndex = 56;
            this.label24.Text = "Mode: Services";
            // 
            // Services
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnRestartServices);
            this.Controls.Add(this.btnStopServices);
            this.Controls.Add(this.btnStartServices);
            this.Controls.Add(this.lblServices);
            this.Controls.Add(this.label24);
            this.Name = "Services";
            this.Size = new System.Drawing.Size(838, 563);
            this.Load += new System.EventHandler(this.Services_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.SimpleButton btnRestartServices;
        private DevExpress.XtraEditors.SimpleButton btnStopServices;
        private DevExpress.XtraEditors.SimpleButton btnStartServices;
        private System.Windows.Forms.Label lblServices;
        private System.Windows.Forms.Label label24;
    }
}
