﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using InstallationWizardClass;
using System.IO;
using System.Xml;
using System.Threading;
using System.Net;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace InstallationWizard
{
    public partial class CheckConnections : UserControl
    {
        DataSet ds = new DataSet();
        string cashUrl = "";
        string macAdd = "";
        string ipAddress = "";
        private static CheckConnections _instance;
        public static CheckConnections Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CheckConnections();
                return _instance;
            }
        }

        public CheckConnections()
        {
            InitializeComponent();
        }

        private void CheckConnections_Load(object sender, EventArgs e)
        {
            
            readLOXml();
            readCashXml();
            readCdsKey();

            simpleButton1.Enabled = true;

            if (this.txtConnections1.TextLength==0)
            {
                simpleButton1.Enabled = false;
                MessageBox.Show("Error Trying to read Live Office Url", "Live office Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wLogging.log("Error Trying to read Live Office Url");
            }

            if (this.txtConnections2.TextLength == 0)
            {
                simpleButton2.Enabled = false;
                MessageBox.Show("Error Trying to read Cash Advance Url", "Cash Advance Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wLogging.log("Error Trying to read Cash Advance Url");
            }

            if (this.txtConnections2.TextLength == 0)
            {
                simpleButton4.Enabled = false;
                MessageBox.Show("Error Trying to read CDS IP", "Live office Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wLogging.log("Error Trying to read CDS IP", "Live office Connection");
            }
        }


        private void simpleButton3_Click(object sender, EventArgs e)
        {
            
            //reading CashAdvance Service file xml by elements
            wLogging.log("Reading cash advance element on xml ");
            try
            {
                //Todo Decrypt

                if (File.Exists(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml"))
                {
                    File.Delete(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml");
                }

                string j = KioskConfiguration.Generic.MfkCrypto.Decrypt(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfo.xml");

                

                XmlDocument xmlDc2 = new XmlDocument();
                xmlDc2.LoadXml(j);
                xmlDc2.Save(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml");

                Thread.Sleep(2000);

                

                XmlNodeList elemList = xmlDc2.GetElementsByTagName("ServerURL");

                for (int i = 0; i < elemList.Count; i++)
                {
                    
                    //txtConnections2.Text = elemList[i].InnerXml;
                    cashUrl = elemList[i].InnerXml;
                }


                //searching the macAddress
                Thread.Sleep(1000);
                
                
                XmlNodeList elemList2 = xmlDc2.GetElementsByTagName("MACAddress");

                for (int i = 0; i < elemList2.Count; i++)
                {
                    //txtCashMac.Text = elemList2[i].InnerXml;
                    macAdd = elemList2[i].InnerXml;
                }

                //MessageBox.Show(macAdd);

                File.Delete(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml");

                //Todo read files Directory
                //Directory.GetFiles("ruta");
                //foreach()

            }
            catch (Exception err)
            {
                wLogging.log("Error reading cash advance xml element " + err);
                MessageBox.Show("Error reading cash advance xml element" + err);
            }



            wLogging.log("Reading get fee ");
            try
            {
                
                FeeRequest(cashUrl.Trim(),macAdd.Trim());

            }
            catch (Exception ex2)
            {

                wLogging.log("Error Trying to call get fees method  " + ex2);

                MessageBox.Show("Error trying to call get fees method" + ex2);
            }

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Unable to Connect", "Live office Connection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            
        }

        public IDSCashAdvanceDataContract.FeesStructure FeeRequest(string cashUrl0, string macAdd) //Last
        {

            string cashUrl = cashUrl0 + @"/getfeeamount?";
            IDSCashAdvanceDataContract.FeesStructure feeConfig=null;
            //MessageBox.Show(cashUrl);
            //MessageBox.Show(macAdd);

            wLogging.log("Starting to stablish de the connection with Cash Advance Server");
            try
            {
                BasicHttpBinding httpBinding = CreateBinding();
                EndpointAddress endPoint = new EndpointAddress(cashUrl.Trim());
                DSCashAdvanceServiceContractClient ids = new DSCashAdvanceServiceContractClient(httpBinding, endPoint);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertficate;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                ids.Open();
                IDSCashAdvanceDataContract.TerminalConfiguration terminalConfig = ids.GetTerminalConfiguration(macAdd);//gonza
                feeConfig = ids.GetFees(terminalConfig.TerminalID);
                ids.Close();

                dataGridView1.ColumnCount = 1;
                dataGridView1.Columns[0].Width = 0;
                dataGridView1.DataSource = feeConfig;
                

            }
            catch (Exception err)
            {
                wLogging.log("Error Trying to call get fees method  " + err);
                MessageBox.Show("Error Trying to connect to Cash Advance Server", "Cash Advance Connection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return feeConfig;

        }


        private BasicHttpBinding CreateBinding()
        {
            BasicHttpBinding httpBinding = new BasicHttpBinding();
            httpBinding.Name = "IDSCashAdvanceEndpoint";
            TimeSpan UsedTimeConfig = new TimeSpan(0, 1, 0);
            httpBinding.CloseTimeout = UsedTimeConfig;
            httpBinding.OpenTimeout = UsedTimeConfig;
            httpBinding.SendTimeout = UsedTimeConfig;
            UsedTimeConfig = new TimeSpan(0, 10, 0);
            httpBinding.ReceiveTimeout = UsedTimeConfig;
            httpBinding.AllowCookies = false;
            httpBinding.BypassProxyOnLocal = false;
            httpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            httpBinding.MaxBufferSize = 65536;
            httpBinding.MaxBufferPoolSize = 524288;
            httpBinding.MaxReceivedMessageSize = 65536;
            httpBinding.MessageEncoding = WSMessageEncoding.Text;
            httpBinding.TextEncoding = Encoding.UTF8;
            httpBinding.TransferMode = TransferMode.Buffered;
            httpBinding.UseDefaultWebProxy = true;
            System.Xml.XmlDictionaryReaderQuotas ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas();
            ReaderQuotas.MaxDepth = 32;
            ReaderQuotas.MaxStringContentLength = 8192;
            ReaderQuotas.MaxArrayLength = 16384;
            ReaderQuotas.MaxBytesPerRead = 4096;
            ReaderQuotas.MaxNameTableCharCount = 16384;
            httpBinding.ReaderQuotas = ReaderQuotas;
            BasicHttpSecurity httpSecurity = httpBinding.Security;

            //if (SecurityMode==true)
                httpSecurity.Mode = BasicHttpSecurityMode.Transport;
            //else
            //    httpSecurity.Mode = BasicHttpSecurityMode.None;
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpTransportSecurity TransportSecurity = httpSecurity.Transport;
            TransportSecurity.ClientCredentialType = HttpClientCredentialType.None;
            TransportSecurity.ProxyCredentialType = HttpProxyCredentialType.None;
            TransportSecurity.Realm = "";
            BasicHttpMessageSecurity MessageSecurity = httpSecurity.Message;
            MessageSecurity.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            MessageSecurity.AlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Default;
            return httpBinding;

        }

        private bool ValidateServerCertficate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate cert,
            System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_2(object sender, EventArgs e)
        {

        }

        private void readLOXml()
        {
            //read xml node by searching its attribute Kiosk Service URl
            wLogging.log("Reading kiosk atribute on xml file ");
            try
            {
                XmlDocument xmlDc = new XmlDocument();
                xmlDc.Load(@"C:\ASAI\Services\MFKServices\BOSWebSvcSettings.config");

                XmlNodeList xnList = xmlDc.SelectNodes("/BOSWebSvcAPI.Properties.BOSWebSvcSettings/setting[@name='KioskServiceURL']");

                foreach (XmlNode xn in xnList)
                {

                    string conf1 = xn["value"].InnerText;
                    txtConnections1.Text = conf1;
                }
            }
            catch (Exception err)
            {
                wLogging.log("Error reading kiosk atribute on xml file " + err);
                MessageBox.Show("Error reading kiosk atribute on xml file" + err,"Reading LO from Xml");
            }
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            //test htpp lo
            string resultLoUrl;

            try
            {

                WebRequest request = WebRequest.Create(txtConnections1.Text.Trim());
                WebResponse response = request.GetResponse();
                resultLoUrl = (((HttpWebResponse)response).StatusDescription);

                MessageBox.Show(resultLoUrl, "Live office Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (Exception err)
            {
                MessageBox.Show("Error: no connection with live office -- " + err,"Checking LO Connection",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
          
        }

        private void readCashXml()
        {
            //reading CashAdvance Service file xml by elements
            wLogging.log("Reading cash advance element on xml ");
            try
            {
                //Todo Decrypt

                if (File.Exists(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml"))
                {
                    File.Delete(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml");
                }

                string j = KioskConfiguration.Generic.MfkCrypto.Decrypt(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfo.xml");



                XmlDocument xmlDc2 = new XmlDocument();
                xmlDc2.LoadXml(j);
                xmlDc2.Save(@"C:\ASAI\CacheConfigFile\ConfigFileCashAdvanceServerInfoTemp.xml");

                Thread.Sleep(2000);



                XmlNodeList elemList = xmlDc2.GetElementsByTagName("ServerURL");

                for (int i = 0; i < elemList.Count; i++)
                {

                    txtConnections2.Text = elemList[i].InnerXml;
                    //cashUrl = elemList[i].InnerXml;
                }
            }
            catch (Exception ex)
            {
                wLogging.log("Error Trying to reading Cash Adv Url " + ex);
                MessageBox.Show("Error reading the URL" + ex, "Reding Cash Adv Url");
            }
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            //test cash adv
            string resultCsUrl;

            try
            {

                WebRequest request = WebRequest.Create(txtConnections2.Text.Trim());
                WebResponse response = request.GetResponse();
                resultCsUrl = (((HttpWebResponse)response).StatusDescription);

                MessageBox.Show(resultCsUrl,"Cash Adv Connection",MessageBoxButtons.OK,MessageBoxIcon.Information);


            }
            catch (Exception err)
            {
                MessageBox.Show("Error: no connection with cash Advance -- " + err, "Checking Cash Adv Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void readCdsKey()
        {

            //reading the cds registry key
            wLogging.log("Reading registry key ");
            try
            {
                
                RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Wincor Nixdorf\ProTopas\CurrentVersion\CCOPEN\COMMUNICATION\SSL\CLIENT");
                {
                    if (key != null)
                    {
                        ipAddress= key.GetValue("RemotePeer").ToString().Trim();
                        txtConnections3.Text = key.GetValue("RemotePeer").ToString().Trim();
                    }
                }

            }
            catch (Exception ex)
            {
                wLogging.log("Error Trying to read registry key " + ex);
                MessageBox.Show("Error reading the key" + ex, "Reding Cash Adv Key");
            }
        
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            TestCDS();
        }

        public void TestCDS()
        {
            //Testing CDS conneciton regKey
            string cdsIp = ReadCdsKey();
            string cdsPort = ReadCdsPortKey();
            string[] split = cdsIp.Split(',');


            wLogging.log("Starting TestCDS method");

            TcpClient tcpClient = new TcpClient();

            foreach (string item in split)
            {
                try
                {

                    tcpClient.Connect(item, Int32.Parse(cdsPort));


                    if (tcpClient.Connected)
                    {
                        MessageBox.Show("CDS Connection: SUCCESS", "CDS Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        wLogging.log("CDS Connection: SUCCESS");
                        
                    }
                    else if (tcpClient.Connected == false)
                    {

                        MessageBox.Show("Error: no connection with CDS -- " , "Checking CDS Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        wLogging.log("CDS Connection Error ");
                        
                    }

                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: CDS connection error -- "+err, "Checking CDS Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    wLogging.log("CDS Connection Error: " + err.Message);
                    
                }
            }

        }

        public string ReadCdsKey()
        {
            string regKeyCds = string.Empty;
            //reading the cds registry key
            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Wincor Nixdorf\ProTopas\CurrentVersion\CCOPEN\COMMUNICATION\SSL\CLIENT"))
                {
                    if (key != null)
                    {
                        string o = key.GetValue("RemotePeer").ToString();
                        if (o != null)
                        {
                            regKeyCds = o;

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                wLogging.log("Error Trying to read registry key " + ex);
            }

            return regKeyCds;
        }

        public string ReadCdsPortKey()
        {
            string regKeyCdsPort = string.Empty;
            //reading the cds registry key
            try
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Wincor Nixdorf\ProTopas\CurrentVersion\CCOPEN\COMMUNICATION\SSL\CLIENT"))
                {
                    if (key != null)
                    {
                        string o = key.GetValue("RemotePort").ToString();
                        if (o != null)
                        {

                            regKeyCdsPort = o;

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                wLogging.log("Error Trying to read registry key " + ex);
            }

            return regKeyCdsPort;
        }

        private void txtConnections3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
