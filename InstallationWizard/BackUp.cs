﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using InstallationWizardClass;
using System.Configuration;
using System.IO;
using System.Security.AccessControl;
using System.Data.SqlClient;
using System.Diagnostics;

namespace InstallationWizard
{
    public partial class BackUp : UserControl
    {
        public int timer;
        public string showStatus, resultStatus, bp, kioskstatus, starting;
        private static BackUp _instance;
        
        public static BackUp Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BackUp();
                return _instance;
            }
            
        }

        public BackUp()
        {         
            InitializeComponent();
            bwBackgroundWorkerBackUp.DoWork += new DoWorkEventHandler(bwBackgroundWorkerBackUp_DoWork);

            string files = "";
            timer = 1;
            //Show the files and folders to process
            files = "Folder to copy BackUP: " + ConfigurationManager.AppSettings["ASAIFOLDER"] + "\n\n";
            files = files + "Files to Copy:\n";
            int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
            for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            {
                string app = "B" + rowCounter;
                files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "") + "\n";
                timer += 1;
            }

            files = files + "\nFolders to Copy:\n";
            copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
            for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            {
                string app = "C" + rowCounter;
                files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "") + "\n";
                timer += 1;
            }

            files = files + "\n" + timer + ".Database BackUp.";
            timer += 1;
            lblBackup.Text = files;

            string folder = ConfigurationManager.AppSettings["ASAIFOLDER"];
            string backup = System.IO.Path.Combine(folder, "MfkLocalLog.bak");
            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }
        }


        private void BackUp_Load(object sender, EventArgs e)
        {
            circularProgressBar1.Value = 0;
            circularProgressBar1.Minimum = 0;
            circularProgressBar1.Maximum = 100;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            circularProgressBar1.Visible = true;

            //var sq = new Create();
            //string folder = ConfigurationManager.AppSettings["ASAIFOLDER"];
            ////lblProgressBar.Text = "BackUp Kiosk Started\n\n0. Start giving permision to " + folder;
            //string backup = System.IO.Path.Combine(folder, "MfkLocalLog.bak");
            //if (!System.IO.Directory.Exists(folder))
            //{
            //    System.IO.Directory.CreateDirectory(folder);
            //}
            ////Create file for database backup.

            //if (!System.IO.File.Exists(backup))
            //{
            //    System.IO.File.Create(backup);
            //    //Give permision to backup file(correcting error to create databases backup)
            //    DirectoryInfo dInfo = new DirectoryInfo(backup);
            //    DirectorySecurity dSecurity = dInfo.GetAccessControl();
            //    dSecurity.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
            //    dInfo.SetAccessControl(dSecurity);
            //}

          

            for (int i = 1; i <= 57; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString()+"%";
            }

            

            ProcessStartInfo info = new ProcessStartInfo("sqlcmd", @" -S . -U sa -P asai1234 -i C:\ASAI\InstallationWizard\Files\Usp_BackupDB.sql");
            //  Indicades if the Operative System shell is used, in this case it is not
            info.UseShellExecute = false;
            //No new window is required
            info.CreateNoWindow = true;
            //The windows style will be hidden
            info.WindowStyle = ProcessWindowStyle.Hidden;
            //The output will be read by the starndar output process
            info.RedirectStandardOutput = true;
            Process proc = new Process();
            proc.StartInfo = info;
            //Start the process
            proc.Start();
            Thread.Sleep(1000);
            //btnBackUp.Enabled = false;
            //bwBackgroundWorkerBackUp.RunWorkerAsync();

            for (int i = 57; i <= 80; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
            }

            Bk();

            for (int i = 80; i <= 100; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
            }


            if (circularProgressBar1.Value == 100)
            {
                lblProgressBar.Text = "Completed";
            }

            

            



        }

        private void circularProgressBar1_Click(object sender, EventArgs e)
        {

        }

        protected void Bk()
        {
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            //decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;

            //Check in database to make sure this scripts exists.
            showStatus += sq.ScriptsDataBase();
            countTimer += 1;

            //Time when start the work
            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            string targetPath = ConfigurationManager.AppSettings["ASAIFOLDER"];
            status = "BackUp Kiosk Started\n\n";
            int copyCount = 0;


            if (bContinue)
            {
                //Try to give permision to Backup Folder
                try
                {
                    //If backup folder don't exist create it
                    if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["ASAIFOLDER"]))
                    {
                        System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    }
                    else
                    {
                        //Delete existing files in BackupFolder if exist
                        try
                        {
                            if (bContinue)
                            {
                                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
                                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                                {
                                    dtStart = DateTime.Now;
                                    string app = "B" + rowCounter;
                                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "");
                                    string fileName = System.IO.Path.GetFileName(full);
                                    string destFile = System.IO.Path.Combine(targetPath, fileName);
                                    if (System.IO.File.Exists(destFile))
                                    {
                                        System.IO.File.Delete(destFile);
                                    }
                                }
                                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
                                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                                {
                                    dtStart = DateTime.Now;
                                    string app = "C" + rowCounter;
                                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "");
                                    string fileName = System.IO.Path.GetFileName(full);
                                    string destFile = System.IO.Path.Combine(targetPath, fileName + ".zip");
                                    if (System.IO.File.Exists(destFile))
                                    {
                                        System.IO.File.Delete(destFile);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    //Give permision to Asai Folder Backup
                    string path = (ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    DirectorySecurity ds = Directory.GetAccessControl(path);
                    FileSystemAccessRule fs = new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow);
                    ds.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
                    Directory.SetAccessControl(path, ds);
                    showStatus = "0. Give permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + " Successful. \n";
                }
                catch (Exception ex)
                {
                    showStatus = "0.Error giving permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". Error: " + ex.Message + ". \n";
                    bContinue = false;
                }
                //Give 1 second for apply the permisions
                Thread.Sleep(3000);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;

                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));

            }

            if (bContinue)
            {
                // Copy B files from appconfig (connecting liveoffice files)
                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings["B" + rowCounter], "");
                    string sourcePath = System.IO.Path.GetDirectoryName(full);
                    string destFile = System.IO.Path.Combine(targetPath, System.IO.Path.GetFileName(full));
                    showStatus += count + ".Start Coping File " + System.IO.Path.GetFileName(destFile) + " in " + destFile + ". \n";
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                    //Copy file process
                    showStatus = sq.BackUpDataBase_CopyBFiles(targetPath, count, full, destFile, sourcePath);
                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                }
            }


            if (bContinue)
            {
                //Copy C folders from appconfig (folders to copy)
                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string sourcePath = System.IO.Path.Combine(ConfigurationManager.AppSettings["C" + rowCounter], "");
                    string name = System.IO.Path.GetFileName(sourcePath);
                    string zipname = targetPath + @"\" + name + ".zip";
                    showStatus += count + ".Start Coping folder " + sourcePath + " in " + zipname + ". \n";
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                    //Copy folders process
                    showStatus = sq.BackUpDataBase_CopyCFolders(targetPath, count, sourcePath);
                    //Calculate time take doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        countTimer -= 1;
                        bContinue = false;
                    }
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                }
            }


            if (bContinue)
            {


                count += 1;
                dtStart = DateTime.Now;
                showStatus += count + ". Starting Backup Database. \n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                //Backup database process
                showStatus = sq.BackUpDataBase_CopyDataBase(targetPath, count);
                //Calculate time taked doing database backup
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;

               

                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nBackUp Failed See Below Errors.\n";
                    showStatus += "\nBackUp Failed See Below Errors.\n";
                    countTimer -= 1;
                    bContinue = false;
                }
                else
                {
                    status += "\nBackUp Completed Successfully. \n";
                    showStatus += "\nBackUp Completed Successfully. \n";
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
            }


            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"BackUp\");
            //Report Last Progress
            //bwBackgroundWorkerBackUp.ReportProgress(101);

            //if (bwBackgroundWorkerBackUp.CancellationPending)
            //{
            //    //e.Cancel = true;
            //    return;
            //}


        }

        #region backgroundworkerBackUp
        private void bwBackgroundWorkerBackUp_DoWork(object sender, DoWorkEventArgs e)
        {
            #region backgroundworkerBackUp Start
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;

            //Check in database to make sure this scripts exists.
            showStatus += sq.ScriptsDataBase();
            countTimer += 1;

            //Time when start the work
            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            string targetPath = ConfigurationManager.AppSettings["ASAIFOLDER"];
            status = "BackUp Kiosk Started\n\n";
            int copyCount = 0;
            #endregion

            #region backgroundworkerBackUp PermisionAsaiFolderBackup
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                //Try to give permision to Backup Folder
                try
                {
                    //If backup folder don't exist create it
                    if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["ASAIFOLDER"]))
                    {
                        System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    }
                    else
                    {
                        //Delete existing files in BackupFolder if exist
                        try
                        {
                            if (bContinue)
                            {
                                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
                                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                                {
                                    dtStart = DateTime.Now;
                                    string app = "B" + rowCounter;
                                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "");
                                    string fileName = System.IO.Path.GetFileName(full);
                                    string destFile = System.IO.Path.Combine(targetPath, fileName);
                                    if (System.IO.File.Exists(destFile))
                                    {
                                        System.IO.File.Delete(destFile);
                                    }
                                }
                                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
                                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                                {
                                    dtStart = DateTime.Now;
                                    string app = "C" + rowCounter;
                                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "");
                                    string fileName = System.IO.Path.GetFileName(full);
                                    string destFile = System.IO.Path.Combine(targetPath, fileName + ".zip");
                                    if (System.IO.File.Exists(destFile))
                                    {
                                        System.IO.File.Delete(destFile);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    //Give permision to Asai Folder Backup
                    string path = (ConfigurationManager.AppSettings["ASAIFOLDER"]);
                    DirectorySecurity ds = Directory.GetAccessControl(path);
                    FileSystemAccessRule fs = new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow);
                    ds.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
                    Directory.SetAccessControl(path, ds);
                    showStatus = "0. Give permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + " Successful. \n";
                }
                catch (Exception ex)
                {
                    showStatus = "0.Error giving permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". Error: " + ex.Message + ". \n";
                    bContinue = false;
                }
                //Give 1 second for apply the permisions
                Thread.Sleep(3000);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;

                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));

            }
            #endregion

            #region backgroundworkerBackUp B Files
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                // Copy B files from appconfig (connecting liveoffice files)
                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings["B" + rowCounter], "");
                    string sourcePath = System.IO.Path.GetDirectoryName(full);
                    string destFile = System.IO.Path.Combine(targetPath, System.IO.Path.GetFileName(full));
                    showStatus += count + ".Start Coping File " + System.IO.Path.GetFileName(destFile) + " in " + destFile + ". \n";
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                    //Copy file process
                    showStatus = sq.BackUpDataBase_CopyBFiles(targetPath, count, full, destFile, sourcePath);
                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                }
            }
            #endregion

            #region backgroundworkerBackUp C Folders
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                //Copy C folders from appconfig (folders to copy)
                copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string sourcePath = System.IO.Path.Combine(ConfigurationManager.AppSettings["C" + rowCounter], "");
                    string name = System.IO.Path.GetFileName(sourcePath);
                    string zipname = targetPath + @"\" + name + ".zip";
                    showStatus += count + ".Start Coping folder " + sourcePath + " in " + zipname + ". \n";
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                    //Copy folders process
                    showStatus = sq.BackUpDataBase_CopyCFolders(targetPath, count, sourcePath);
                    //Calculate time take doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        countTimer -= 1;
                        bContinue = false;
                    }
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                }
            }
            #endregion

            #region backgroundworkerBackUp Database
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                showStatus += count + ". Starting Backup Database. \n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
                //Backup database process
                showStatus = sq.BackUpDataBase_CopyDataBase(targetPath, count);
                //Calculate time taked doing database backup
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nBackUp Failed See Below Errors.\n";
                    showStatus += "\nBackUp Failed See Below Errors.\n";
                    countTimer -= 1;
                    bContinue = false;
                }
                else
                {
                    status += "\nBackUp Completed Successfully. \n";
                    showStatus += "\nBackUp Completed Successfully. \n";
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region backgroundworkerBackUp Endprocess
            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerBackUp.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"BackUp\");
            //Report Last Progress
            bwBackgroundWorkerBackUp.ReportProgress(101);

            if (bwBackgroundWorkerBackUp.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            #endregion
        }

        private void bwBackgroundWorkerBackUp_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //When progresspercentage is 101 it means the work finished
            if (e.ProgressPercentage == 101)
            {
                MessageBox.Show(resultStatus, "Backup Result");
                //Close the application
                //Application.Exit();
            }
            else
            {


                if (circularProgressBar1.Value == 100)
                {
                    //Backup process finished. Wait for 101 to show the report to the user
                    //lblProgressBar.Text = "Completed " + e.ProgressPercentage + "%\n" + showStatus;
                }
                else
                {
                    //Report the progress.
                    //lblProgressBar.Text = "BackUp........ " + e.ProgressPercentage + "%\n" + showStatus + "\n";

                }
            }
        }

        private void bwBackgroundWorkerBackUp_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("The task has been cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error. Details: " + (e.Error as Exception).ToString());
            }
            else
            {
                MessageBox.Show(showStatus, "Backup Result");
                Application.Exit();
            }

        }

        #endregion

    }
}
