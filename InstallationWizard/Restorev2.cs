﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.AccessControl;
using System.Configuration;
using InstallationWizardClass;
using System.Threading;

namespace InstallationWizard
{
    public partial class Restorev2 : UserControl
    {
        public int timer;
        public string showStatus, resultStatus, bp, kioskstatus, starting;

        private static Restorev2 _instance;
        public static Restorev2 Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Restorev2();
                return _instance;
            }
        }

        public Restorev2()
        {
            InitializeComponent();
            string files = "";
            timer = 1;
            //Show the files and folders to process
            files = "Folder to take the Restore: " + ConfigurationManager.AppSettings["ASAIFOLDER"] + "\n\n";
            files = files + "Files to Restore:\n";
            int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
            for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            {
                string app = "B" + rowCounter;
                files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "") + "\n";
                timer += 1;
            }

            files = files + "\nFolders to Restore:\n";
            copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
            for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            {
                string app = "C" + rowCounter;
                files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "") + "\n";
                timer += 1;
            }

            files = files + "\n" + timer + ".Database Restore.";
            timer += 1;
            lblRestore.Text = files;
        }

        private void Restorev2_Load(object sender, EventArgs e)
        {
            circularProgressBar1.Value = 0;
            circularProgressBar1.Minimum = 0;
            circularProgressBar1.Maximum = 100;
        }

       

      

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            circularProgressBar1.Visible = true;
            //var sq = new Create();
            string folder = ConfigurationManager.AppSettings["ASAIFOLDER"];
            bp = System.IO.Path.Combine(folder, "BackUp" + (DateTime.Now.ToString("yyyyMMddHHmmss")));
            string backup = System.IO.Path.Combine(bp, "MfkLocalLog.bak");
            if (System.IO.Directory.Exists(folder))
            {
                if (!System.IO.Directory.Exists(bp))
                {
                    System.IO.Directory.CreateDirectory(bp);
                }

                for (int i = 1; i <= 22; i++)
                {
                    Thread.Sleep(20);
                    circularProgressBar1.Value = i;
                    circularProgressBar1.Update();
                    circularProgressBar1.Text = i.ToString() + "%";
                }

                ////Create file for database backup.
                if (!System.IO.File.Exists(backup))
                {
                    System.IO.File.Create(backup);

                    for (int i = 22; i <= 35; i++)
                    {
                        Thread.Sleep(20);
                        circularProgressBar1.Value = i;
                        circularProgressBar1.Update();
                        circularProgressBar1.Text = i.ToString() + "%";
                    }
                }
                else
                {
                    wLogging.log(backup);
                    System.IO.File.Delete(backup);
                    System.IO.File.Create(backup);
                }
                ////Give permision to backup file(correcting error to create databases backup)
            DirectoryInfo dInfo = new DirectoryInfo(backup);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
        }
            //btnRestore.Enabled = false;
            //bwBackgroundWorkerRestore.RunWorkerAsync();

            for (int i = 35; i <= 69; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
            }

            Rst();

            for (int i = 69; i <= 100; i++)
            {
                Thread.Sleep(20);
                circularProgressBar1.Value = i;
                circularProgressBar1.Update();
                circularProgressBar1.Text = i.ToString() + "%";
            }

            if (circularProgressBar1.Value == 100)
            {
                lblProgressBar.Text = "Completed";
            }

        }



        protected void Rst()
        {
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            //decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;

            showStatus += sq.ScriptsDataBase();
            countTimer += 1;

            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            string sourcePath = ConfigurationManager.AppSettings["ASAIFOLDER"];
            string backupPath = bp;
            status = "Restore Kiosk Started\n\nBackUpFolder Before Installation: " + backupPath + "\n\n";
          
            if (bContinue)
            {
                //Try to give permision to Backup Folder
                try
                {
                    //If backup folder don't exist show the error
                    if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["ASAIFOLDER"]))
                    {
                        showStatus = "0.Restore Error couldn't find " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". \n";
                        bContinue = false;
                    }
                    else
                    {
                        //Give permision to Asai Folder Backup
                        string path = (ConfigurationManager.AppSettings["ASAIFOLDER"]);
                        DirectorySecurity ds = Directory.GetAccessControl(path);
                        FileSystemAccessRule fs = new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow);
                        ds.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
                        Directory.SetAccessControl(path, ds);
                        showStatus = "0. Give permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + " Successful.\n";
                    }
                }
                catch (Exception ex)
                {
                    showStatus = "0.Error giving permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". Error: " + ex.Message + ".\n";
                    bContinue = false;
                }
                //Give 1 second for apply the permisions
                Thread.Sleep(1000);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;

                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
            }

            if (bContinue)
            {
                // Restore B files from appconfig (connecting liveoffice files)
                int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings["B" + rowCounter], "");
                    showStatus += count + ".Start Restoring File " + System.IO.Path.GetFileName(full) + " in " + full + ". \n";
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                    //Restore file process
                    showStatus = sq.RestoreDataBase_CopyBFiles(sourcePath, backupPath, count, full);
                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                }
            }

            if (bContinue)
            {
                // Restore C folders from appconfig (folders to Restore)
                int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string targetPath = System.IO.Path.Combine(ConfigurationManager.AppSettings["C" + rowCounter], "");
                    showStatus += count + ".Start Restoring Folder " + targetPath + ". \n";
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                    //Restore folders process
                    showStatus = sq.RestoreDataBase_CopyCFolder(sourcePath, backupPath, count, targetPath);
                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                }
            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                showStatus += count + ". Starting Restore Database. \n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                //Restore database process
                showStatus = sq.RestoreDataBase_CopyDataBase(sourcePath, backupPath, count);
                //Calculate time taked doing database backup
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nBackUp Failed See Below Errors.\n";
                    showStatus += "\nBackUp Failed See Below Errors.\n";
                    countTimer -= 1;
                    bContinue = false;
                }
                else
                {
                    status += "\nBackUp Completed Successfully. \n";
                    showStatus += "\nBackUp Completed Successfully. \n";
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
            }

            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"Restore\");
            //Report Last Progress
            //bwBackgroundWorkerRestore.ReportProgress(101);

            //if (bwBackgroundWorkerRestore.CancellationPending)
            //{
            //    e.Cancel = true;
            //    return;
            //}

        }



        #region backgroundRestore
        private void bwBackgroundWorkerRestore_DoWork(object sender, DoWorkEventArgs e)
        {
            #region bwBackgroundWorkerRestore Start
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;

            showStatus += sq.ScriptsDataBase();
            countTimer += 1;

            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            string sourcePath = ConfigurationManager.AppSettings["ASAIFOLDER"];
            string backupPath = bp;
            status = "Restore Kiosk Started\n\nBackUpFolder Before Installation: " + backupPath + "\n\n";
            #endregion

            #region bwBackgroundWorkerRestore PermissionAsaiFolder
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                //Try to give permision to Backup Folder
                try
                {
                    //If backup folder don't exist show the error
                    if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["ASAIFOLDER"]))
                    {
                        showStatus = "0.Restore Error couldn't find " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". \n";
                        bContinue = false;
                    }
                    else
                    {
                        //Give permision to Asai Folder Backup
                        string path = (ConfigurationManager.AppSettings["ASAIFOLDER"]);
                        DirectorySecurity ds = Directory.GetAccessControl(path);
                        FileSystemAccessRule fs = new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow);
                        ds.AddAccessRule(new FileSystemAccessRule(ConfigurationManager.AppSettings["SQLAccount"], FileSystemRights.FullControl, AccessControlType.Allow));
                        Directory.SetAccessControl(path, ds);
                        showStatus = "0. Give permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + " Successful.\n";
                    }
                }
                catch (Exception ex)
                {
                    showStatus = "0.Error giving permission to Folder " + ConfigurationManager.AppSettings["ASAIFOLDER"] + ". Error: " + ex.Message + ".\n";
                    bContinue = false;
                }
                //Give 1 second for apply the permisions
                Thread.Sleep(1000);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;

                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerRestore B Files
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                // Restore B files from appconfig (connecting liveoffice files)
                int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["BCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings["B" + rowCounter], "");
                    showStatus += count + ".Start Restoring File " + System.IO.Path.GetFileName(full) + " in " + full + ". \n";
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                    //Restore file process
                    showStatus = sq.RestoreDataBase_CopyBFiles(sourcePath, backupPath, count, full);
                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                }
            }
            #endregion

            #region bwBackgroundWorkerRestore C Folders
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                // Restore C folders from appconfig (folders to Restore)
                int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string targetPath = System.IO.Path.Combine(ConfigurationManager.AppSettings["C" + rowCounter], "");
                    showStatus += count + ".Start Restoring Folder " + targetPath + ". \n";
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                    //Restore folders process
                    showStatus = sq.RestoreDataBase_CopyCFolder(sourcePath, backupPath, count, targetPath);
                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nBackUp Failed See Below Errors.\n";
                        showStatus += "\nBackUp Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                }
            }
            #endregion

            #region bwBackgroundWorkerRestore Database
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                showStatus += count + ". Starting Restore Database. \n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
                //Restore database process
                showStatus = sq.RestoreDataBase_CopyDataBase(sourcePath, backupPath, count);
                //Calculate time taked doing database backup
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nBackUp Failed See Below Errors.\n";
                    showStatus += "\nBackUp Failed See Below Errors.\n";
                    countTimer -= 1;
                    bContinue = false;
                }
                else
                {
                    status += "\nBackUp Completed Successfully. \n";
                    showStatus += "\nBackUp Completed Successfully. \n";
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerRestore EndProcess
            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerRestore.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"Restore\");
            //Report Last Progress
            bwBackgroundWorkerRestore.ReportProgress(101);

            if (bwBackgroundWorkerRestore.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            #endregion
        }

        private void bwBackgroundWorkerRestore_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //When progresspercentage is 101 it means the work finished
            //if (e.ProgressPercentage == 101)
            //{
            //    MessageBox.Show(resultStatus, "Restore Result");
            //    //Close the application
            //    //Application.Exit();
            //}
            //else
            //{
            //    //pbProgressBar.Value = e.ProgressPercentage; //Update progress bar
            //    //if (pbProgressBar.Value == 100)
            //    //{
            //    //    //Backup process finished. Wait for 101 to show the report to the user
            //    //    lblProgressBar.Text = "Completed " + e.ProgressPercentage + "%\n" + showStatus;
            //    //}
            //    //else
            //    //{
            //    //    //Report the progress.
            //    //    lblProgressBar.Text = "Restoring........ " + e.ProgressPercentage + "%\n" + showStatus;
            //    //}
            //}
        }

        private void bwBackgroundWorkerRestore_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("The task has been cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error. Details: " + (e.Error as Exception).ToString());
            }
            else
            {
                MessageBox.Show(showStatus, "Restore Result");
                Application.Exit();
            }

        }

        #endregion


    }
}
