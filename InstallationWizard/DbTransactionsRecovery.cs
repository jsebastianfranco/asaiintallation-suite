﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InstallationWizardClass;
using System.Threading;
using System.IO;

namespace InstallationWizard
{
    public partial class DbTransactionsRecovery : UserControl
    {
        string strfilename;
        
        System.Data.SqlClient.SqlConnection cn;
        DataSet ds = new DataSet();

        private static DbTransactionsRecovery _instance;
        public static DbTransactionsRecovery Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DbTransactionsRecovery();
                return _instance;
            }
        }

        public DbTransactionsRecovery()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

            //Todo Store Procedure
            wLogging.log("Creating the Store procedure to insert selected values in to transactions table");
            try //to create the store procedure
            {
                Conn();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[TransactionRC]'))";
                cmd.CommandText = "DROP PROCEDURE [TransactionRC]";
                cmd.CommandText = "Go";
                cmd.CommandText = "CREATE procedure[dbo].[TransactionRC] @Header nchar(100),@Content nchar(4000),@Attempts nchar(2) as declare @t nchar(4000) set @t = REPLACE(@Content, ' ', '') INSERT INTO dbo.MFKLocalLog(Header, Content, Attempts) VALUES(@Header, RTRIM(LTRIM(@t)), @Attempts)";

                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                wLogging.log("Error creating the Store procedure " + err.Message);
            }

            wLogging.log("Creating the Temp Store procedure to search transactions in the Temp table");
            try //to create the temp store procedure
            {
                Conn();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[TransactionRCTemp]'))";
                cmd.CommandText = "DROP PROCEDURE [TransactionRCTemp]";
                cmd.CommandText = "Go";
                cmd.CommandText = "CREATE procedure[dbo].[TransactionRCTemp] @Header nchar(100),@Content nchar(4000), @LocalTime [datetime],@Attempts nchar(2) as declare @t nchar(4000) set @t = REPLACE(@Content, ' ', '') INSERT INTO dbo.MFKLocalLogTemp(Header, Content, LocalTime, Attempts) VALUES(@Header, RTRIM(LTRIM(@t)), @LocalTime, @Attempts)";

                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                wLogging.log("Error creating the Tem Store procedure " + err.Message);
            }

            //Starts reading the MFK_Backip.log file
            ds.ReadXml(strfilename);

            wLogging.log("Showing the modified MFK_Backup file into the Grid ");
            try
            {
                this.comboBox1.DataSource = ds.Tables[0];
                this.comboBox1.DisplayMember = "timestamp";
                this.comboBox1.ValueMember = "timestamp";

                DataSet dsu = new DataSet();
                dsu.ReadXml(strfilename);

                this.comboBox2.DataSource = dsu.Tables[0];
                this.comboBox2.DisplayMember = "timestamp";
                this.comboBox2.ValueMember = "timestamp";


                dataGridView1.DataSource = ds.Tables[0];
                string t = ds.Tables[0].Rows[0].ItemArray[2].ToString().Substring(14, 1);    //.Replace(@"\", string.Empty);
                t.Replace(@"\\", string.Empty);
                string j = t;
            }
            catch (Exception errRead)
            {
                wLogging.log("Error showing the modified MFK_Backup file into the Grid " + errRead.Message);
                MessageBox.Show("The file couldnt be read" + errRead.Message);
            }

            //Todo Create Temp Table
            wLogging.log("Creating the Temp table to filter transactions by time range ");
            try //to create the tmp table
            {
                Conn();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MFKLocalLogTemp') ";
                cmd.CommandText = "DROP TABLE [dbo].[MFKLocalLogTemp]";
                cmd.CommandText = "Go";
                cmd.CommandText = "CREATE TABLE [dbo].[MFKLocalLogTemp]([MFKLocalLogID][bigint] IDENTITY(1, 1) NOT NULL, [Header] [nvarchar] (100) NOT NULL,[Content] [nvarchar] (4000) NULL,[LocalTime] [datetime] NOT NULL ,[SentToServerTime] [datetime] NULL,[Attempts] [int] NULL DEFAULT((0)),[Error] [nvarchar] (2000) NULL, CONSTRAINT[PK_MFKLocalLogTemp] PRIMARY KEY CLUSTERED([MFKLocalLogID] ASC )WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]) ON[PRIMARY]";

                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                wLogging.log("Error creating the Temp table to filter transactions " + err.Message);
            }


            //Todo Insert
            wLogging.log("Start to inserting transactions displayed on the grid in to Temp table ");
            cmd2();
            wLogging.log("Temp table successfully filled");

            desc();
            MessageBox.Show("File successfully read","Reading Process");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Todo Buscar
            wLogging.log("Filtering the selected information into Temp table");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            try
            {
                Conn();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM MFKLocalLogTemp WHERE LocalTime BETWEEN '" + comboBox1.SelectedValue + "'" + "AND" + "'" + comboBox2.SelectedValue + "'";

                cmd.ExecuteNonQuery();

                System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader();
                System.Data.DataTable dt = new DataTable();
                dt.Load(dr);
                dataGridView1.DataSource = dt;


            }
            catch (Exception err)
            {
                wLogging.log("Error trying to Filter the selected Transactions into Temp table" + err.Message);
            }

            desc();
            wLogging.log("Transactions successfully filtered");
            MessageBox.Show("Transactions successfully filtered","Filtering Process");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cmd();
            wLogging.log("Insertion process ended successfully ");
            MessageBox.Show("Insertion process ended successfully ","Insertion Process");
        }

        private void Conn()
        {

            try
            {
                cn = new System.Data.SqlClient.SqlConnection();
                cn.ConnectionString = "server=localhost;initial catalog=MFKLocalLog;user id=sa;pwd=asai1234;";
                cn.Open();
            }
            catch (Exception errCon)
            {
                MessageBox.Show("Error trying to connect to database: " + errCon.Message);
            }

        }

        private void desc()
        {

            try
            {
                cn.Close();
                cn.Dispose();
            }
            catch (Exception errDisCon)
            {
                MessageBox.Show("Error trying to connect to database: " + errDisCon.Message);
            }

        }

        public void cmd()
        {
            wLogging.log("Starting the insertion into MFKLocalLog table of selected transactions");
            try
            {

                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

                //  cmd.CommandType = CommandType.Text;

                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    //string a="\"";
                    Conn();
                    cmd.Connection = cn;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "TransactionRC";
                    System.Data.SqlClient.SqlParameter par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@Header";
                    par.Value = dataGridView1.Rows[i].Cells[1].Value;
                    cmd.Parameters.Add(par);
                    par = null;
                    par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@Content";
                    par.Value = dataGridView1.Rows[i].Cells[2].Value.ToString().Replace("\n", string.Empty).Replace("\r", string.Empty).Replace(@"\", string.Empty);
                    cmd.Parameters.Add(par);
                    par = null;


                    par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@Attempts";
                    par.Value = 1;
                    cmd.Parameters.Add(par);
                    cmd.ExecuteReader();
                    cmd = null;
                    cmd = new System.Data.SqlClient.SqlCommand();


                }
                desc();


                //Todo Borrar Tabla Temporal
                wLogging.log("Deleting Temp table");
                try //borrar tabla temporal
                {
                    Conn();
                    cmd = null;
                    cmd = new System.Data.SqlClient.SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DROP TABLE [dbo].[MFKLocalLogTemp]";
                    cmd.ExecuteNonQuery();
                    desc();


                    Conn();
                    cmd = null;
                    cmd = new System.Data.SqlClient.SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;

                    cmd.CommandText = "DROP PROCEDURE [TransactionRCTemp]";
                    cmd.ExecuteNonQuery();
                    desc();
                }
                catch (Exception err3)
                {
                    wLogging.log("Error trying to Delete Temp table" + err3.Message);
                }

            }
            catch (Exception errCmd)
            {
                wLogging.log("Error inserting Transactions into MFKLocalLog table " + errCmd.Message);
            }


        }

        public void cmd2()
        {
            wLogging.log("Inserting transactions into temp table ");
            try
            {

                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {

                    Conn();
                    cmd.Connection = cn;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "TransactionRCTemp";
                    System.Data.SqlClient.SqlParameter par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@Header";
                    par.Value = dataGridView1.Rows[i].Cells[1].Value;
                    cmd.Parameters.Add(par);
                    par = null;
                    par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@Content";
                    par.Value = dataGridView1.Rows[i].Cells[2].Value.ToString().Replace("\n", string.Empty).Replace("\r", string.Empty).Replace(@"\", string.Empty);
                    cmd.Parameters.Add(par);
                    par = null;
                    par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@LocalTime";
                    par.Value = dataGridView1.Rows[i].Cells[0].Value;
                    cmd.Parameters.Add(par);
                    par = null;
                    par = new System.Data.SqlClient.SqlParameter();
                    par.ParameterName = "@Attempts";
                    par.Value = 1;
                    cmd.Parameters.Add(par);
                    cmd.ExecuteReader();
                    cmd = null;
                    cmd = new System.Data.SqlClient.SqlCommand();


                }
                desc();
                wLogging.log("Transactions correctly added into temp table ");


            }
            catch (Exception errCmd)
            {
                wLogging.log("Error Inserting transactions into temp table " + errCmd.Message);
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            wLogging.log("Executing replace function to fit the transaction Backup File");
            try
            {
                
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strfilename = openFileDialog1.FileName;
                }

                MessageBox.Show(strfilename,"File Path Confirmation");

                System.IO.File.Move(strfilename, @"C:\ASAI\readXml\MFK_Backup.txt");

                // System.IO.File.Move(@"C:\ASAI\readXml\MFK_Backup.log", @"C:\ASAI\readXml\MFK_Backup.txt"); //change

            }
            catch (Exception err)
            {
                wLogging.log("Error trying to execute replace" + err.Message);
            }

            Thread.Sleep(2000);

            wLogging.log("Trying to Fit the MFK:_Backup File ");
            try
            {
                //first we modify the .log file copied into the txt 

                string readF = File.ReadAllText(@"C:\ASAI\readXml\MFK_Backup.txt");

                readF = readF.Replace("<LogEntry>", "<a>");
                readF = readF.Replace("<!-- [ENTRY_END] -->", "");
                readF = readF.Replace("</LogEntry>", "</a>");
                File.WriteAllText(@"C:\ASAI\readXml\MFK_Backup.txt", readF);

                //then we re build the .log file again
                wLogging.log("Trying to write the node <LogEntry> ");
                try
                {

                    TextWriter tsw = new StreamWriter(strfilename/*@"C:\ASAI\readXml\MFK_Backup.log"*/, true);
                    tsw.WriteLine("<LogEntry>");
                    tsw.WriteLine(readF);
                    tsw.Close();
                }
                catch (Exception err1)
                {
                    wLogging.log("Error adding <LogEntry> into the file" + err1.Message);
                }


                File.AppendAllText(strfilename/*@"C:\ASAI\readXml\MFK_Backup.log"*/, "</LogEntry>"); //for write at the last line


            }
            catch (Exception err2)
            {
                wLogging.log("Error Trying to fit the MFK_Backup file" + err2.Message);
                
            }

            MessageBox.Show("The replace process has ended ","Replace Process");
        }

        private void DbTransactionsRecovery_Load(object sender, EventArgs e)
        {
            try
            {
                if (!File.Exists(@"C:\ASAI\readXml\a.txt"))
                {
                    File.CreateText(@"C:\ASAI\readXml\a.txt");
                }
            }
            catch (Exception)
            {

               
            }
            
        }
    }
}
