﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using InstallationWizardClass;

namespace InstallationWizard
{
    public partial class TestDBConn : UserControl
    {
        System.Data.SqlClient.SqlConnection cn;
        DataSet ds = new DataSet();

        private static TestDBConn _instance;
        public static TestDBConn Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new TestDBConn();
                return _instance;
            }
        }

        public TestDBConn()
        {
            InitializeComponent();
        }


        private void TestDBConn_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            Create cG = new Create();
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection();
            try
            {
                cn.ConnectionString = cG.sqlConnectionStringS;
                cn.Open();

                MessageBox.Show("Connection Successfull", "DataBase Connection", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        private void Conn()
        {

            try
            {
                cn = new System.Data.SqlClient.SqlConnection();
                cn.ConnectionString = "server=localhost;initial catalog=MFKLocalLog;user id=sa;pwd=asai1234;";
                cn.Open();
            }
            catch (Exception errCon)
            {
                MessageBox.Show("Error trying to connect to database: " + errCon.Message);
            }

        }

        private void desc()
        {

            try
            {
                cn.Close();
                cn.Dispose();
            }
            catch (Exception errDisCon)
            {
                MessageBox.Show("Error trying to connect to database: " + errDisCon.Message);
            }

        }
    }
}
