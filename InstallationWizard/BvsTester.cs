﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstallationWizard
{
    public partial class BvsTester : UserControl
    {
        private static BvsTester _instance;
        public static BvsTester Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BvsTester();
                return _instance;
            }
        }

        public BvsTester()
        {
            InitializeComponent();
        }


        private void BvsTester_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\ASAI\jcmPrinter\WfTclPrinter.exe");
        }
    }
}
