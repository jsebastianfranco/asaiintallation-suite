USE [master]
GO

/****** Object:  StoredProcedure [dbo].[Usp_RestoreDB]    Script Date: 04/23/2014 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_RestoreDB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Usp_RestoreDB]
GO

USE [master]
GO

/****** Object:  StoredProcedure [dbo].[Usp_RestoreDB]    Script Date: 04/23/2014 10:56:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[Usp_RestoreDB]  
(
    @pDBName NVARCHAR(200),/* Give Database name here  eg :MFKLocalLog */
	@pBackUpLocation NVARCHAR(400)/*Give the location name here eg : N'C:\ASAIBACKUP\'  */
) 

AS


/*

   Created By      : Altaf Hussain - Gonzalo Mu�oz
   Created Date    : 04/21/2014
   Description     : Create a Backup to Restore Images


   EXEC [dbo].[Usp_Restore]  @pDBName = 'MFKLocalLog', @pBackUpLocation = N'C:\ASAIBACKUP\'

*/

BEGIN

ALTER DATABASE [MFKLocalLog]
SET SINGLE_USER
--This rolls back all uncommitted transactions in the db.
WITH ROLLBACK IMMEDIATE

    DECLARE @restorepath NVARCHAR(400) = @pBackUpLocation + @pDBName +'.bak'

	RESTORE DATABASE [MFKLocalLog] FROM  
	DISK = @restorepath 
	WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 10
	
	Select 'Restore MFKLocalLog DataBase Completed'

ALTER DATABASE [MFKLocalLog] SET MULTI_USER

END                 




GO


