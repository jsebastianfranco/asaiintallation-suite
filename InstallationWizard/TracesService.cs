﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace InstallationWizard
{
    public partial class TracesService : UserControl
    {
        private static TracesService _instance;
        public static TracesService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new TracesService();
                return _instance;
            }
        }

        public TracesService()
        {
            InitializeComponent();
        }


        private void TracesService_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            if (File.Exists(@"C:\ASAI\PullingTraces\PullingTraces.bat"))
            {
                System.Diagnostics.Process.Start(@"C:\ASAI\InstallationWizard\Traces Service\MFKASAI Multifunction Manager.exe");
            }
            else
            {

                if (MessageBox.Show("Traces service is not installed. Do you wanted to install it ?", "Traces Service", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(@"C:\ASAI\InstallationWizard\Traces Service\TracesServiceInstaller\setup.exe");
                }
            }
        }
    }
}
