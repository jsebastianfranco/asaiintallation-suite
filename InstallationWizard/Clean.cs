﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using InstallationWizardClass;
using System.IO;
using System.Threading;

namespace InstallationWizard
{
    public partial class Clean : UserControl
    {
        private static Clean _instance;
        public int timer;
        public string showStatus, resultStatus, bp, kioskstatus, starting;

        

        public static Clean Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Clean();
                return _instance;
            }
        }

        private void Clean_Load(object sender, EventArgs e)
        {
            circularProgressBar1.Value = 0;
            circularProgressBar1.Minimum = 0;
            circularProgressBar1.Maximum = 100;
        }

        public Clean()
        {
            InitializeComponent();
            string files = "";
            timer = 1;
            //Show the files and folders to process
            //files = "Folder to take the Restore: " + ConfigurationManager.AppSettings["ASAIFOLDER"] + "\n\n";
            files = files + "Files to Clean:\n";
            int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CleanCount"]);
            for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
            {
                string app = "Clean" + rowCounter;
                files = files + timer + "." + System.IO.Path.Combine(ConfigurationManager.AppSettings[app], "") + "\n";
                timer += 1;
            }

            files = files + "\n" + timer + ".Clean Database.";
            timer += 1;
            lblClean.Text = files;
        }


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            circularProgressBar1.Visible = false;

            DialogResult dialogResult = MessageBox.Show("Are you sure?", "CONFIRMATION", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                var sq = new Create();
                string folder = ConfigurationManager.AppSettings["ASAIFOLDER"];
                lblProgressBar.Text = "Restore Kiosk Started\n\n0. Start giving permision to " + folder;
                bp = System.IO.Path.Combine(folder, "BackUp" + (DateTime.Now.ToString("yyyyMMddHHmmss")));
                string backup = System.IO.Path.Combine(bp, "MfkLocalLog.bak");



                // bwBackgroundWorkerClean.RunWorkerAsync();

                circularProgressBar1.Visible = true;

                for (int i = 1; i <= 36; i++)
                {
                    Thread.Sleep(20);
                    circularProgressBar1.Value = i;
                    circularProgressBar1.Update();
                    circularProgressBar1.Text = i.ToString() + "%";
                }

                Cln();

                for (int i = 36; i <= 95; i++)
                {
                    Thread.Sleep(20);
                    circularProgressBar1.Value = i;
                    circularProgressBar1.Update();
                    circularProgressBar1.Text = i.ToString() + "%";
                }
                Thread.Sleep(1000);

                for (int i = 95; i <= 100; i++)
                {
                    Thread.Sleep(20);
                    circularProgressBar1.Value = i;
                    circularProgressBar1.Update();
                    circularProgressBar1.Text = i.ToString() + "%";
                }



                if (circularProgressBar1.Value == 100)
                {
                    lblProgressBar.Text = "Completed";
                }

                

            }
            else if (dialogResult == DialogResult.No)
            {

            }


        }

        protected void Cln()
        {
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;

            showStatus += sq.ScriptsDataBase();
            countTimer += 1;

            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            string sourcePath = ConfigurationManager.AppSettings["ASAIFOLDER"];
            string backupPath = bp;
            status = "Clean Kiosk Started\n\nBackUpFolder Before Installation: " + backupPath + "\n\n";

            if (bContinue)
            {
                //Clean Floders
                // Restore Clean folders from appconfig
                int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CleanCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings["Clean" + rowCounter], "");
                    showStatus += count + ".Start Cleaning folder " + System.IO.Path.GetFileName(full) + " in " + full + ". \n";
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
                    //MessageBox.Show("Si limpio");
                    //Restore file process
                    showStatus = "";
                    if (System.IO.Directory.Exists(full))
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(full);

                        if (full.Contains(@"C:\Agilis\logs"))
                        {
                            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                            {
                                if (file.FullName.Contains("udbd"))
                                {
                                    try
                                    {
                                        file.Delete();
                                        showStatus += count + ". Delete File " + file.FullName + "\n";
                                    }
                                    catch (Exception ex)
                                    {
                                        showStatus += "Failed Deleting " + file.FullName + ". \n" + ex.Message + "\n";
                                    }
                                }

                            }
                        }
                        else
                        {

                            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                            {

                                try
                                {
                                    file.Delete();
                                    showStatus += count + ". Delete File " + file.FullName + "\n";
                                }
                                catch (Exception ex)
                                {
                                    showStatus += "Failed Deleting " + file.FullName + ". \n" + ex.Message + "\n";
                                }
                            }

                            foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
                            {
                                try
                                {
                                    dir.Delete(true);
                                    showStatus += count + ". Delete Folder " + dir.FullName + "\n";
                                }
                                catch (Exception ex)
                                {
                                    showStatus += "Failed Deleting " + dir.FullName + ". \n" + ex.Message + "\n";
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(full);
                    }

                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nClean Failed See Below Errors.\n";
                        showStatus += "\nClean Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    //bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
                }

            }

            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                string fileScript = ConfigurationManager.AppSettings["A3"];
                showStatus += count + ".Start Creating Database in " + fileScript + ".\n";
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_CreationSQLScripts(fileScript, count);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nClean Installation Failed. See Below Errors.\n";
                    showStatus += "\nClean Installation Failed. See Below Errors.\n";
                    countTimer -= 1;
                    bContinue = false;
                }
                else
                {
                    status += "\nClean Completed Successfully. \n";
                    showStatus += "\nClean Completed Successfully. \n";
                }
                //Show progress
                //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                //bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
            }

            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            //i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            //bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"Clean\");
            //Report Last Progress
            //bwBackgroundWorkerClean.ReportProgress(101);

            //if (bwBackgroundWorkerClean.CancellationPending)
            //{
            //    //e.Cancel = true;
            //    return;
            //}


        }


        private void bwBackgroundWorkerClean_DoWork(object sender, DoWorkEventArgs e)
        {
            #region bwBackgroundWorkerClean Start
            int countTimer = 0, count = 0;
            var sq = new Create();
            string status = "";
            bool bContinue = true;
            decimal i;
            DateTime dtStart, dtFinish;
            DateTime dtStartTotal, dtFinishTotal;
            TimeSpan tsDuration;
            double dSecondTotal;

            showStatus += sq.ScriptsDataBase();
            countTimer += 1;

            dtStartTotal = DateTime.Now;
            dtStart = DateTime.Now;
            string sourcePath = ConfigurationManager.AppSettings["ASAIFOLDER"];
            string backupPath = bp;
            status = "Clean Kiosk Started\n\nBackUpFolder Before Installation: " + backupPath + "\n\n";
            #endregion

            #region bwBackgroundWorkerClean Clean Folders
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                //Clean Floders
                // Restore Clean folders from appconfig
                int copyCount = Convert.ToInt32(ConfigurationManager.AppSettings["CleanCount"]);
                for (int rowCounter = 1; rowCounter <= copyCount; rowCounter++)
                {
                    count += 1;
                    dtStart = DateTime.Now;
                    string full = System.IO.Path.Combine(ConfigurationManager.AppSettings["Clean" + rowCounter], "");
                    showStatus += count + ".Start Cleaning folder " + System.IO.Path.GetFileName(full) + " in " + full + ". \n";
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
                    MessageBox.Show("Si limpio");
                    //Restore file process
                    showStatus = "";
                    if (System.IO.Directory.Exists(full))
                    {
                        System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(full);

                        if (full.Contains(@"C:\Agilis\logs"))
                        {
                            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                            {
                                if (file.FullName.Contains("udbd"))
                                {
                                    try
                                    {
                                        file.Delete();
                                        showStatus += count + ". Delete File " + file.FullName + "\n";
                                    }
                                    catch (Exception ex)
                                    {
                                        showStatus += "Failed Deleting " + file.FullName + ". \n" + ex.Message + "\n";
                                    }
                                }

                            }
                        }
                        else
                        {

                            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                            {

                                try
                                {
                                    file.Delete();
                                    showStatus += count + ". Delete File " + file.FullName + "\n";
                                }
                                catch (Exception ex)
                                {
                                    showStatus += "Failed Deleting " + file.FullName + ". \n" + ex.Message + "\n";
                                }
                            }

                            foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
                            {
                                try
                                {
                                    dir.Delete(true);
                                    showStatus += count + ". Delete Folder " + dir.FullName + "\n";
                                }
                                catch (Exception ex)
                                {
                                    showStatus += "Failed Deleting " + dir.FullName + ". \n" + ex.Message + "\n";
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(full);
                    }

                    //Calculate time taked doing this progress
                    dtFinish = DateTime.Now;
                    tsDuration = dtFinish - dtStart;
                    dSecondTotal = tsDuration.TotalSeconds;
                    status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                    countTimer += 1;
                    // Check for error and break the process
                    if (showStatus.Contains("Error"))
                    {
                        status += "\nClean Failed See Below Errors.\n";
                        showStatus += "\nClean Failed See Below Errors.\n";
                        rowCounter = copyCount + 1;
                        bContinue = false;
                        countTimer -= 1;
                    }
                    //Show progress
                    i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                    bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
                }

            }
            #endregion

            #region bwBackgroundWorkerClean Database
            // Check for errors if there is a problem cancel all process
            if (bContinue)
            {
                count += 1;
                dtStart = DateTime.Now;
                string fileScript = ConfigurationManager.AppSettings["A3"];
                showStatus += count + ".Start Creating Database in " + fileScript + ".\n";
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
                //Create process
                showStatus = sq.NewDataBase_CreationSQLScripts(fileScript, count);
                //Calculate time taked doing this progress
                dtFinish = DateTime.Now;
                tsDuration = dtFinish - dtStart;
                dSecondTotal = tsDuration.TotalSeconds;
                status += showStatus + "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                showStatus += "Time Used = " + dSecondTotal.ToString() + " seconds.\n";
                countTimer += 1;
                // Check for error and break the process
                if (showStatus.Contains("Error"))
                {
                    status += "\nClean Installation Failed. See Below Errors.\n";
                    showStatus += "\nClean Installation Failed. See Below Errors.\n";
                    countTimer -= 1;
                    bContinue = false;
                }
                else
                {
                    status += "\nClean Completed Successfully. \n";
                    showStatus += "\nClean Completed Successfully. \n";
                }
                //Show progress
                i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
                bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
            }
            #endregion

            #region bwBackgroundWorkerClean EndProcess
            //Calculate time doing all process
            dtFinishTotal = DateTime.Now;
            tsDuration = dtFinishTotal - dtStartTotal;
            dSecondTotal = tsDuration.TotalSeconds;
            status += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            showStatus += "Total Time Used = " + dSecondTotal.ToString() + " Seconds.\n";
            //Report progress
            i = Convert.ToDecimal(countTimer) / Convert.ToDecimal(timer) * 100;
            bwBackgroundWorkerClean.ReportProgress(Convert.ToInt32(i));
            //Create a file with the complete process in the machine
            resultStatus = sq.ResultFile(status, @"Clean\");
            //Report Last Progress
            bwBackgroundWorkerClean.ReportProgress(101);

            if (bwBackgroundWorkerClean.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            #endregion
        }
    }
}
