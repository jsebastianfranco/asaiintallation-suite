﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;

namespace InstallationWizardClass
{
    public class RecoveryDb
    {
        
        public void cmd()
        {
            string ResultsPath = @"C:\ASAI\InstallationWizard\RecoveryDbResults\";

            wLogging.log("Executing method to send a DBCC Comand");
            try
            {
                if (!Directory.Exists(ResultsPath))
                {
                    Directory.CreateDirectory(ResultsPath);
                }
            }
            catch (Exception errCmd)
            {
                wLogging.log("Error trying to create Results folder" + errCmd.Message);
            }


            try
            {
                wLogging.log("Executing method to send a DBCC Comand: Create the Ps1 file");

                var ps1File = @"C:\ASAI\InstallationWizard\RecoveryDbResults\command.bat";
                try
                {
                    if (!File.Exists(ps1File))
                    {
                        FileStream stream = null;
                        stream = new FileStream(ps1File, FileMode.OpenOrCreate);
                        stream.Close();
                        //File.Create(ps1File);
                        //File.CreateText(ps1File);
                    }
                                       
                }
                catch (Exception errPs1)
                {
                    wLogging.log("Error trying to create Ps1 " + errPs1.Message);
                }

                System.Threading.Thread.Sleep(2000);

                wLogging.log("Executing method to send a DBCC Comand: writing file");
                string c = "\"";
                string message = @"sqlcmd -S localhost -U sa -P asai1234 -o C:\asai\InstallationWizard\RecoveryDbResults\Results.Txt -Q " + c + "DBCC CHECKDB('MFKLocalLog')" + c;

                try
                {
                    
                    var fs = File.Open(ps1File, FileMode.Append, FileAccess.Write, FileShare.Read);
                    System.IO.StreamWriter sw = new StreamWriter(fs);
                    sw.AutoFlush = true;
                    sw.WriteLine(message);
                    sw.Close();
                    //File.WriteAllText(ps1File, message);
                }
                catch (Exception errWCmd)
                {
                    wLogging.log("Error trying to create Ps1 " + errWCmd.Message);
                }


                wLogging.log("Executing method to send a DBCC Comand: running file");
                try
                {
                    //PowerShell ps = PowerShell.Create();
                    //ps.AddScript(@"C:\ASAI\InstallationWizard\RestoreDbResults\command.ps1");
                    //ps.Invoke();
                    //ps.InvocationStateChanged += Ps_InvocationStateChanged;

                    Process proc = new Process();
                    proc.Exited += new EventHandler(Fin);
                    
                    proc.StartInfo.UseShellExecute = true;
                    
                  //  proc.StartInfo.Arguments = @"-NoProfile -ExecutionPolicy unrestricted C:\ASAI\InstallationWizard\RestoreDbResults\command.ps1";
                    proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    proc.StartInfo.FileName = @"C:\ASAI\InstallationWizard\RecoveryDbResults\command.bat";

                    proc.EnableRaisingEvents = true;
                    proc.Start();

                }
                catch (Exception errRCmd)
                {
                    wLogging.log("Error trying to run Ps1 " + errRCmd.Message);
                }

            }
            catch (Exception errDbccCmd)
            {
                wLogging.log("Error trying to execute the method - send a DBCC Comand: " + errDbccCmd.Message);
            }
            
        }

      
        private void Fin(object sender, EventArgs e)
        {
            wLogging.log("Trying to delete the Script:");
            try
            {
                File.Delete(@"C:\ASAI\InstallationWizard\RecoveryDbResults\command.bat");
            }
            catch (Exception errDelCmd)
            {
                wLogging.log("Error to delete the Script: " + errDelCmd);
            }
        }
    }
}
