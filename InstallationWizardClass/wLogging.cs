﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using System.Runtime.CompilerServices;

namespace InstallationWizardClass
{
    public static class wLogging
    {
        #region Log
        private static void validatesize()
        {
            long length = 0;
            try
            {
                if (File.Exists(@"C:\ASAI\InstallationWizard\RestoreDbLogs\" + "RestoreDbLog.txt"))
                {
                    length = new System.IO.FileInfo(@"C:\ASAI\InstallationWizard\RestoreDbLogs\" + "RestoreDbLog.txt").Length;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {

                log(ex.Message);
                return;
            }
            string s = BytesToString(length);
            if (s.Contains("MB"))
            {
                float t = float.Parse(s.Replace("MB", "").Replace("B", "").Replace("KB", ""));

                if (t > 5.0)
                {
                    File.Move(@"C:\ASAI\InstallationWizard\RestoreDbLogs\" + "RestoreDbLog.txt", @"C:\ASAI\InstallationWizard\RestoreDbLogs\" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "RestoreDbLog.txt");
                }
            }
        }

        private static String BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }

        public static void log(string msg, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0)
        {
            string logsPath = @"C:\ASAI\InstallationWizard\RecoveryDbLogs\";
            string logsFile = @"C:\ASAI\InstallationWizard\RecoveryDbLogs\RecoveryDbLogs.txt";
            try
            {
                if (!Directory.Exists(logsPath))
                {
                    Directory.CreateDirectory(logsPath);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            try
            {
                if (!File.Exists(logsFile))
                {
                    File.Create(logsFile);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            try
            {
                int lineCount = File.ReadLines(@"C:\ASAI\InstallationWizard\RecoveryDbLogs\" + "RecoveryDbLogs.txt").Count();
                string fl = string.Empty;

                var filelog = @"C:\ASAI\InstallationWizard\RecoveryDbLogs\" + "RecoveryDbLogs.txt";
                var fs = File.Open(filelog, FileMode.Append, FileAccess.Write, FileShare.Read);

                System.IO.StreamWriter sw = new StreamWriter(fs); //System.IO.File.AppendText(fs);
                sw.AutoFlush = true;

                if (fl != "n")
                {

                    sw.WriteLine(fl);
                    sw.WriteLine(DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year + "  " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second + "." + (DateTime.Now.Millisecond / 10) + " | " + " INFO " + " | " + msg + " | " + " Class: " + Path.GetFileName(file) + " | " + " Line: " + line);
                    sw.Close();
                }
                else
                {

                    sw.WriteLine(DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year + "  " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second + "." + (DateTime.Now.Millisecond / 10) + " | " + " INFO " + " | " + msg + " | " + " Class: " + Path.GetFileName(file) + " | " + " Line: " + line);
                    sw.Close();
                }
            }
            catch (Exception)
            {
                return;
            }


        }

        #endregion
    }
}
