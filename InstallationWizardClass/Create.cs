﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Web;
using System.Configuration;
using System.Data.EntityClient;
using System.IO;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Ionic.Zip;
using InstallationWizardClass.EncryptDecrypt;
using System.Threading;
using System.Xml;
using System.Net;
using System.Xml.Linq;
using System.Reflection;

namespace InstallationWizardClass
{
    public class Create
    {
        
        MFKLocalLogEntities context;
        public string sqlConnectionStringS = GetConnectionString();

        public Create()
        {
            string providerName = "System.Data.SqlClient";
            string providerString = GetConnectionString();

            // Initialize the EntityUserConnectionStringBuilder.
            EntityConnectionStringBuilder entityTerminalBuilder = new EntityConnectionStringBuilder();
            //Set the provider name.
            entityTerminalBuilder.Provider = providerName;
            // Set the provider-specific connection string.
            entityTerminalBuilder.ProviderConnectionString = providerString;
            // Set the Metadata location.
            entityTerminalBuilder.Metadata = @"res://*/MfkLocalLogModel.csdl|
                            res://*/MfkLocalLogModel.ssdl|
                            res://*/MfkLocalLogModel.msl";

            context = new MFKLocalLogEntities(entityTerminalBuilder.ToString());
        }

        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();
            string authentication = CryptorEngine.Decrypt(ConfigurationManager.AppSettings["S1"], true);
            string serverName = CryptorEngine.Decrypt(ConfigurationManager.AppSettings["S2"], true);
            string databaseName = CryptorEngine.Decrypt(ConfigurationManager.AppSettings["S3"], true);
            string user = CryptorEngine.Decrypt(ConfigurationManager.AppSettings["S4"], true);
            string pwd = CryptorEngine.Decrypt(ConfigurationManager.AppSettings["S5"], true);
            string providerString = "";

            // Set the properties for the data source.
            sqlBuilder.DataSource = serverName;
            sqlBuilder.InitialCatalog = databaseName;
            sqlBuilder.UserID = user;
            sqlBuilder.Password = pwd;
            sqlBuilder.MultipleActiveResultSets = true;

            providerString = sqlBuilder.ToString();
            return providerString;
        }

        public string ScriptsDataBase()
        {
            string status = "";
            try
            {
                string sqlConnectionString = GetConnectionString();
                FileInfo file = new FileInfo(ConfigurationManager.AppSettings["A1"]);
                string script = file.OpenText().ReadToEnd();
                SqlConnection conn = new SqlConnection(sqlConnectionString);
                Server server = new Server(new ServerConnection(conn));
                server.ConnectionContext.ExecuteNonQuery(script);

                string sqlConnectionString2 = GetConnectionString();
                FileInfo file2 = new FileInfo(ConfigurationManager.AppSettings["A2"]);
                string script2 = file2.OpenText().ReadToEnd();
                SqlConnection conn2 = new SqlConnection(sqlConnectionString2);
                Server server2 = new Server(new ServerConnection(conn2));
                server2.ConnectionContext.ExecuteNonQuery(script2);
            }
            catch (Exception e)
            {
                status = "Error" + e.Message + " \n";
            }
            return status;
        }

        public string BackUpDataBase_CopyBFiles(string targetPath, int counter, string full, string destFile, string sourcePath)
        {
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            if (System.IO.Directory.Exists(sourcePath))
            {
                try
                {
                    System.IO.File.Copy(full, destFile, true);
                    return counter + ".Copied File " + System.IO.Path.GetFileName(destFile) + " in " + destFile + ". \n";
                }
                catch (Exception e)
                {
                    if (System.IO.Path.GetFileName(full).Contains("MFKSynchroModuleSvc.exe.config"))
                        return counter + ". " + full + " File Don't Exist continue others.\n";
                    else
                        return counter + ".Error copying File " + System.IO.Path.GetFileName(destFile) + ". " + e.Message + ". \n";
                }
            }
            else
            {
                    return counter + ".Error File " + full + " don't exist. \n";
            }
        }

        public string BackUpDataBase_CopyCFolders(string targetPath, int counter, string sourcePath)
        {
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }
            if (System.IO.Directory.Exists(sourcePath))
            {
                string name = System.IO.Path.GetFileName(sourcePath);
                string zipname = targetPath + @"\" + name + ".zip";
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(sourcePath);
                        zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                        zip.ParallelDeflateThreshold = -1;
                        zip.Save(zipname);
                        return counter + ".Copied folder " + sourcePath + " in " + zipname + ". \n";
                    }
                }
                catch (Exception e)
                {
                    return counter + ".Error copying Folder " + sourcePath + " in zip file " + name + ".zip. " + e.Message + ". \n";
                }
            }
            else
            {
                return counter + ".Error Folder " + sourcePath + " don't exist. \n";
            }
        }

        public string BackUpDataBase_CopyDataBase(string targetPath, int counter)
        {
            string status = ScriptsDataBase();

            try
            {
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }
                var eConnection = (System.Data.EntityClient.EntityConnection)context.Connection;

                DbDataReader dataReader;
                DbConnection conn = eConnection.StoreConnection;
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    DbParameter param = cmd.CreateParameter();
                    param.DbType = DbType.String;
                    param.ParameterName = "@pDBName";
                    param.Value = "MfkLocalLog";
                    cmd.Parameters.Add(param);

                    DbParameter param1 = cmd.CreateParameter();
                    param1.DbType = DbType.String;
                    param1.ParameterName = "@pBackUpLocation";
                    param1.Value = targetPath + @"\";
                    cmd.Parameters.Add(param1);

                    cmd.CommandText = "Usp_BackupDB";//change
                    dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                string value = "";
                while (dataReader.Read())
                {
                    value = dataReader[0].ToString();
                }

                conn.Close();
                context.Dispose();
                return counter + ". " + value + ". \n";
            }
            catch (Exception e)
            {
                return counter + ".Error generating DataBase BackUp. " + e.Message + ". \n";
            }
        }

        public string RestoreDataBase_CopyBFiles(string sourcePath, string backupPath, int counter, string full)
        {
            string status = "";
            string targetPath = System.IO.Path.GetDirectoryName(full);
            // Use static Path methods to extract only the file name from the path.
            string fileName = System.IO.Path.GetFileName(full);
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string backupFile = System.IO.Path.Combine(backupPath, fileName);

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            if (!System.IO.Directory.Exists(backupPath))
            {
                System.IO.Directory.CreateDirectory(backupPath);
            }

            if (System.IO.Directory.Exists(sourcePath))
            {
                try
                {
                    try
                    {
                        System.IO.File.Copy(full, backupFile, true);
                        status = counter + ".BackUp File " + System.IO.Path.GetFileName(backupFile) + " in " + backupFile + ". \n";
                    }
                    catch (Exception e)
                    {
                        status = counter + ".Failed backup File " + System.IO.Path.GetFileName(backupFile) + " in " + backupFile + ". " + e.Message + ".\n";
                    }
                    System.IO.File.Copy(sourceFile, full, true);
                    return status + counter + ".Restored File " + System.IO.Path.GetFileName(full) + " in " + full + ". \n";
                }
                catch (Exception e)
                {
                    if (System.IO.Path.GetFileName(full).Contains("MFKSynchroModuleSvc.exe.config"))
                        return status + counter + ". " + System.IO.Path.GetFileName(full) + " File Don't Exist continue others.\n";
                    else
                        return status + counter + ".Error restoring File " + System.IO.Path.GetFileName(full) + ". " + e.Message + ".\n";
                }
            }
            else
            {
                if(sourcePath.Contains("MFKSynchroModuleSvc"))
                    return status + counter + "File Don't Exist continue others.\n";
                else
                    return status + counter + ".Error couldn't find " + sourcePath + " folder.\n";
            }
        }

        public string RestoreDataBase_CopyCFolder(string sourcePath, string backupPath, int counter, string targetPath)
        {
            string status = "";

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }
            else
            {
                System.IO.Directory.Delete(targetPath, true);
                System.IO.Directory.CreateDirectory(targetPath);
            }

            if (!System.IO.Directory.Exists(backupPath))
            {
                System.IO.Directory.CreateDirectory(backupPath);
            }

            if (System.IO.Directory.Exists(sourcePath))
            {
                string name = System.IO.Path.GetFileName(targetPath);
                string zipname = sourcePath + @"\" + name + ".zip";
                string unpackDirectory = targetPath;
                try
                {
                    string backupZipName = backupPath + @"\" + name + ".zip";
                    try
                    {
                        using (ZipFile zip = new ZipFile())
                        {
                            zip.AddDirectory(unpackDirectory);
                            zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                            zip.ParallelDeflateThreshold = -1;
                            zip.Save(backupZipName);
                            status = counter + ".Backup folder " + targetPath + " in " + backupZipName + ". \n";
                        }
                    }
                    catch (Exception e)
                    {
                        status = counter + ".Failed backuping Folder " + targetPath + " in zip file " + backupZipName + ". " + e.Message + ".\n";
                    }

                    using (ZipFile zip = ZipFile.Read(zipname))
                    {
                        foreach (ZipEntry e in zip)
                        {
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                        }

                        return status + counter + ".Restored Folder " + targetPath + ". \n";
                    }

                }
                catch (Exception e)
                {
                    return status + counter + ".Error restoring Folder " + targetPath + ". " + e.Message + ".\n";
                }
            }
            else
            {
                return status + counter + ".Error zip file " + sourcePath + " don't exist.\n";
            }
        }

        public string RestoreDataBase_CopyDataBase(string sourcePath, string backupPath, int counter)
        {
            string status = "";
            try
            {
                //try
                //{
                //    if (!System.IO.Directory.Exists(backupPath))
                //    {
                //        System.IO.Directory.CreateDirectory(backupPath);
                //    }
                //    var eConnection = (System.Data.EntityClient.EntityConnection)context.Connection;

                //    DbDataReader dataReader;
                //    DbConnection conn = eConnection.StoreConnection;
                //    if (conn.State != ConnectionState.Open)
                //        conn.Open();

                //    using (DbCommand cmd = conn.CreateCommand())
                //    {
                //        cmd.CommandType = CommandType.StoredProcedure;
                //        DbParameter param = cmd.CreateParameter();
                //        param.DbType = DbType.String;
                //        param.ParameterName = "@pDBName";
                //        param.Value = "MfkLocalLog";
                //        cmd.Parameters.Add(param);

                //        DbParameter param1 = cmd.CreateParameter();
                //        param1.DbType = DbType.String;
                //        param1.ParameterName = "@pBackUpLocation";
                //        param1.Value = backupPath + @"\";
                //        cmd.Parameters.Add(param1);

                //        cmd.CommandText = "Usp_BackupDB";//change
                //        dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //    }
                //    string value = "";
                //    while (dataReader.Read())
                //    {
                //        value = dataReader[0].ToString();
                //    }

                //    status = counter + ". " + value + ".\n";
                //}
                //catch (Exception e)
                //{
                //    return counter + ".Error generating DataBase BackUp. " + e.Message + ".\n";
                //}
                if (!System.IO.Directory.Exists(sourcePath))
                {
                    return status + counter + ".Error Couldnt' find " + sourcePath + ".\n";
                }
                else
                {
                    var eConnection = (System.Data.EntityClient.EntityConnection)context.Connection;

                    DbDataReader dataReader;
                    DbConnection conn = eConnection.StoreConnection;
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (DbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        DbParameter param = cmd.CreateParameter();
                        param.DbType = DbType.String;
                        param.ParameterName = "@pDBName";
                        param.Value = "MfkLocalLog";
                        cmd.Parameters.Add(param);

                        DbParameter param1 = cmd.CreateParameter();
                        param1.DbType = DbType.String;
                        param1.ParameterName = "@pBackUpLocation";
                        param1.Value = sourcePath + @"\";
                        cmd.Parameters.Add(param1);

                        cmd.CommandText = "Usp_RestoreDB";
                        cmd.CommandTimeout = 0;
                        dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    }
                    string value = "";
                    while (dataReader.Read())
                    {
                        value = dataReader[0].ToString();
                    }

                    conn.Close();
                    context.Dispose();
                    return status + counter + ". " + value + ".\n";
                }
            }
            catch (Exception e)
            {
                return status + counter + ".Error Restoring BackUp. " + e.Message + ".\n";
            }
        }

        public string NewDataBase_BOSWebSvcSettings(string liveUrl, string path, string backupPath, int counter, string webProtocol)
        {
            string status = "";
            if (!System.IO.Directory.Exists(backupPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(backupPath);
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create backup folder " + backupPath + ". " + ex.Message + ".\n";
                }
            }


            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
                    status += counter + ".Created " + System.IO.Path.GetDirectoryName(path) + ".\n";
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create folder " + System.IO.Path.GetDirectoryName(path) + ". " + ex.Message + ".\n";
                }
            }

            string fileName = System.IO.Path.GetFileName(path);
            string backupFile = System.IO.Path.Combine(backupPath, fileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Copy(path, backupFile, true);
                    status += counter + ".BackUp " + backupFile + " .\n";
                }
                catch (Exception e)
                {
                    status += counter + ".Couldn't backup File " + backupFile + ". " + e.Message + ".\n";
                }
            }
            else
            {
                status += counter + ".Couldn't backup File " + backupFile + ". Folder don't exist. \n";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>");
            sb.AppendLine("\t\t<BOSWebSvcAPI.Properties.BOSWebSvcSettings>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "LogServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "KioskServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.KioskService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "DataServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/ASAIWebservices.DataService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t</BOSWebSvcAPI.Properties.BOSWebSvcSettings>");

            try
            {
                using (StreamWriter outfile = new StreamWriter(path))
                {
                    outfile.Write(sb.ToString());
                    return status + counter + ".Created File " + path + ".\n";
                }
            }
            catch (Exception e)
            {
                return status + counter + ".Error Creating file BOSWebSvcSettings.config. " + e.Message + ".\n";
            }
        }

        public string NewDataBase_KioskWebSvcSettings(string liveUrl, string path, string backupPath, int counter, string webProtocol)
        {
            string status = "";
            if (!System.IO.Directory.Exists(backupPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(backupPath);
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create backup folder " + backupPath + ". " + ex.Message + ".\n";
                }
            }


            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
                    status += counter + ".Created " + System.IO.Path.GetDirectoryName(path) + ".\n";
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create folder " + System.IO.Path.GetDirectoryName(path) + ". " + ex.Message + ".\n";
                }
            }

            string fileName = System.IO.Path.GetFileName(path);
            string backupFile = System.IO.Path.Combine(backupPath, fileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Copy(path, backupFile, true);
                    status += counter + ".BackUp " + backupFile + ". \n";
                }
                catch (Exception e)
                {
                    status += counter + ".Couldn't backup File " + backupFile + ". " + e.Message + ".\n";
                }
            }
            else
            {
                status += counter + ".Couldn't backup File " + backupFile + ". Folder don't exist. \n";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>");
            sb.AppendLine("\t\t<BOSWebSvcAPI.Properties.KioskWebSvcSettings>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "TcpServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>net.tcp://localhost/BOServer/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "WsServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "HttpServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t</BOSWebSvcAPI.Properties.KioskWebSvcSettings>");

            try
            {
                using (StreamWriter outfile = new StreamWriter(path))
                {
                    outfile.Write(sb.ToString());
                    return status + counter + ".Created File " + path + ".\n";
                }
            }
            catch (Exception e)
            {
                return status + counter + ".Error Creating file KioskWebSvcSettings.config. " + e.Message + ".\n";
            }
        }

        public string NewDataBase_KioskWinServiceProperties(string liveUrl, string path, string backupPath, int counter, string webProtocol)
        {
            string status = "";
            if (!System.IO.Directory.Exists(backupPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(backupPath);
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create backup folder " + backupPath + ". " + ex.Message + ".\n";
                }
            }


            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
                    status += counter + ".Created " + System.IO.Path.GetDirectoryName(path) + ".\n";
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create folder " + System.IO.Path.GetDirectoryName(path) + ". " + ex.Message + ".\n";
                }
            }

            string fileName = System.IO.Path.GetFileName(path);
            string backupFile = System.IO.Path.Combine(backupPath, fileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Copy(path, backupFile, true);
                    status += counter + ".BackUp " + backupFile + " .\n";
                }
                catch (Exception e)
                {
                    status += counter + ".Couldn't backup File " + backupFile + ". " + e.Message + ".\n";
                }
            }
            else
            {
                status += counter + ".Couldn't backup File " + backupFile + ". Folder don't exist. \n";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>");
            sb.AppendLine("\t<BOSWebSvcSettings>");
            sb.AppendLine("\t\t<BOSWebSvcAPI.Properties.BOSWebSvcSettings>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "LogServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "KioskServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.KioskService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t</BOSWebSvcAPI.Properties.BOSWebSvcSettings>");
            sb.AppendLine("\t\t<BOSWebSvcAPI.Properties.KioskWebSvcSettings>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "TcpServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>net.tcp://localhost:8191/BOServer/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "WsServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "HttpServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t</BOSWebSvcAPI.Properties.KioskWebSvcSettings>");
            sb.AppendLine("\t\t<BOSWebServicesAPI.Properties.BOSWsApiSettings>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "LogServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.LogService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "KioskServiceURL" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + webProtocol + "://" + liveUrl + "/BOSWebServices.KioskService.svc</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t</BOSWebServicesAPI.Properties.BOSWsApiSettings>");
            sb.AppendLine("\t\t</BOSWebSvcSettings>");

            try
            {
                using (StreamWriter outfile = new StreamWriter(path))
                {
                    outfile.Write(sb.ToString());
                    return status + counter + ".Created " + path + ".\n";
                }
            }
            catch (Exception e)
            {
                return status + counter + ".Error Creating file KioskWinService.Properties.config. " + e.Message + ".\n";
            }
        }

        public string NewDataBase_MFKSyncroSvcSettings(string kioskID, string serviceIntervalMinutes, string path, string backupPath, int counter, string webProtocol)
        {
            string status = "";
            if (!System.IO.Directory.Exists(backupPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(backupPath);
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create backup folder " + backupPath + ". " + ex.Message + ".\n";
                }
            }


            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
                    status += counter + ".Created " + System.IO.Path.GetDirectoryName(path) + ".\n";
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create folder " + System.IO.Path.GetDirectoryName(path) + ". " + ex.Message + ".\n";
                }
            }

            string fileName = System.IO.Path.GetFileName(path);
            string backupFile = System.IO.Path.Combine(backupPath, fileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Copy(path, backupFile, true);
                    status += counter + ".BackUp " + backupFile + ". \n";
                }
                catch (Exception e)
                {
                    status += counter + ".Couldn't backup File " + backupFile + ". " + e.Message + ".\n";
                }
            }
            else
            {
                status += counter + ".Couldn't backup File " + backupFile + ". Folder don't exist. \n";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>");
            sb.AppendLine("\t\t<KioskWinService.Properties.MFKSyncroSvc>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "KioskIdBOS" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + kioskID + "</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t\t<setting name=" + '"' + "ServiceIntervalMinutes" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t<value>" + serviceIntervalMinutes + "</value>");
            sb.AppendLine("\t\t\t</setting>");
            sb.AppendLine("\t\t</KioskWinService.Properties.MFKSyncroSvc>");

            try
            {
                using (StreamWriter outfile = new StreamWriter(path))
                {
                    outfile.Write(sb.ToString());
                    return status + counter + ".Created File " + path + ".\n";
                }
            }
            catch (Exception e)
            {
                return status + counter + ".Error Creating file MFKSyncroSvcSettings.config. " + e.Message + ".\n";
            }
        }

        public string NewDataBase_CreatingNFiles(string sourcePath, int counter, string full)
        {
            string targetPath = System.IO.Path.GetDirectoryName(full);

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            // Use static Path methods to extract only the file name from the path.
            string fileName = System.IO.Path.GetFileName(full);
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);

            if (System.IO.Directory.Exists(sourcePath))
            {
                try
                {
                    System.IO.File.Copy(sourceFile, full, true);
                    return counter + ".Created File " + full + ". \n";
                }
                catch (Exception e)
                {
                    return counter + ".Error creating File " + System.IO.Path.GetFileName(full) + ". " + e.Message + ".\n";
                }
            }
            else
            {
                return counter + ".Error couldn't find " + full + " File.\n";
            }

        }

        public string NewDataBase_CreationSQLScripts(string fileScript, int counter)
        {
            try
            {
                string sqlConnectionString = GetConnectionString();
                FileInfo file = new FileInfo(fileScript);
                string script = file.OpenText().ReadToEnd();
                SqlConnection conn = new SqlConnection(sqlConnectionString);
                Server server = new Server(new ServerConnection(conn));
                server.ConnectionContext.ExecuteNonQuery(script);
                return counter + ".Database Created Successfully.\n";
            }
            catch (Exception e)
            {
                return counter + ".Error Creating Database. " + e.Message + ".\n";
            }

        }

        //public string KioskInfo_FileInfo(XmlDataDocument xmldoc, string elementname)
             public string KioskInfo_FileInfo(XmlDocument xmldoc, string elementname)
        {
            try
            {
                XmlNodeList xmlnode;
                string str = null;
                int i = 0;
                xmlnode = xmldoc.GetElementsByTagName(elementname);
                str = xmlnode[i].ChildNodes.Item(i).InnerText.Trim();
                return str;

            }
            catch
            {
                return "Not Found.";
            }
        }

        public string KioskInfo_FileInfoConfig(XElement root, string elementname)
        {
            try
            {
                string str = null;
                IEnumerable<XElement> address =
                                        from el in root.Elements("setting")
                                        //where (string)el.Attribute("Type") == "Billing"
                                        select el;
                foreach (XElement el in address)
                {
                    if (el.FirstAttribute.Value.ToString() == elementname)
                        str = el.Value.ToString();
                }
                return str;
            }
            catch
            {
                return "Not Found.";
            }
        }

        //public string KioskInfo_BOServerParametersInfo(XmlDataDocument xmldoc, string elementname, int count)
        public string KioskInfo_BOServerParametersInfo(XmlDocument xmldoc, string elementname, int count)
        {
            try
            {
                XmlNodeList xmlnode;
                int i = 0;
                string str = null;

                xmlnode = xmldoc.GetElementsByTagName(elementname);
                string echeck = "Failed", echeck2 = "Failed", echeckweb = "Failed", echeckweb2 = "Failed";

                for (i = 0; i <= xmlnode.Count - 1; i++)
                {
                    string cnn = xmlnode[i].ChildNodes.Item(2).InnerText.Trim();
                    if (cnn.Contains("ECheckCertegyUrl"))
                    {
                        str = xmlnode[i].ChildNodes.Item(3).InnerText.Trim();
                        echeck = "Echeck Url " + str + "\nEcheck Url Connect " + KioskInfo_Echeck(str) + "\n";
                    }
                    if (cnn.Contains("ECheckCertegySecondUrl"))
                    {
                        str = xmlnode[i].ChildNodes.Item(3).InnerText.Trim();
                        echeck2 = "EcheckSecond Url " + str + "\nEcheckSecond Url Connect " + KioskInfo_Echeck(str) + "\n";
                    }
                    if (cnn.Contains("ECheckCertegyWebServiceUrl"))
                    {
                        str = xmlnode[i].ChildNodes.Item(3).InnerText.Trim();
                        echeckweb = "EcheckWeb Url " + str + "\nEcheckWeb Url Connect " + KioskInfo_Echeck(str) + "\n";
                    }
                    if (cnn.Contains("ECheckCertegyWebServiceSecondUrl"))
                    {
                        str = xmlnode[i].ChildNodes.Item(3).InnerText.Trim();
                        echeckweb2 = "EcheckWebSecond Url " + str + "\nEcheckWebSecond Url Connect " + KioskInfo_Echeck(str) + "\n";
                    }
                }
                if (echeckweb.Contains("Failed") && echeckweb2.Contains("Failed"))
                {
                    return echeck + echeck2 + "\n";
                }
                else
                {
                    return echeckweb + echeckweb2 + "\n";
                }
            }
            catch
            {
                return "Not Found.\n";
            }
        }

        public string KioskInfo_ping(string url)
        {
            try
            {
                var ping = new System.Net.NetworkInformation.Ping();

                var result = ping.Send(url);

                if (result.Status == System.Net.NetworkInformation.IPStatus.Success)
                    return " ping Success";
                else
                    return " ping Failed";
            }
            catch (Exception ex)
            {
                string txt = ex.Message;
                return " ping Failed";
            }

           

        }




        private string KioskInfo_Echeck(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
            request.Method = "HEAD";
            try
            {
                var response = request.GetResponse();
                // do something with response.Headers to find out information about the request
                return "Success";
            }
            //catch (WebException wex)
            catch (WebException)
            {
               
                //set flag if there was a timeout or some other issues
                return "Failed";
            }
        }

        public string ResultFile(string result, string folder)
        {
            string path = System.IO.Path.Combine(ConfigurationManager.AppSettings["ResultPath"], folder);

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(result);
            string name = "result" + (DateTime.Now.ToString("yyyyMMddHHmmss")) + ".txt";
            string destFile = System.IO.Path.Combine(path, name);
            try
            {
                using (StreamWriter outfile = new StreamWriter(destFile))
                {
                    outfile.Write(sb.ToString());
                    return "\nResult File Created " + path + name + "\n";
                }
            }
            catch (Exception e)
            {
                return "\nError Creating Result File " + e.Message;
            }
        }

        public DataTable Database_Restore_Rows(DataTable dt)
        {
            string ret = null;

            DataTable dtRet = new DataTable();
            dtRet.Columns.Add("Restored");
            dtRet.Columns.Add("Timestamp");
            dtRet.Columns.Add("Subject");
            dtRet.Columns.Add("Content");

            for (int d = 0; d < dt.Rows.Count; d++)
            {
                string timestamp = null, subject = null, content = null;
                timestamp = dt.Rows[d].ItemArray[0].ToString();
                subject = @dt.Rows[d].ItemArray[1].ToString();
                content = @dt.Rows[d].ItemArray[2].ToString();

                try
                {
                    //MFKLocalLog tab = new MFKLocalLog();
                    //tab.SentToServerTime = null;
                    //tab.Header = "1";//@subject;
                    //tab.Content = "2";//@content;
                    //tab.LocalTime = Convert.ToDateTime(timestamp);
                    //tab.Attempts = 1;
                    //tab.Error = null;

                    //context.AddToMFKLocalLog(tab);
                    //context.SaveChanges();
                    string sqlConnectionString = GetConnectionString();
                    FileInfo file = new FileInfo(ConfigurationManager.AppSettings["A1"]);
                    string script = "INSERT INTO [MFKLocalLog].[dbo].[MFKLocalLog]([Header],[Content],[LocalTime],[SentToServerTime],[Attempts],[Error])VALUES('"+subject+"','"+content+"','" + timestamp + "',null,0,null)";
                    SqlConnection conn = new SqlConnection(sqlConnectionString);
                    Server server = new Server(new ServerConnection(conn));
                    server.ConnectionContext.ExecuteNonQuery(script);
                    ret = "Complete";
                }
                catch (Exception e)
                {
                    ret = "Failed. " + e.Message;
                }
                DataRow dr = dtRet.NewRow();
                dr["Restored"] = ret;
                dr["Timestamp"] = timestamp;
                dr["Subject"] = @subject;
                dr["Content"] = @content;
                dtRet.Rows.Add(dr);
            }
            context.Dispose();
            return dtRet;
        }

        public string OnlyConfig_Module(string liveUrl, string moduleUrl, string path, string backupPath, int counter, string webProtocol)
        {
            string status = "";
            if (!System.IO.Directory.Exists(backupPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(backupPath);
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create backup folder " + backupPath + ". " + ex.Message + ".\n";
                }
            }


            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
                    status += counter + ".Created " + System.IO.Path.GetDirectoryName(path) + ".\n";
                }
                catch (Exception ex)
                {
                    status = counter + ".Couldn't create folder " + System.IO.Path.GetDirectoryName(path) + ". " + ex.Message + ".\n";
                }
            }

            string fileName = System.IO.Path.GetFileName(path);
            string backupFile = System.IO.Path.Combine(backupPath, fileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Copy(path, backupFile, true);
                    status += counter + ".BackUp " + backupFile + ". \n";
                }
                catch (Exception e)
                {
                    status += counter + ".Couldn't backup File " + backupFile + ". " + e.Message + ".\n";
                }
            }
            else
            {
                status += counter + ".Couldn't backup File " + backupFile + ". Folder don't exist. \n";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=" + '"' + "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + "?>");
            sb.AppendLine("<configuration>");
            sb.AppendLine("\t\t<configSections>");
            sb.AppendLine("\t\t\t<sectionGroup name=" + '"' + "applicationSettings" + '"' + " type=" + '"' + "System.Configuration.ApplicationSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" + '"' + ">");
            sb.AppendLine("\t\t\t\t<section name=" + '"' + "MFKSynchroModuleSvc.Properties.MFKSynchroModuleSvc" + '"' + " type=" + '"' + "System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" + '"' + " requirePermission=" +'"' + "false" +'"' + " />");
            sb.AppendLine("\t\t\t</sectionGroup>");
            sb.AppendLine("\t\t\t<section name=" + '"' + "loggingConfiguration" + '"' + " type=" + '"' + "Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" + '"' + " />");
            sb.AppendLine("\t\t</configSections>");
            sb.AppendLine("");
            sb.AppendLine("\t\t<!--LOGGING APPLICATION BLOCK-->");
            sb.AppendLine("\t\t\t<loggingConfiguration configSource=" +'"'+ "LogginBlock.config" + '"' + " />");
            sb.AppendLine("\t\t<!--LOGGING APPLICATION BLOCK-->");
            sb.AppendLine("");
            sb.AppendLine("\t\t<system.serviceModel>");
            sb.AppendLine("\t\t\t<bindings>");
            sb.AppendLine("\t\t\t\t<basicHttpBinding>");
            sb.AppendLine("\t\t\t\t\t<binding name=" + '"' + "BasicHttpBinding_IKioskModuleService" + '"' + "  closeTimeout=" + '"' + "00:03:00" + '"' + " openTimeout=" + '"' + "00:03:00" + '"' + " receiveTimeout=" + '"' + "00:03:00" + '"' + " sendTimeout=" + '"' + "00:03:00" + '"' + " maxBufferPoolSize=" + '"' + "2147482548" + '"' + " maxBufferSize=" + '"' + "2147482548" + '"' + " maxReceivedMessageSize=" + '"' + "2147482548" + '"' + " messageEncoding=" + '"' + "Text" + '"' + "  >");
            sb.AppendLine("\t\t\t\t\t\t<readerQuotas maxDepth=" + '"' + "2147482548" + '"' + " maxStringContentLength=" + '"' + "2147482548" + '"' + " maxArrayLength=" + '"' + "2147482548" + '"' + " maxBytesPerRead=" + '"' + "2147482548" + '"' + " maxNameTableCharCount=" + '"' + "2147482548" + '"' + " />");
            if(webProtocol == "https")
                sb.AppendLine("\t\t\t\t\t\t<security mode=" + '"' + "Transport" + '"' + " />");
            else
                sb.AppendLine("");
            sb.AppendLine("\t\t\t\t\t</binding>");
            sb.AppendLine("\t\t\t\t</basicHttpBinding>");
            sb.AppendLine("\t\t\t</bindings>");
            sb.AppendLine("\t\t\t<client>");
            sb.AppendLine("\t\t\t\t<endpoint address=" + '"' + webProtocol + "://" + moduleUrl + "/BOSWebModuleSvc.KioskModuleService.svc" + '"');
            sb.AppendLine("\t\t\t\tbinding=" + '"' + "basicHttpBinding" + '"' + " bindingConfiguration=" + '"' + "BasicHttpBinding_IKioskModuleService" + '"');
            sb.AppendLine("\t\t\t\tcontract=" + '"' + "IKioskModuleService" + '"' + " name=" + '"' + "BasicHttpBinding_IKioskModuleService" + '"' + " />");
            sb.AppendLine("\t\t\t</client>");
            sb.AppendLine("\t\t</system.serviceModel>");
            sb.AppendLine("\t\t<applicationSettings>");
            sb.AppendLine("\t\t\t<MFKSynchroModuleSvc.Properties.MFKSynchroModuleSvc>");
            sb.AppendLine("\t\t\t\t<setting name=" + '"' + "ServiceIntervalMinutes" + '"' + " serializeAs=" + '"' + "String" + '"' + ">");
            sb.AppendLine("\t\t\t\t\t<value>5</value>");
            sb.AppendLine("\t\t\t\t</setting>");
            sb.AppendLine("\t\t\t</MFKSynchroModuleSvc.Properties.MFKSynchroModuleSvc>");
            sb.AppendLine("\t\t</applicationSettings>");
            sb.AppendLine("\t\t<runtime>");
            sb.AppendLine("\t\t\t<assemblyBinding xmlns=" + '"' + "urn:schemas-microsoft-com:asm.v1" + '"' + ">");
            //"+'"'+"            
            sb.AppendLine("\t\t\t\t<probing privatePath=" + '"' + "Bin;" + '"' + " />");
            sb.AppendLine("\t\t\t</assemblyBinding>");
            sb.AppendLine("\t\t</runtime>");
            sb.AppendLine("</configuration>");

            try
            {
                using (StreamWriter outfile = new StreamWriter(path))
                {
                    outfile.Write(sb.ToString());
                    return status + counter + ".Created File " + path + ".\n";
                }
            }
            catch (Exception e)
            {
                return status + counter + ".Error Creating file KioskWebSvcSettings.config. " + e.Message + ".\n";
            }
        }

    }
}
